-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: internal-db.s206686.gridserver.com
-- Generation Time: Aug 26, 2015 at 06:27 PM
-- Server version: 5.1.72-rel14.10
-- PHP Version: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db206686_jessereedfromohio`
--

-- --------------------------------------------------------

--
-- Table structure for table `craft_assetfiles`
--

CREATE TABLE IF NOT EXISTS `craft_assetfiles` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` smallint(6) unsigned DEFAULT NULL,
  `height` smallint(6) unsigned DEFAULT NULL,
  `size` int(11) unsigned DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetfiles_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `craft_assetfiles_sourceId_fk` (`sourceId`),
  KEY `craft_assetfiles_folderId_fk` (`folderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `craft_assetfiles`
--

INSERT INTO `craft_assetfiles` (`id`, `sourceId`, `folderId`, `filename`, `kind`, `width`, `height`, `size`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(56, 1, 1, 'MS_GE_30-copy_web_960.jpg', 'image', 960, 625, 146277, '2015-06-01 04:56:30', '2015-06-01 04:56:30', '2015-06-01 04:56:30', 'c4f1e0d5-18fa-4b66-8660-ccf2404fb180'),
(58, 1, 1, 'MS_GE_24-copy_web_960.jpg', 'image', 960, 648, 164690, '2015-06-01 04:57:42', '2015-06-01 04:57:42', '2015-06-01 04:57:42', '83a88caf-cd3e-4554-9218-09d466ece76e'),
(59, 1, 1, 'MS_GE_17-copy_web_960.jpg', 'image', 960, 621, 147010, '2015-06-01 04:57:43', '2015-06-01 04:57:43', '2015-06-01 04:57:43', '92486bac-facc-4ab6-ad5a-583d5f41d16d'),
(159, 1, 1, 'nycta_graphics_standards_manual__detail_2_WEB.jpg', 'image', 1400, 933, 393543, '2015-07-21 01:57:02', '2015-07-21 01:57:03', '2015-07-21 01:57:03', 'fd917152-5c68-4164-a926-f5a3e80f6068'),
(160, 1, 1, 'nycta_graphics_standards_manual_2_WEB.jpg', 'image', 1400, 888, 152421, '2015-07-21 01:57:54', '2015-07-21 01:57:55', '2015-07-21 01:57:55', '17f70e60-a0e6-4378-abf0-68cc0acecff2'),
(161, 1, 1, 'nycta_graphics_standards_manual_4_WEB.jpg', 'image', 1400, 888, 169420, '2015-07-21 01:57:57', '2015-07-21 01:57:59', '2015-07-21 01:57:59', 'd8370262-5b0f-4a5c-8acf-f459f6fd9f61'),
(162, 1, 1, 'nycta_graphics_standards_manual_7_WEB.jpg', 'image', 1400, 888, 121422, '2015-07-21 01:58:00', '2015-07-21 01:58:02', '2015-07-21 01:58:02', '656851e7-c4e7-4e6f-9e60-11198bd26fe8'),
(163, 1, 1, 'nycta_graphics_standards_manual_8_WEB.jpg', 'image', 1400, 888, 126382, '2015-07-21 01:58:04', '2015-07-21 01:58:07', '2015-07-21 01:58:07', 'c31de8c0-3c6b-4be4-a8d1-baeb649a421a'),
(164, 1, 1, 'nycta_graphics_standards_manual_13_WEB.jpg', 'image', 1400, 888, 149580, '2015-07-21 01:58:08', '2015-07-21 01:58:10', '2015-07-21 01:58:10', 'b66bbd03-7519-47ca-afda-85f153456e74'),
(165, 1, 1, 'nycta_graphics_standards_manual_14_WEB.jpg', 'image', 1400, 888, 108598, '2015-07-21 01:58:12', '2015-07-21 01:58:15', '2015-07-21 01:58:15', '8802ec6c-7be6-4a6b-b118-962f1995ec1a'),
(166, 1, 1, 'nycta_graphics_standards_manual_16_WEB.jpg', 'image', 1400, 888, 100848, '2015-07-21 01:58:18', '2015-07-21 01:58:20', '2015-07-21 01:58:20', 'ef6c179b-8e1e-4c7d-b5e0-f35648b896b8'),
(167, 1, 1, 'nycta_graphics_standards_manual_20_WEB.jpg', 'image', 1400, 888, 104846, '2015-07-21 01:58:22', '2015-07-21 01:58:25', '2015-07-21 01:58:25', '5d48b286-db58-4934-9f54-cbae118a78c6'),
(168, 1, 1, 'nycta_graphics_standards_manual_26_WEB.jpg', 'image', 1400, 888, 96952, '2015-07-21 01:58:28', '2015-07-21 01:58:30', '2015-07-21 01:58:30', '6e8b9f9d-37c1-447a-97f7-ba5ff9b97275'),
(169, 1, 1, 'nycta_graphics_standards_manual_30_WEB.jpg', 'image', 1400, 888, 90417, '2015-07-21 01:58:32', '2015-07-21 01:58:34', '2015-07-21 01:58:34', 'ba3b4e7f-3ddb-44e1-b05f-4648265c335b'),
(170, 1, 1, 'nycta_graphics_standards_manual_39_WEB.jpg', 'image', 1400, 888, 103894, '2015-07-21 01:58:36', '2015-07-21 01:58:38', '2015-07-21 01:58:38', 'e928f6dd-7878-498c-b20d-26017585c2eb'),
(171, 1, 1, 'nycta_graphics_standards_manual_43_WEB.jpg', 'image', 1400, 888, 113803, '2015-07-21 01:58:40', '2015-07-21 01:58:42', '2015-07-21 01:58:42', 'caa15d8d-4a13-4028-a290-0646d053bfc7'),
(172, 1, 1, 'nycta_graphics_standards_manual_44_WEB.jpg', 'image', 1400, 888, 110719, '2015-07-21 01:58:45', '2015-07-21 01:58:46', '2015-07-21 01:58:46', '4c8dad05-c5c4-4c19-97d4-6af4eac6cd5d'),
(173, 1, 1, 'nycta_graphics_standards_manual_45_WEB.jpg', 'image', 1400, 888, 106405, '2015-07-21 01:58:49', '2015-07-21 01:58:51', '2015-07-21 01:58:51', 'bf91144e-264c-40dd-b280-a8c8f2429c7a'),
(174, 1, 1, 'nycta_graphics_standards_manual_46_WEB.jpg', 'image', 1400, 888, 107988, '2015-07-21 01:58:54', '2015-07-21 01:58:56', '2015-07-21 01:58:56', '827496cc-adb6-458a-bdc3-2eb49674d5c4'),
(175, 1, 1, 'nycta_graphics_standards_manual_47_WEB.jpg', 'image', 1400, 888, 111423, '2015-07-21 01:58:58', '2015-07-21 01:59:00', '2015-07-21 01:59:00', '384ddc82-76ea-491a-893e-3e53a92c1e0d'),
(176, 1, 1, 'nycta_graphics_standards_manual_48_WEB.jpg', 'image', 1400, 888, 103379, '2015-07-21 01:59:01', '2015-07-21 01:59:04', '2015-07-21 01:59:04', '016d1223-5f0d-4bef-a30b-75bf4c0d345e'),
(177, 1, 1, 'nycta_graphics_standards_manual_cover_WEB.jpg', 'image', 1400, 933, 220273, '2015-07-21 01:59:07', '2015-07-21 01:59:08', '2015-07-21 01:59:08', '47d2c1bd-6816-4912-9a92-23ba60338ec3'),
(178, 1, 1, 'nycta_graphics_standards_manual_detail_5_WEB.jpg', 'image', 1400, 933, 1053997, '2015-07-21 01:59:12', '2015-07-21 01:59:14', '2015-07-21 01:59:14', '8a7dc474-0158-4842-b7cf-94984de07b04'),
(179, 1, 1, 'nycta_graphics_standards_manual_detail_7_WEB.jpg', 'image', 1400, 933, 87793, '2015-07-21 01:59:16', '2015-07-21 01:59:18', '2015-07-21 01:59:18', 'a5006a83-1afd-4133-8bf4-e2c092027250'),
(180, 1, 1, 'nycta_graphics_standards_manual_detail_10_WEB.jpg', 'image', 1400, 933, 161831, '2015-07-21 01:59:19', '2015-07-21 01:59:21', '2015-07-21 01:59:21', '247b35e5-3f3c-4ea3-81f6-35b0aa3c16b2'),
(181, 1, 1, 'nycta_graphics_standards_manual_detail_16_WEB.jpg', 'image', 1400, 933, 252693, '2015-07-21 01:59:24', '2015-07-21 01:59:26', '2015-07-21 01:59:26', 'adcc6070-e1d7-446b-ba1d-1af84f453a87'),
(182, 1, 1, 'nycta_graphics_standards_manual_detail_17_WEB.jpg', 'image', 1400, 933, 461406, '2015-07-21 01:59:29', '2015-07-21 01:59:32', '2015-07-21 01:59:32', '8d0e1e3a-ec37-481f-bdf2-86ee93cf67f8'),
(183, 1, 1, 'nycta_graphics_standards_manual_detail_25_WEB.jpg', 'image', 1400, 933, 299090, '2015-07-21 01:59:35', '2015-07-21 01:59:36', '2015-07-21 01:59:36', '2fdd6cdf-7208-4e41-b207-69c85b8c0f99'),
(184, 1, 1, 'nycta_graphics_standards_manual_1_WEB.jpg', 'image', 1400, 888, 108465, '2015-07-21 02:05:35', '2015-07-21 02:05:36', '2015-07-21 02:05:36', 'bd76f224-2100-4fca-8a66-158f3c22a982'),
(185, 1, 1, 'GATO_WEB_1.jpg', 'image', 1400, 933, 346416, '2015-07-21 02:11:35', '2015-07-21 02:11:36', '2015-07-21 02:11:36', '6ccab5cb-850f-445a-8638-45fd04f836e3'),
(186, 1, 1, 'GATO_WEB_2.jpg', 'image', 1400, 933, 367432, '2015-07-21 02:11:37', '2015-07-21 02:11:38', '2015-07-21 02:11:38', 'dcd79fd7-a295-4095-82ab-377325ea7281'),
(187, 1, 1, 'GATO_WEB_3.jpg', 'image', 1400, 933, 471964, '2015-07-21 02:11:39', '2015-07-21 02:11:40', '2015-07-21 02:11:40', 'b1549eae-1206-4a67-a2b2-3309e56570a2'),
(188, 1, 1, 'GATO_WEB_4.jpg', 'image', 1400, 885, 121662, '2015-07-21 02:11:41', '2015-07-21 02:11:42', '2015-07-21 02:11:42', 'fc8c4ad7-32cf-425f-9d1e-0518814722e1'),
(189, 1, 1, 'GATO_WEB_5.jpg', 'image', 1400, 934, 306008, '2015-07-21 02:11:43', '2015-07-21 02:11:44', '2015-07-21 02:11:44', 'bdbf4631-6824-4670-9ddb-a4cf8ad3c587'),
(190, 1, 1, 'GATO_WEB_6.jpg', 'image', 1400, 934, 189919, '2015-07-21 02:11:45', '2015-07-21 02:11:46', '2015-07-21 02:11:46', '0532ba9e-eeb1-4dba-9673-2ea536f9497f'),
(191, 1, 1, 'GATO_WEB_7.jpg', 'image', 1400, 934, 407171, '2015-07-21 02:11:47', '2015-07-21 02:11:48', '2015-07-21 02:11:48', '60f0ae9b-e076-4ae3-bc83-38ab3f02cf6c'),
(192, 1, 1, 'GATO_WEB_8.jpg', 'image', 1400, 934, 635131, '2015-07-21 02:11:50', '2015-07-21 02:11:50', '2015-07-21 02:11:50', '157d946e-d012-44e7-9982-e2a19137d849'),
(193, 1, 1, 'GATO_WEB_9.jpg', 'image', 1400, 935, 294386, '2015-07-21 02:11:51', '2015-07-21 02:11:52', '2015-07-21 02:11:52', 'b84fbdaa-108d-46fc-868a-baf5349622dc'),
(194, 1, 1, 'GATO_WEB_10.jpg', 'image', 1400, 935, 381609, '2015-07-21 02:11:53', '2015-07-21 02:11:54', '2015-07-21 02:11:54', '12d4ef30-c78b-45f8-8f78-8e86a7beeb03'),
(195, 1, 1, 'GATO_WEB_11.jpg', 'image', 1400, 934, 205483, '2015-07-21 02:11:55', '2015-07-21 02:11:55', '2015-07-21 02:11:55', 'f75a3f45-04d8-4ea9-851c-ea0b716d4366'),
(196, 1, 1, 'GATO_WEB_12.jpg', 'image', 1400, 934, 299125, '2015-07-21 02:11:57', '2015-07-21 02:11:58', '2015-07-21 02:11:58', 'fb5c0336-b970-4a78-b150-8c9fc46a1c4d'),
(197, 1, 1, 'GATO_WEB_13.jpg', 'image', 1400, 934, 501660, '2015-07-21 02:11:59', '2015-07-21 02:12:00', '2015-07-21 02:12:00', '47b97631-c3ca-4d77-b80b-54d8209cbfaa'),
(198, 1, 1, 'GATO_WEB_14.jpg', 'image', 1400, 923, 299370, '2015-07-21 02:12:01', '2015-07-21 02:12:02', '2015-07-21 02:12:02', '412bfddd-ada8-45a9-807b-eed82e39586c'),
(199, 1, 1, 'GATO_WEB_15.jpg', 'image', 1400, 934, 192641, '2015-07-21 02:12:03', '2015-07-21 02:12:03', '2015-07-21 02:12:03', 'b40d4929-0027-4b4d-a607-86944306fe94'),
(200, 1, 1, 'GATO_WEB_16.jpg', 'image', 1400, 934, 348450, '2015-07-21 02:12:05', '2015-07-21 02:12:05', '2015-07-21 02:12:05', '85224b41-a08f-4cc8-9fbb-d12d808532e0'),
(201, 1, 1, 'GATO_WEB_17rev.jpg', 'image', 1400, 934, 589000, '2015-07-21 02:12:07', '2015-07-21 02:12:07', '2015-07-21 02:12:07', '8d7d02d3-d4c9-4206-bfec-f12c701020c7'),
(202, 1, 1, 'GATO_WEB_18.jpg', 'image', 1400, 934, 529550, '2015-07-21 02:12:09', '2015-07-21 02:12:10', '2015-07-21 02:12:10', '331f4237-cc71-4841-b352-b6381b2249ff'),
(203, 1, 1, 'SAKS_BAG_STREET_5_TRIX_WEB.jpg', 'image', 1400, 933, 321675, '2015-07-21 02:37:04', '2015-07-21 02:37:04', '2015-07-21 02:37:04', 'fed1ce63-a7c7-4ce6-bab3-f98c8f1ab4be'),
(204, 1, 1, 'SAKS_BAG_STREET_8_TRIX_WEB.jpg', 'image', 1400, 933, 491029, '2015-07-21 02:37:06', '2015-07-21 02:37:06', '2015-07-21 02:37:06', '464e35d7-1e6b-481c-8b39-1f2a0834dfea'),
(205, 1, 1, 'SAKS_BAG_STREET_9_TRIX_WEB.jpg', 'image', 1400, 933, 474484, '2015-07-21 02:37:08', '2015-07-21 02:37:09', '2015-07-21 02:37:09', '8e2df27b-7696-44b7-8ccf-e2fc0ead4866'),
(206, 1, 1, 'SAKS_BAG_STREET_16_TRIX_WEB.jpg', 'image', 1400, 933, 501284, '2015-07-21 02:37:10', '2015-07-21 02:37:11', '2015-07-21 02:37:11', 'fc0dc74b-e932-48b9-afc1-f080492a588b'),
(207, 1, 1, 'SAKS_LOOK_MARK.jpg', 'image', 2917, 1944, 95461, '2015-07-21 02:37:13', '2015-07-21 02:37:13', '2015-07-21 02:37:13', '47610129-ad1c-40f6-9fbd-e509b42cc4b3'),
(208, 1, 1, 'SFA_LOOK_CATAGLOGUE_AUG1_TRIX_WEB.jpg', 'image', 1400, 933, 454038, '2015-07-21 02:37:15', '2015-07-21 02:37:15', '2015-07-21 02:37:15', 'f3b9d57d-ab9f-4e35-ae04-57ed46d0fbfe'),
(209, 1, 1, 'SFA_LOOK_CATAGLOGUE_JUNE_1_TRIX_WEB.jpg', 'image', 1400, 933, 395748, '2015-07-21 02:37:17', '2015-07-21 02:37:18', '2015-07-21 02:37:18', '7d0fd426-f5ef-4387-8df2-f0b1f7a91aa0'),
(210, 1, 1, 'SFA_LOOK_CATAGLOGUE_NEW1_TRIX_WEB.jpg', 'image', 1400, 933, 421803, '2015-07-21 02:37:19', '2015-07-21 02:37:20', '2015-07-21 02:37:20', '74cecb1d-6767-48c7-a3ee-f98da8440d41'),
(211, 1, 1, 'SFA_LOOK_CATAGLOGUE_NEW2_TRIX_WEB.jpg', 'image', 1400, 933, 363235, '2015-07-21 02:37:21', '2015-07-21 02:37:22', '2015-07-21 02:37:22', 'a8b8b28d-371b-4a7e-8cd7-8b3406c0e809'),
(212, 1, 1, 'SAKS_BAG_STREET_4_TRIX_WEB.jpg', 'image', 1400, 933, 433295, '2015-07-21 02:39:47', '2015-07-21 02:39:47', '2015-07-21 02:39:47', '8bb822bd-bba0-4820-82c7-cc302daac4b8'),
(216, 1, 1, 'MS_GE_17_WEB.jpg', 'image', 1440, 937, 226515, '2015-07-21 03:21:31', '2015-07-21 03:21:31', '2015-07-21 03:21:31', 'd48bd3bb-46af-4f82-be10-dd4c8d7586ed'),
(217, 1, 1, 'MS_GE_21_WEB.jpg', 'image', 1440, 937, 376644, '2015-07-21 03:21:34', '2015-07-21 03:21:35', '2015-07-21 03:21:35', '6faf2fa9-8eaa-4f4b-9c22-a789eef02e50'),
(218, 1, 1, 'MS_GE_24_WEB.jpg', 'image', 1440, 937, 288716, '2015-07-21 03:21:39', '2015-07-21 03:21:40', '2015-07-21 03:21:40', '68168738-45ba-4866-a1d3-3b6c7dd90065'),
(219, 1, 1, 'MS_GE_26_WEB.jpg', 'image', 1440, 937, 253663, '2015-07-21 03:21:42', '2015-07-21 03:21:43', '2015-07-21 03:21:43', 'b2a45a1a-4576-44c3-81f2-9d885de8b924'),
(220, 1, 1, 'MS_GE_30_WEB.jpg', 'image', 1440, 937, 234091, '2015-07-21 03:21:46', '2015-07-21 03:21:47', '2015-07-21 03:21:47', '78918742-df40-46fc-a2f7-526b64c00372'),
(221, 1, 1, 'MS_GE_36_WEB.jpg', 'image', 1440, 937, 200879, '2015-07-21 03:21:49', '2015-07-21 03:21:50', '2015-07-21 03:21:50', 'ef5e58e8-fe44-454b-9a20-61420170b772'),
(222, 1, 1, 'ms_south-africa_german-express_5481_WEB.jpg', 'image', 1440, 937, 631487, '2015-07-21 03:21:53', '2015-07-21 03:21:54', '2015-07-21 03:21:54', 'dc5b183a-cfdb-43af-9aa7-8f1b39fb29e5'),
(223, 1, 1, 'ms_south-africa_german-express_5484_WEB.jpg', 'image', 1440, 937, 206782, '2015-07-21 03:21:55', '2015-07-21 03:21:56', '2015-07-21 03:21:56', '63bf2cf3-f980-4ba0-b3f0-d918ab7403e5'),
(224, 1, 1, 'ms_south-africa_german-express_5487_WEB.jpg', 'image', 1440, 937, 274543, '2015-07-21 03:21:58', '2015-07-21 03:21:59', '2015-07-21 03:21:59', '74558346-00b8-4f61-b54d-855a36f2d697'),
(225, 1, 1, 'ms_south-africa_german-express_5490_WEB.jpg', 'image', 1440, 937, 407305, '2015-07-21 03:22:01', '2015-07-21 03:22:02', '2015-07-21 03:22:02', '75ed94aa-8658-4dd5-b2cb-e222b0341fe1'),
(226, 1, 1, 'ms_south-africa_german-express_5499_WEB.jpg', 'image', 1440, 937, 445999, '2015-07-21 03:22:04', '2015-07-21 03:22:05', '2015-07-21 03:22:05', '91e79a17-6058-4583-a7fd-437aab2c8ee5'),
(227, 1, 1, 'ms_south-africa_german-express_5509_WEB.jpg', 'image', 1440, 937, 917125, '2015-07-21 03:22:08', '2015-07-21 03:22:09', '2015-07-21 03:22:09', '3bac2ba0-04e5-400b-a621-e1130656f578'),
(228, 1, 1, 'ms_south-africa_german-express_5511_WEB.jpg', 'image', 1440, 937, 505860, '2015-07-21 03:22:11', '2015-07-21 03:22:11', '2015-07-21 03:22:11', 'fbf451f2-c547-46c3-a9a1-8c69f50b4a8a'),
(229, 1, 1, 'ms_south-africa_german-express_5518_WEB.jpg', 'image', 1440, 937, 728410, '2015-07-21 03:22:19', '2015-07-21 03:22:20', '2015-07-21 03:22:20', '70c228f3-4eb4-4053-a560-e2ec05b2f208'),
(230, 1, 1, 'ms_south-africa_german-express_5526_WEB.jpg', 'image', 1440, 937, 436138, '2015-07-21 03:22:26', '2015-07-21 03:22:27', '2015-07-21 03:22:27', 'fdac30c0-6116-458a-a1c8-a82b5f5a1723'),
(231, 1, 1, 'ms_south-africa_german-express_5561_WEB.jpg', 'image', 1440, 937, 195742, '2015-07-21 03:22:29', '2015-07-21 03:22:30', '2015-07-21 03:22:30', 'c5cc15bb-6f92-4fab-8679-cc42da575d8d'),
(232, 1, 1, 'ms_south-africa_german-express_5585_WEB.jpg', 'image', 1440, 937, 307223, '2015-07-21 03:22:32', '2015-07-21 03:22:33', '2015-07-21 03:22:33', 'dc8d9fd1-b137-4589-ade7-c043937ab846'),
(242, 1, 1, 'Coyote_v_Acme_01_WEB.jpg', 'image', 1400, 933, 107428, '2015-07-23 23:03:46', '2015-07-23 23:03:47', '2015-07-23 23:03:47', '36463e55-6de0-4dbe-a881-2226cbe08ff3'),
(243, 1, 1, 'Coyote_v_Acme_02_WEB.jpg', 'image', 1400, 933, 104151, '2015-07-23 23:03:48', '2015-07-23 23:03:48', '2015-07-23 23:03:48', '88036705-ab92-48b1-b7f3-7ff55f29d60d'),
(244, 1, 1, 'Coyote_v_Acme_03_WEB.jpg', 'image', 1400, 933, 169725, '2015-07-23 23:03:49', '2015-07-23 23:03:49', '2015-07-23 23:03:49', '03f7233d-6deb-4d17-a2a9-a8e670ac3670'),
(245, 1, 1, 'Coyote_v_Acme_06_WEB.jpg', 'image', 1400, 933, 191312, '2015-07-23 23:03:50', '2015-07-23 23:03:50', '2015-07-23 23:03:50', '988a1490-835d-4a94-b6b5-5fb6e182ea5e'),
(246, 1, 1, 'Coyote_v_Acme_11_WEB.jpg', 'image', 1400, 933, 178182, '2015-07-23 23:03:51', '2015-07-23 23:03:52', '2015-07-23 23:03:52', 'f1d56dd3-1d6f-4ead-a46b-e3f0c2ae1e76'),
(247, 1, 1, 'Coyote_v_Acme_13_WEB.jpg', 'image', 1400, 933, 192292, '2015-07-23 23:03:53', '2015-07-23 23:03:53', '2015-07-23 23:03:53', '8209fc9d-e76e-4718-83aa-2e3c1d33dc26'),
(248, 1, 1, 'Coyote_v_Acme_16_WEB.jpg', 'image', 1400, 933, 186580, '2015-07-23 23:03:54', '2015-07-23 23:03:55', '2015-07-23 23:03:55', '4737e034-9953-47e7-89ab-9c84675c78ea'),
(249, 1, 1, 'Coyote_v_Acme_18_WEB.jpg', 'image', 1400, 933, 269089, '2015-07-23 23:03:56', '2015-07-23 23:03:56', '2015-07-23 23:03:56', '5db8da45-d070-4464-b1c8-de318184e303'),
(250, 1, 1, 'Coyote_v_Acme_19_WEB.jpg', 'image', 1400, 933, 313300, '2015-07-23 23:03:58', '2015-07-23 23:03:59', '2015-07-23 23:03:59', 'eb913b0b-a0c8-4aab-bffe-a38d3dad870f'),
(251, 1, 1, 'Coyote_v_Acme_20_WEB.jpg', 'image', 1400, 933, 326143, '2015-07-23 23:04:00', '2015-07-23 23:04:00', '2015-07-23 23:04:00', '14fe9266-9114-4268-9350-d8d95344984e'),
(252, 1, 1, 'Coyote_v_Acme_23_WEB.jpg', 'image', 1400, 933, 350041, '2015-07-23 23:04:01', '2015-07-23 23:04:02', '2015-07-23 23:04:02', 'd9a97943-0a95-4f06-b737-6d473a6d39bc'),
(253, 1, 1, 'Coyote_v_Acme_28_WEB.jpg', 'image', 1400, 933, 389747, '2015-07-23 23:04:03', '2015-07-23 23:04:03', '2015-07-23 23:04:03', '6e65c8d8-7b86-43d8-b44a-e8bfcaca993b'),
(254, 1, 1, 'Coyote_v_Acme_29_WEB.jpg', 'image', 1400, 933, 472436, '2015-07-23 23:04:04', '2015-07-23 23:04:05', '2015-07-23 23:04:05', '3556fb9e-7414-466f-8c26-1a6fd400faf1'),
(262, 1, 1, 'BIGAPPS_SYMBOL_WEB.jpg', 'image', 3000, 1944, 94385, '2015-07-23 23:33:49', '2015-07-23 23:33:50', '2015-07-23 23:33:50', '54d3132c-4a69-43c8-8d2c-859469a78deb'),
(263, 1, 1, 'bigapps_program_WEB.jpg', 'image', 1400, 933, 331843, '2015-07-23 23:40:14', '2015-07-23 23:40:15', '2015-07-23 23:40:15', '85210529-944c-4515-b140-d97f3db41427'),
(264, 1, 1, 'bigapps_mayor_WEB.jpg', 'image', 1400, 933, 308269, '2015-07-23 23:40:35', '2015-07-23 23:40:36', '2015-07-23 23:40:36', 'd06f3a81-22b6-461d-a2a1-6e7b86895463'),
(265, 1, 1, 'bigapps_poster_detail_WEB.jpg', 'image', 1400, 933, 285613, '2015-07-23 23:40:48', '2015-07-23 23:40:49', '2015-07-23 23:40:49', '27033d53-cc30-4d24-8bf9-a48d7809b50f'),
(267, 1, 1, 'bigapps_poster_v4_WEB.jpg', 'image', 1400, 933, 809133, '2015-07-23 23:41:22', '2015-07-23 23:41:23', '2015-07-23 23:41:23', 'a6c610a3-8811-40cf-803d-86d84eb16321'),
(268, 1, 1, 'bigapps_screen_circles_web.jpg', 'image', 1400, 933, 202611, '2015-07-23 23:41:30', '2015-07-23 23:41:31', '2015-07-23 23:41:31', '1ef48ac6-a3ce-4693-8a48-aeb52a70d627'),
(269, 1, 1, 'BigApps_Bus-Shelter_2014_11_CC_WEB.jpg', 'image', 1400, 933, 480278, '2015-07-23 23:50:11', '2015-07-23 23:50:12', '2015-07-23 23:50:12', 'f5dc9f77-3d76-4913-acb2-fdaf367e943a'),
(270, 1, 1, 'BigApps_Bus-Shelter_2014_50_WEB.jpg', 'image', 1400, 933, 371420, '2015-07-23 23:50:23', '2015-07-23 23:50:24', '2015-07-23 23:50:24', '59549e5a-b776-447e-8557-7da8e56e59ec'),
(271, 1, 1, 'bigapps_button_WEB.jpg', 'image', 1400, 933, 752625, '2015-07-23 23:53:20', '2015-07-23 23:53:21', '2015-07-23 23:53:21', '89f5cfdc-b0b1-476d-bff8-2fc6fb4119b9'),
(272, 1, 1, 'BIGAPPS_BLOOM_WEB.jpg', 'image', 1400, 933, 227063, '2015-07-23 23:56:15', '2015-07-23 23:56:16', '2015-07-23 23:56:16', '7766d75a-7559-4053-8419-f19600af3d3e'),
(279, 1, 1, 'JessicaJorgensen-AbsintheMakesTheHeartGrowFonder_web.jpg', 'image', 491, 600, 174590, '2015-08-01 04:08:47', '2015-08-01 04:08:48', '2015-08-01 04:08:48', '3a17a143-b168-4656-aa5f-26898d638105'),
(281, 1, 1, 'DOT_Taxi_140407_WEB.jpg', 'image', 1400, 933, 175882, '2015-08-07 17:42:48', '2015-08-07 17:42:48', '2015-08-07 17:42:48', '47ca34b0-4a08-4877-9f1e-dbf78f8e26bd'),
(282, 1, 1, 'DOT_TruckLoading2_140407_WEB.jpg', 'image', 1400, 933, 304162, '2015-08-07 17:46:16', '2015-08-07 17:46:17', '2015-08-07 17:46:17', '8b41380b-4bb8-4eb9-9d22-88304acacf79'),
(285, 1, 1, 'DOT_ICONS.jpg', 'image', 1400, 933, 55330, '2015-08-16 21:55:10', '2015-08-16 21:14:13', '2015-08-16 21:56:00', '02da5a28-464a-468b-9ca6-3b0e4d93fa5f'),
(287, 1, 1, 'DOT_ICONS2.jpg', 'image', 1400, 933, 51417, '2015-08-16 21:55:12', '2015-08-16 21:55:13', '2015-08-16 21:55:13', 'cb7ece31-557a-4014-8507-b49ced41692e'),
(288, 1, 1, 'DOT_ICONS3.jpg', 'image', 1400, 933, 55079, '2015-08-16 21:55:14', '2015-08-16 21:55:15', '2015-08-16 21:55:15', '5aa27801-b3e0-4a7d-af24-f64d89d53252'),
(289, 1, 1, 'DOT_ICONS4.jpg', 'image', 1400, 933, 51221, '2015-08-16 21:55:16', '2015-08-16 21:55:16', '2015-08-16 21:55:16', '2ebcd7df-10b9-4b4c-94b2-2779c23ea9d0'),
(290, 1, 1, 'DOT_ICONS5.jpg', 'image', 1400, 933, 48180, '2015-08-16 21:55:17', '2015-08-16 21:55:18', '2015-08-16 21:55:18', '94719c51-14b2-4469-a65d-74c75c9e663c'),
(291, 1, 1, 'DOT_ICONS6.jpg', 'image', 1400, 933, 53537, '2015-08-16 21:55:19', '2015-08-16 21:55:20', '2015-08-16 21:55:20', 'd47f2eed-7225-408e-9021-8a26032682f1'),
(292, 1, 1, 'DOT_ICONS7.jpg', 'image', 1400, 933, 53597, '2015-08-16 21:55:21', '2015-08-16 21:55:22', '2015-08-16 21:55:22', 'ff6f0a21-5e57-4dec-a225-484534f06369'),
(293, 1, 1, 'DOT_ICONS8.jpg', 'image', 1400, 933, 55353, '2015-08-16 21:55:22', '2015-08-16 21:55:23', '2015-08-16 21:55:23', '68657d12-3e21-40d8-8e9f-0a15df304b16'),
(294, 1, 1, 'DOT_ICONS9.jpg', 'image', 1400, 933, 55354, '2015-08-16 21:55:24', '2015-08-16 21:55:25', '2015-08-16 21:55:25', 'ab1e3cf7-3e2c-464a-b304-3233f57e550c'),
(295, 1, 1, 'DOT_ICONS10.jpg', 'image', 1400, 933, 53989, '2015-08-16 21:55:25', '2015-08-16 21:55:26', '2015-08-16 21:55:26', '479d7d2d-3814-4e21-bc68-5b494fd8c756'),
(296, 1, 1, 'DOT_ICONS11.jpg', 'image', 1400, 933, 52423, '2015-08-16 21:55:27', '2015-08-16 21:55:28', '2015-08-16 21:55:28', '2afab50d-a4cd-4675-87bc-0fe1300beaef'),
(297, 1, 1, 'DOT_ICONS12.jpg', 'image', 1400, 933, 54737, '2015-08-16 21:55:29', '2015-08-16 21:55:30', '2015-08-16 21:55:30', '430d0af5-9d5e-458a-ba90-1f12d6b90c5e'),
(298, 1, 1, 'DOT_ICONS13.jpg', 'image', 1400, 933, 59633, '2015-08-16 21:55:30', '2015-08-16 21:55:31', '2015-08-16 21:55:31', '373cc163-9713-4207-997f-749869657695'),
(299, 1, 1, 'DOT_ICONS14.jpg', 'image', 1400, 933, 52895, '2015-08-16 21:55:32', '2015-08-16 21:55:33', '2015-08-16 21:55:33', 'd5104dfa-01f6-46e4-bc8d-af2c5546571e'),
(300, 1, 1, 'DOT_ICONS15.jpg', 'image', 1400, 933, 50897, '2015-08-16 21:55:34', '2015-08-16 21:55:35', '2015-08-16 21:55:35', '711777fc-f03a-4473-867a-58787512e5c8'),
(301, 1, 1, 'DOT_ICONS16.jpg', 'image', 1400, 933, 51934, '2015-08-16 21:55:35', '2015-08-16 21:55:36', '2015-08-16 21:55:36', '465c8807-e41d-4eb9-900c-41f132be50ea'),
(302, 1, 1, 'DOT_ICONS17.jpg', 'image', 1400, 933, 54292, '2015-08-16 21:55:37', '2015-08-16 21:55:38', '2015-08-16 21:55:38', '5337bc44-ffa5-4e48-989c-01b1db2d33c1'),
(303, 1, 1, 'DOT_ICONS18.jpg', 'image', 1400, 933, 57345, '2015-08-16 21:55:38', '2015-08-16 21:55:39', '2015-08-16 21:55:39', '881f9ee2-8feb-4c1c-8792-d0c32b551e91'),
(304, 1, 1, 'DOT_ICONS19.jpg', 'image', 1400, 933, 50570, '2015-08-16 21:55:40', '2015-08-16 21:55:41', '2015-08-16 21:55:41', '2099e66a-030e-4b05-bae7-2e09d53e936d'),
(305, 1, 1, 'DOT_ICONS20.jpg', 'image', 1400, 933, 52391, '2015-08-16 21:55:41', '2015-08-16 21:55:42', '2015-08-16 21:55:42', 'a7fd2371-8180-4df2-b418-a4a6155585f3'),
(306, 1, 1, 'DOT_ICONS21.jpg', 'image', 1400, 933, 56318, '2015-08-16 21:55:43', '2015-08-16 21:55:44', '2015-08-16 21:55:44', '93290323-24a5-4eb5-b7d9-42bb16865fe7'),
(307, 1, 1, 'DOT_ICONS22.jpg', 'image', 1400, 933, 52016, '2015-08-16 21:55:45', '2015-08-16 21:55:46', '2015-08-16 21:55:46', 'b27256b2-3a36-4c52-b037-eee81da85162'),
(308, 1, 1, 'DOT_ICONS23.jpg', 'image', 1400, 933, 53694, '2015-08-16 21:55:46', '2015-08-16 21:55:47', '2015-08-16 21:55:47', '4707d07a-db45-428c-8750-e8090a845480'),
(311, 1, 1, 'DOT_SIGN_CHINATOWN_WEB.jpg', 'image', 1400, 933, 360092, '2015-08-18 13:29:18', '2015-08-18 13:29:19', '2015-08-18 13:29:19', '7d787d5c-cce2-44d4-ad8a-2808dbd99c31'),
(312, 1, 1, 'DOT_TYPE.jpg', 'image', 1400, 933, 164666, '2015-08-18 13:46:45', '2015-08-18 13:46:46', '2015-08-18 13:46:46', '01b727e2-fa94-4a97-b1dd-08facc11c6b5'),
(313, 1, 1, 'DOT_TYPE2.jpg', 'image', 1400, 933, 67085, '2015-08-18 13:46:47', '2015-08-18 13:46:47', '2015-08-18 13:46:47', '2dd79a49-2875-4d15-9cd3-27e421a66862'),
(318, 1, 1, 'DOT_SECK1.jpg', 'image', 1400, 933, 326552, '2015-08-19 02:43:12', '2015-08-19 02:43:13', '2015-08-19 02:43:13', '458dee50-7266-4deb-be0a-49643da51a07'),
(319, 1, 1, 'DOT_SECK2.jpg', 'image', 1400, 933, 405884, '2015-08-19 02:43:14', '2015-08-19 02:43:15', '2015-08-19 02:43:15', '3f908670-3cc5-4231-9214-a6b35fbb45a6'),
(320, 1, 1, 'DOT_SECK3.jpg', 'image', 1400, 933, 307356, '2015-08-19 02:43:16', '2015-08-19 02:43:17', '2015-08-19 02:43:17', 'f0093f77-a823-4db4-a4b1-663a030724df'),
(321, 1, 1, 'DOT_SECK4.jpg', 'image', 1400, 933, 397083, '2015-08-19 02:43:18', '2015-08-19 02:43:19', '2015-08-19 02:43:19', 'f93f83d3-2bcf-4654-9ac6-70e590067ad9'),
(325, 1, 1, 'DOT_RATION.jpg', 'image', 2917, 1944, 243240, '2015-08-19 03:35:02', '2015-08-19 03:35:05', '2015-08-19 03:35:05', 'c596f172-5516-41ab-b98b-f3f775c3e5cb'),
(326, 1, 1, 'DOT_GRID.jpg', 'image', 2917, 1944, 485421, '2015-08-19 03:42:04', '2015-08-19 03:42:06', '2015-08-19 03:42:06', '50071410-6013-4ade-9454-6b06f5889d42'),
(327, 1, 1, 'DOT_TYPE_MED.jpg', 'image', 2917, 1944, 450126, '2015-08-19 03:52:08', '2015-08-19 03:52:11', '2015-08-19 03:52:11', '3d1cf746-6824-4d29-84e6-c8b8e9fdee4a'),
(328, 1, 1, 'DOT_TYPE_MED2.jpg', 'image', 2917, 1944, 232490, '2015-08-19 03:52:11', '2015-08-19 03:52:14', '2015-08-19 03:52:14', '68a0203b-5afc-40fc-9ad8-9222d14ff60e'),
(331, 1, 1, 'JRFO_LOGOTYPE_CROPS.jpg', 'image', 2917, 1944, 483591, '2015-08-20 19:36:34', '2015-08-20 19:36:37', '2015-08-20 19:36:37', '5bde5d90-758c-49a1-b7b6-9e1bac2bbdcd'),
(332, 1, 1, 'JRFO_LOGOTYPE_CROPS2.jpg', 'image', 2917, 1944, 552296, '2015-08-20 19:36:37', '2015-08-20 19:36:40', '2015-08-20 19:36:40', '37c61e6d-6c10-4310-b7a8-24892982cb0c'),
(333, 1, 1, 'JRFO_LOGOTYPE.jpg', 'image', 2917, 1944, 450449, '2015-08-20 19:36:41', '2015-08-20 19:36:44', '2015-08-20 19:36:44', 'ef5299b1-cad5-4d36-bc78-c32ac518b20e'),
(334, 1, 1, 'JRFO_LOGOTYPE2.jpg', 'image', 2917, 1944, 449947, '2015-08-20 19:36:44', '2015-08-20 19:36:47', '2015-08-20 19:36:47', 'c0fcd715-5160-4d13-98d7-f0d1d3b3fb62'),
(335, 1, 1, 'JRFO_LOGOTYPE3.jpg', 'image', 2917, 1944, 438293, '2015-08-20 19:36:48', '2015-08-20 19:36:51', '2015-08-20 19:36:51', '725b394d-8169-4995-8144-00a91f82e3e8'),
(336, 1, 1, 'JRFO_LOGOTYPE4.jpg', 'image', 2917, 1944, 467747, '2015-08-20 19:36:51', '2015-08-20 19:36:54', '2015-08-20 19:36:54', '5a34972d-a4d4-430b-9497-f1c18af6ee7b'),
(337, 1, 1, 'JRFO_LOGOTYPE5.jpg', 'image', 2917, 1944, 421151, '2015-08-20 19:36:54', '2015-08-20 19:36:57', '2015-08-20 19:36:57', '4851412d-78f3-4b1d-9c1f-c86936c9dfad'),
(338, 1, 1, 'JRFO_LOGOTYPE6.jpg', 'image', 2917, 1944, 482386, '2015-08-20 19:36:57', '2015-08-20 19:37:00', '2015-08-20 19:37:00', '19345894-f1a3-49d3-ba7f-3e7b769ebab7'),
(339, 1, 1, 'JRFO_LOGOTYPE7.jpg', 'image', 2917, 1944, 449228, '2015-08-20 19:37:01', '2015-08-20 19:37:04', '2015-08-20 19:37:04', 'f1cbf178-1656-4993-9568-e602ed20ee31'),
(340, 1, 1, 'JRFO_LOGOTYPE8.jpg', 'image', 2917, 1944, 427548, '2015-08-20 19:37:04', '2015-08-20 19:37:07', '2015-08-20 19:37:07', 'b563e2e5-4ce1-4470-a2d3-ebe754a21cc9'),
(341, 1, 1, 'JRFO_LOGOTYPE9.jpg', 'image', 2917, 1944, 400592, '2015-08-20 19:37:07', '2015-08-20 19:37:10', '2015-08-20 19:37:10', '09505d49-5aa8-4585-8987-61ca9b105268'),
(342, 1, 1, 'JRFO_LOGOTYPE10.jpg', 'image', 2917, 1944, 426167, '2015-08-20 19:37:10', '2015-08-20 19:37:13', '2015-08-20 19:37:13', '66b306e2-028e-4478-891b-f233f3ee4e7d');

-- --------------------------------------------------------

--
-- Table structure for table `craft_assetfolders`
--

CREATE TABLE IF NOT EXISTS `craft_assetfolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetfolders_name_parentId_sourceId_unq_idx` (`name`,`parentId`,`sourceId`),
  KEY `craft_assetfolders_parentId_fk` (`parentId`),
  KEY `craft_assetfolders_sourceId_fk` (`sourceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `craft_assetfolders`
--

INSERT INTO `craft_assetfolders` (`id`, `parentId`, `sourceId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, NULL, 1, 'All Images', '', '2015-05-07 23:41:24', '2015-05-07 23:41:24', '46a013f8-66e8-423d-ac71-16501e65b5f8'),
(2, 1, 1, 'build', 'build/', '2015-05-07 23:42:02', '2015-05-07 23:42:02', '9dab803b-624d-4ad7-97c4-93ca63ae0157'),
(3, 2, 1, 'jessereed', 'build/jessereed/', '2015-05-07 23:42:02', '2015-05-07 23:42:02', '3dba81d6-ed50-4542-9345-d87afdd4ed84'),
(4, 3, 1, 'html', 'build/jessereed/html/', '2015-05-07 23:42:02', '2015-05-07 23:42:02', 'e30eeb1e-8542-4bda-a594-aaad24fb89a2'),
(5, 4, 1, 'uploads', 'build/jessereed/html/uploads/', '2015-05-07 23:42:02', '2015-05-07 23:42:02', '0434ee6e-92a7-4fe7-801e-53b5a29d533d');

-- --------------------------------------------------------

--
-- Table structure for table `craft_assetindexdata`
--

CREATE TABLE IF NOT EXISTS `craft_assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sourceId` int(10) NOT NULL,
  `offset` int(10) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(10) DEFAULT NULL,
  `recordId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetindexdata_sessionId_sourceId_offset_unq_idx` (`sessionId`,`sourceId`,`offset`),
  KEY `craft_assetindexdata_sourceId_fk` (`sourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_assetsources`
--

CREATE TABLE IF NOT EXISTS `craft_assetsources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetsources_name_unq_idx` (`name`),
  UNIQUE KEY `craft_assetsources_handle_unq_idx` (`handle`),
  KEY `craft_assetsources_fieldLayoutId_fk` (`fieldLayoutId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `craft_assetsources`
--

INSERT INTO `craft_assetsources` (`id`, `name`, `handle`, `type`, `settings`, `sortOrder`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'All Images', 'allImages', 'Local', '{"path":"\\/home\\/206686\\/domains\\/jessereedfromohio.com\\/html\\/_uploads\\/","url":"http:\\/\\/jessereedfromohio.com.s206686.gridserver.com\\/_uploads\\/"}', 1, 34, '2015-05-07 23:41:24', '2015-05-08 06:36:34', '690ffa9d-2f9d-427b-8427-a379f5a8a93c');

-- --------------------------------------------------------

--
-- Table structure for table `craft_assettransformindex`
--

CREATE TABLE IF NOT EXISTS `craft_assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT NULL,
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_assettransformindex_sourceId_fileId_location_idx` (`sourceId`,`fileId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_assettransforms`
--

CREATE TABLE IF NOT EXISTS `craft_assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(10) DEFAULT NULL,
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `craft_assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_categories`
--

CREATE TABLE IF NOT EXISTS `craft_categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_categories_groupId_fk` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `craft_categorygroups`
--

CREATE TABLE IF NOT EXISTS `craft_categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_categorygroups_handle_unq_idx` (`handle`),
  KEY `craft_categorygroups_structureId_fk` (`structureId`),
  KEY `craft_categorygroups_fieldLayoutId_fk` (`fieldLayoutId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_categorygroups_i18n`
--

CREATE TABLE IF NOT EXISTS `craft_categorygroups_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `urlFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nestedUrlFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_categorygroups_i18n_groupId_locale_unq_idx` (`groupId`,`locale`),
  KEY `craft_categorygroups_i18n_locale_fk` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_content`
--

CREATE TABLE IF NOT EXISTS `craft_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_meta` text COLLATE utf8_unicode_ci,
  `field_links` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_content_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `craft_content_title_idx` (`title`),
  KEY `craft_content_locale_fk` (`locale`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=268 ;

--
-- Dumping data for table `craft_content`
--

INSERT INTO `craft_content` (`id`, `elementId`, `locale`, `title`, `field_description`, `field_meta`, `field_links`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'en_us', NULL, NULL, NULL, NULL, '2014-12-30 09:30:28', '2014-12-30 09:32:19', '1fb2b1d4-5080-4f4a-b7eb-70ac3c0b40d5'),
(7, 13, 'en_us', 'GATO', 'Identity for Bobby Flay''s restaurant, GATO, located in the NoHo neighborhood of Manhattan. Gato combines a mix of Mediterranean inspired dishes with the industrial environment of downtown NYC.', 'Project:\r\n\r\nGato\r\n\r\nClient:\r\n\r\nBobby Flay\r\n\r\nCR DIR:\r\n\r\nMichael Beirut\r\n\r\nFirm:\r\n\r\nPentagram\r\n\r\nPhotos:\r\n\r\nMartin Seck', '&gt; [gatonyc.com](http://gatonyc.com)\r\n\r\n&gt; [pentagram.com](http://http://new.pentagram.com/2014/04/new-work-gato/)', '2015-05-05 19:46:22', '2015-08-22 02:08:23', 'fc6f09dc-6e40-4ec4-a88a-4f4b28648e02'),
(9, 15, 'en_us', 'Saks Fifth Avenue', 'Identity for the 2013 national campaign exploring the theme of ''looking''. A bold and geometric approach allowed for endless variations applied to bags, catalogs, and in-store displays.', 'Project:\r\n\r\n2013 campaign\r\n\r\nClient:\r\n\r\nSaks Fifth Avenue\r\n\r\nCR DIR:\r\n\r\nMichael Beirut\r\n\r\nFirm:\r\n\r\nPentagram', '&gt; [pentagram blog](http://http://new.pentagram.com/2013/09/new-work-saks-fifth-avenue-look-campaign/)', '2015-05-05 20:26:19', '2015-08-22 02:16:03', 'c3a8b0e2-b05b-48b8-9bf4-cf0b62c140f6'),
(39, 56, 'en_us', 'MS GE 30-copy web 960', NULL, NULL, NULL, '2015-06-01 04:56:30', '2015-06-01 04:56:30', '0600c96b-62bf-4f0d-8607-e5a5e75cc32d'),
(40, 58, 'en_us', 'MS GE 24-copy web 960', NULL, NULL, NULL, '2015-06-01 04:57:42', '2015-06-01 04:57:42', '6f8958a4-f43d-4b9d-a6a6-2813790f2d9b'),
(41, 59, 'en_us', 'MS GE 17-copy web 960', NULL, NULL, NULL, '2015-06-01 04:57:43', '2015-06-01 04:57:43', '28e0263c-d0da-4dcc-a64e-cf0d8cf0d165'),
(53, 73, 'en_us', 'NYCTA Graphics Standards Manual Reissue', 'FULL-SIZE REISSUE OF THE 1970 NYC TRANSIT AUTHORITY GRAPHICS STANDARDS MANUAL. FOREWORD BY MICHAEL BIERUT, PENTAGRAM. ESSAY BY CHRISTOPHER BONANOS, NY MAG. IN COLLABORATION WITH HAMISH SMYTH.', 'Project:\r\n\r\nNYCTA GRAPHICS STANDARDS MANUAL REISSUE\r\n\r\nPhotos:\r\n\r\nBRIAN KELLEY', '&gt; [thestandardsmanual.com](http://thestandardsmanual.com)\r\n\r\n&gt; [kickstarter campaign](http://https://www.kickstarter.com/projects/thestandardsmanual/full-size-reissue-of-the-nycta-graphics-standards/description)', '2015-07-13 21:05:33', '2015-08-22 02:29:17', '4793e641-3064-4776-8798-adcfaa4d693d'),
(118, 159, 'en_us', 'nycta graphics standards manual  detail 2 WEB', NULL, NULL, NULL, '2015-07-21 01:57:03', '2015-07-21 01:57:03', '4b73f9ba-41e4-4c9b-b66a-4d61b107813d'),
(119, 160, 'en_us', 'nycta graphics standards manual 2 WEB', NULL, NULL, NULL, '2015-07-21 01:57:54', '2015-07-21 01:57:54', '9237b050-9ca8-42b6-8e28-208e0bddd1e6'),
(120, 161, 'en_us', 'nycta graphics standards manual 4 WEB', NULL, NULL, NULL, '2015-07-21 01:57:58', '2015-07-21 01:57:58', 'daf5239c-86cb-4364-8f62-8f989d721a81'),
(121, 162, 'en_us', 'nycta graphics standards manual 7 WEB', NULL, NULL, NULL, '2015-07-21 01:58:01', '2015-07-21 01:58:01', 'd573aa27-a89c-451f-8da4-e9002d1239b0'),
(122, 163, 'en_us', 'nycta graphics standards manual 8 WEB', NULL, NULL, NULL, '2015-07-21 01:58:05', '2015-07-21 01:58:05', 'ee9d6621-8f08-4cb4-af22-00923a91cbb1'),
(123, 164, 'en_us', 'nycta graphics standards manual 13 WEB', NULL, NULL, NULL, '2015-07-21 01:58:10', '2015-07-21 01:58:10', 'd4e473da-1b9d-40c9-8b09-262fbc04f894'),
(124, 165, 'en_us', 'nycta graphics standards manual 14 WEB', NULL, NULL, NULL, '2015-07-21 01:58:14', '2015-07-21 01:58:14', 'd75f5af0-76dc-473b-9ad4-c15363e64743'),
(125, 166, 'en_us', 'nycta graphics standards manual 16 WEB', NULL, NULL, NULL, '2015-07-21 01:58:19', '2015-07-21 01:58:19', 'f3f24b6b-1de2-4408-983b-a3f97bbd5d68'),
(126, 167, 'en_us', 'nycta graphics standards manual 20 WEB', NULL, NULL, NULL, '2015-07-21 01:58:23', '2015-07-21 01:58:23', '75f15069-b4c4-4d81-bb04-a9d07b71a505'),
(127, 168, 'en_us', 'nycta graphics standards manual 26 WEB', NULL, NULL, NULL, '2015-07-21 01:58:29', '2015-07-21 01:58:29', '7f7b6e22-4196-4069-88f7-d8e7f0f0093f'),
(128, 169, 'en_us', 'nycta graphics standards manual 30 WEB', NULL, NULL, NULL, '2015-07-21 01:58:33', '2015-07-21 01:58:33', '95ef9403-6f2d-47e8-86c9-393df1722ff2'),
(129, 170, 'en_us', 'nycta graphics standards manual 39 WEB', NULL, NULL, NULL, '2015-07-21 01:58:36', '2015-07-21 01:58:36', 'cd2dbec0-4960-44da-9490-1760a6bb48fa'),
(130, 171, 'en_us', 'nycta graphics standards manual 43 WEB', NULL, NULL, NULL, '2015-07-21 01:58:41', '2015-07-21 01:58:41', 'bada45e7-04b6-427d-a619-1824f16932a0'),
(131, 172, 'en_us', 'nycta graphics standards manual 44 WEB', NULL, NULL, NULL, '2015-07-21 01:58:46', '2015-07-21 01:58:46', 'c5160a2d-07e8-41ce-bc96-8327ed831286'),
(132, 173, 'en_us', 'nycta graphics standards manual 45 WEB', NULL, NULL, NULL, '2015-07-21 01:58:50', '2015-07-21 01:58:50', '913430df-2bd6-44f4-8e97-bc7690b4d433'),
(133, 174, 'en_us', 'nycta graphics standards manual 46 WEB', NULL, NULL, NULL, '2015-07-21 01:58:55', '2015-07-21 01:58:55', 'de89a97c-45e4-46aa-bdc1-77ffc960c802'),
(134, 175, 'en_us', 'nycta graphics standards manual 47 WEB', NULL, NULL, NULL, '2015-07-21 01:58:59', '2015-07-21 01:58:59', 'f2b202e8-5d39-4330-8fa7-2e39a0ce50b0'),
(135, 176, 'en_us', 'nycta graphics standards manual 48 WEB', NULL, NULL, NULL, '2015-07-21 01:59:03', '2015-07-21 01:59:03', 'cc671ea2-3478-4a20-86b9-4d1c6fef3b30'),
(136, 177, 'en_us', 'nycta graphics standards manual cover WEB', NULL, NULL, NULL, '2015-07-21 01:59:08', '2015-07-21 01:59:08', 'd06d75b2-1926-417d-b842-42337d87e5b8'),
(137, 178, 'en_us', 'nycta graphics standards manual detail 5 WEB', NULL, NULL, NULL, '2015-07-21 01:59:13', '2015-07-21 01:59:13', '419a757d-bfa6-470c-a372-01c68174e7c9'),
(138, 179, 'en_us', 'nycta graphics standards manual detail 7 WEB', NULL, NULL, NULL, '2015-07-21 01:59:17', '2015-07-21 01:59:17', 'a80a5eb5-a01f-46cd-babb-e49159f7845e'),
(139, 180, 'en_us', 'nycta graphics standards manual detail 10 WEB', NULL, NULL, NULL, '2015-07-21 01:59:20', '2015-07-21 01:59:20', '50dd0713-8b09-49e3-ba38-566d82345ada'),
(140, 181, 'en_us', 'nycta graphics standards manual detail 16 WEB', NULL, NULL, NULL, '2015-07-21 01:59:25', '2015-07-21 01:59:25', '3e18f089-97c6-49e1-aaee-cb5f5f012c92'),
(141, 182, 'en_us', 'nycta graphics standards manual detail 17 WEB', NULL, NULL, NULL, '2015-07-21 01:59:31', '2015-07-21 01:59:31', '6f40455a-2c99-490b-aea5-ae9c85856112'),
(142, 183, 'en_us', 'nycta graphics standards manual detail 25 WEB', NULL, NULL, NULL, '2015-07-21 01:59:36', '2015-07-21 01:59:36', 'e6225676-d988-451d-8acc-a30430909575'),
(143, 184, 'en_us', 'nycta graphics standards manual 1 WEB', NULL, NULL, NULL, '2015-07-21 02:05:35', '2015-07-21 02:05:35', '52f8eb89-4b9f-4206-9769-0bd763cab7c6'),
(144, 185, 'en_us', 'GATO WEB 1', NULL, NULL, NULL, '2015-07-21 02:11:36', '2015-07-21 02:11:36', 'ecf332ba-8b12-47c6-b280-87e64d1a3bbf'),
(145, 186, 'en_us', 'GATO WEB 2', NULL, NULL, NULL, '2015-07-21 02:11:38', '2015-07-21 02:11:38', '567aa202-ffa3-41a4-af45-43cf4f31376f'),
(146, 187, 'en_us', 'GATO WEB 3', NULL, NULL, NULL, '2015-07-21 02:11:40', '2015-07-21 02:11:40', 'ca8245de-1bde-4feb-8fa0-831f2b31051b'),
(147, 188, 'en_us', 'GATO WEB 4', NULL, NULL, NULL, '2015-07-21 02:11:42', '2015-07-21 02:11:42', 'c8edd9e3-827b-4401-8c9b-5444ed7aa8e5'),
(148, 189, 'en_us', 'GATO WEB 5', NULL, NULL, NULL, '2015-07-21 02:11:44', '2015-07-21 02:11:44', 'f0acb365-d0d6-4358-aa7a-b822e70f93ec'),
(149, 190, 'en_us', 'GATO WEB 6', NULL, NULL, NULL, '2015-07-21 02:11:46', '2015-07-21 02:11:46', '2b6a8fae-3601-4635-b34c-4f9100cc6de4'),
(150, 191, 'en_us', 'GATO WEB 7', NULL, NULL, NULL, '2015-07-21 02:11:48', '2015-07-21 02:11:48', '4858837d-11e8-45db-8974-e6dbfced1ef5'),
(151, 192, 'en_us', 'GATO WEB 8', NULL, NULL, NULL, '2015-07-21 02:11:50', '2015-07-21 02:11:50', '23928b12-20c9-462f-a36a-0e5291c801c9'),
(152, 193, 'en_us', 'GATO WEB 9', NULL, NULL, NULL, '2015-07-21 02:11:52', '2015-07-21 02:11:52', '0717b8a2-d3dc-47de-9372-7e84312438d2'),
(153, 194, 'en_us', 'GATO WEB 10', NULL, NULL, NULL, '2015-07-21 02:11:54', '2015-07-21 02:11:54', 'b4e47180-20c5-495a-9938-c06b86b589e9'),
(154, 195, 'en_us', 'GATO WEB 11', NULL, NULL, NULL, '2015-07-21 02:11:55', '2015-07-21 02:11:55', '4f573e8c-908a-446d-ae2a-70e8a5641f3f'),
(155, 196, 'en_us', 'GATO WEB 12', NULL, NULL, NULL, '2015-07-21 02:11:58', '2015-07-21 02:11:58', 'd51cf85c-66a9-45cc-8624-54a9a0281b7b'),
(156, 197, 'en_us', 'GATO WEB 13', NULL, NULL, NULL, '2015-07-21 02:12:00', '2015-07-21 02:12:00', '80696015-10b5-458e-a1d0-4bd20aa9c7e9'),
(157, 198, 'en_us', 'GATO WEB 14', NULL, NULL, NULL, '2015-07-21 02:12:01', '2015-07-21 02:12:01', 'd59082cb-4d37-4fc8-8b70-4d25c1e5994f'),
(158, 199, 'en_us', 'GATO WEB 15', NULL, NULL, NULL, '2015-07-21 02:12:03', '2015-07-21 02:12:03', '48315684-b605-432b-b416-7c2a6f26f9bf'),
(159, 200, 'en_us', 'GATO WEB 16', NULL, NULL, NULL, '2015-07-21 02:12:05', '2015-07-21 02:12:05', '230389bf-a42d-4ea2-8934-61ff6524e280'),
(160, 201, 'en_us', 'GATO WEB 17rev', NULL, NULL, NULL, '2015-07-21 02:12:07', '2015-07-21 02:12:07', 'db989e3f-c5a5-4405-b571-aadfb9f3f2b9'),
(161, 202, 'en_us', 'GATO WEB 18', NULL, NULL, NULL, '2015-07-21 02:12:10', '2015-07-21 02:12:10', '686a3f2c-4d06-4a57-8ba8-ccef66c4d265'),
(162, 203, 'en_us', 'SAKS BAG STREET 5 TRIX WEB', NULL, NULL, NULL, '2015-07-21 02:37:04', '2015-07-21 02:37:04', 'bf53acaa-8039-4695-bef5-55c6a2a5980a'),
(163, 204, 'en_us', 'SAKS BAG STREET 8 TRIX WEB', NULL, NULL, NULL, '2015-07-21 02:37:06', '2015-07-21 02:37:06', '7eb80a98-885a-4307-a7f6-5692a2732157'),
(164, 205, 'en_us', 'SAKS BAG STREET 9 TRIX WEB', NULL, NULL, NULL, '2015-07-21 02:37:08', '2015-07-21 02:37:08', 'fe7bc277-748b-4852-8636-2a29e61d284b'),
(165, 206, 'en_us', 'SAKS BAG STREET 16 TRIX WEB', NULL, NULL, NULL, '2015-07-21 02:37:11', '2015-07-21 02:37:11', 'eedf45cd-dc46-49a9-953f-2b0df1644257'),
(166, 207, 'en_us', 'SAKS LOOK MARK', NULL, NULL, NULL, '2015-07-21 02:37:13', '2015-07-21 02:37:13', '00282050-20b6-46af-a938-274514e940c0'),
(167, 208, 'en_us', 'SFA LOOK CATAGLOGUE AUG1 TRIX WEB', NULL, NULL, NULL, '2015-07-21 02:37:15', '2015-07-21 02:37:15', '7eb76691-e787-4b8d-9234-2898be406805'),
(168, 209, 'en_us', 'SFA LOOK CATAGLOGUE JUNE 1 TRIX WEB', NULL, NULL, NULL, '2015-07-21 02:37:18', '2015-07-21 02:37:18', 'be68ae1a-c6a5-49e0-8457-ee0f6af6b569'),
(169, 210, 'en_us', 'SFA LOOK CATAGLOGUE NEW1 TRIX WEB', NULL, NULL, NULL, '2015-07-21 02:37:20', '2015-07-21 02:37:20', '66ded884-5c8f-4feb-9c30-20de73c33c8d'),
(170, 211, 'en_us', 'SFA LOOK CATAGLOGUE NEW2 TRIX WEB', NULL, NULL, NULL, '2015-07-21 02:37:22', '2015-07-21 02:37:22', '4731f71e-a4d4-4fcd-8cfc-76835ff76c59'),
(171, 212, 'en_us', 'SAKS BAG STREET 4 TRIX WEB', NULL, NULL, NULL, '2015-07-21 02:39:47', '2015-07-21 02:39:47', 'a4be31fc-219e-4836-96aa-9e148c7934a2'),
(172, 215, 'en_us', 'German Expressionism: The Graphic Impulse', 'Exhibition identity for ''German Expressionism: The Graphic Impulse'' — a collection of prints, painting, and sculpture created in Germany and Austria during the earlier decades of the 20th century.', 'Project:\r\n\r\nGerman Expressionism: The Graphic Impulse\r\n\r\nClient:\r\n\r\nMuseum of Modern Art (MoMA)\r\n\r\nCR DIR:\r\n\r\nJulia Hoffmann\r\n\r\nAR DIR:\r\n\r\nBrigitta Bungard\r\n\r\nPhotos:\r\n\r\nMartin Seck', '', '2015-07-21 03:12:40', '2015-08-20 18:46:55', '9d71c767-a8c4-4c14-b392-84488d13e635'),
(173, 216, 'en_us', 'MS GE 17 WEB', NULL, NULL, NULL, '2015-07-21 03:21:31', '2015-07-21 03:21:31', '895df72a-bcf7-4df4-87c6-a14bfaad67fb'),
(174, 217, 'en_us', 'MS GE 21 WEB', NULL, NULL, NULL, '2015-07-21 03:21:35', '2015-07-21 03:21:35', '6aeb5d7e-9011-4ee0-be44-3e15f4ba06d9'),
(175, 218, 'en_us', 'MS GE 24 WEB', NULL, NULL, NULL, '2015-07-21 03:21:40', '2015-07-21 03:21:40', 'fc881977-d32b-41b9-a550-c4c7c50e5bb6'),
(176, 219, 'en_us', 'MS GE 26 WEB', NULL, NULL, NULL, '2015-07-21 03:21:43', '2015-07-21 03:21:43', 'bec7a4fe-9993-47a4-b493-dff17b269003'),
(177, 220, 'en_us', 'MS GE 30 WEB', NULL, NULL, NULL, '2015-07-21 03:21:47', '2015-07-21 03:21:47', '5b21db1d-3d0b-48c1-8d6d-74fa1db192a7'),
(178, 221, 'en_us', 'MS GE 36 WEB', NULL, NULL, NULL, '2015-07-21 03:21:50', '2015-07-21 03:21:50', '6ed15c91-ec00-4593-bbdd-9a851c91eff2'),
(179, 222, 'en_us', 'ms south-africa german-express 5481 WEB', NULL, NULL, NULL, '2015-07-21 03:21:53', '2015-07-21 03:21:53', '2829d4f5-3765-44f4-8be6-f9c63bdb54ad'),
(180, 223, 'en_us', 'ms south-africa german-express 5484 WEB', NULL, NULL, NULL, '2015-07-21 03:21:56', '2015-07-21 03:21:56', '08df179f-30db-40c2-bec8-56a17aed8e5e'),
(181, 224, 'en_us', 'ms south-africa german-express 5487 WEB', NULL, NULL, NULL, '2015-07-21 03:21:59', '2015-07-21 03:21:59', 'eb8887d7-754b-4819-8ec5-4a59d610be85'),
(182, 225, 'en_us', 'ms south-africa german-express 5490 WEB', NULL, NULL, NULL, '2015-07-21 03:22:02', '2015-07-21 03:22:02', 'a4463ea4-4e07-46ee-b603-b015fc2056fe'),
(183, 226, 'en_us', 'ms south-africa german-express 5499 WEB', NULL, NULL, NULL, '2015-07-21 03:22:04', '2015-07-21 03:22:04', '55724bad-695b-4f46-9e81-6ea36b1df316'),
(184, 227, 'en_us', 'ms south-africa german-express 5509 WEB', NULL, NULL, NULL, '2015-07-21 03:22:09', '2015-07-21 03:22:09', '029082ff-1f8c-4941-85f1-62ed8a908507'),
(185, 228, 'en_us', 'ms south-africa german-express 5511 WEB', NULL, NULL, NULL, '2015-07-21 03:22:11', '2015-07-21 03:22:11', '9bba136f-d38c-42b8-b601-ff4b6a0ea449'),
(186, 229, 'en_us', 'ms south-africa german-express 5518 WEB', NULL, NULL, NULL, '2015-07-21 03:22:19', '2015-07-21 03:22:19', '8c975c4c-bb52-45ac-b8fa-1a4948a1d102'),
(187, 230, 'en_us', 'ms south-africa german-express 5526 WEB', NULL, NULL, NULL, '2015-07-21 03:22:27', '2015-07-21 03:22:27', '945f9857-f222-4a4b-9ccc-a15c40637613'),
(188, 231, 'en_us', 'ms south-africa german-express 5561 WEB', NULL, NULL, NULL, '2015-07-21 03:22:30', '2015-07-21 03:22:30', 'd5f42a43-48f6-4593-b0e6-84212f201716'),
(189, 232, 'en_us', 'ms south-africa german-express 5585 WEB', NULL, NULL, NULL, '2015-07-21 03:22:33', '2015-07-21 03:22:33', '53503004-70a5-4de2-a53d-0c77a63c3935'),
(190, 242, 'en_us', 'Coyote v Acme 01 WEB', NULL, NULL, NULL, '2015-07-23 23:03:47', '2015-07-23 23:03:47', '8ec6c9e5-f1d4-4505-a359-0a7348de7c71'),
(191, 243, 'en_us', 'Coyote v Acme 02 WEB', NULL, NULL, NULL, '2015-07-23 23:03:48', '2015-07-23 23:03:48', 'd7054d28-71b9-478e-b710-7c53c8b45b58'),
(192, 244, 'en_us', 'Coyote v Acme 03 WEB', NULL, NULL, NULL, '2015-07-23 23:03:49', '2015-07-23 23:03:49', 'a3445305-c2d4-4821-b616-4f5b597fcd90'),
(193, 245, 'en_us', 'Coyote v Acme 06 WEB', NULL, NULL, NULL, '2015-07-23 23:03:50', '2015-07-23 23:03:50', '09530d0e-e4f3-48d8-9dae-b598f6917320'),
(194, 246, 'en_us', 'Coyote v Acme 11 WEB', NULL, NULL, NULL, '2015-07-23 23:03:52', '2015-07-23 23:03:52', '0deccaa1-6ab7-4cc1-a396-31b3b90aa0fd'),
(195, 247, 'en_us', 'Coyote v Acme 13 WEB', NULL, NULL, NULL, '2015-07-23 23:03:53', '2015-07-23 23:03:53', '36391a38-a370-465d-9b5b-49724788b00e'),
(196, 248, 'en_us', 'Coyote v Acme 16 WEB', NULL, NULL, NULL, '2015-07-23 23:03:55', '2015-07-23 23:03:55', 'b81d4202-2cd7-48d6-bb1b-73645b328acc'),
(197, 249, 'en_us', 'Coyote v Acme 18 WEB', NULL, NULL, NULL, '2015-07-23 23:03:56', '2015-07-23 23:03:56', 'eb859b55-bf61-40cf-baa9-afbdf7de548e'),
(198, 250, 'en_us', 'Coyote v Acme 19 WEB', NULL, NULL, NULL, '2015-07-23 23:03:58', '2015-07-23 23:03:58', 'bebd4231-9d61-4262-9831-f08fd4e2e4fe'),
(199, 251, 'en_us', 'Coyote v Acme 20 WEB', NULL, NULL, NULL, '2015-07-23 23:04:00', '2015-07-23 23:04:00', 'b7270b67-fb4a-4791-81fa-60a0b8738e7f'),
(200, 252, 'en_us', 'Coyote v Acme 23 WEB', NULL, NULL, NULL, '2015-07-23 23:04:02', '2015-07-23 23:04:02', 'b30b89b2-a218-45d1-97ca-ee34b47560f3'),
(201, 253, 'en_us', 'Coyote v Acme 28 WEB', NULL, NULL, NULL, '2015-07-23 23:04:03', '2015-07-23 23:04:03', '443a92b6-663b-4f4d-b157-965186e10c7b'),
(202, 254, 'en_us', 'Coyote v Acme 29 WEB', NULL, NULL, NULL, '2015-07-23 23:04:05', '2015-07-23 23:04:05', 'c5b956fe-bda0-40e1-91bc-61eef5b689ec'),
(203, 255, 'en_us', 'Coyote v. Acme', 'Originally written by Ian Friazier for The New Yorker, this hypothetical lawsuit against Wile E. Coyote versus the Acme corporation has been meticulously illustrated by Pentagram partner Daniel Weill.', 'Project:\r\n\r\nCoyote v. Acme\r\n\r\nClient:\r\n\r\nPentagram\r\n\r\nCR DIR:\r\n\r\nMichael Beirut\r\n\r\nFirm:\r\n\r\nPentagram\r\n\r\nPhotos:\r\n\r\nMartin Seck', '&gt; [Pentagram](http://new.pentagram.com/2014/01/new-work-coyote-v-acme/)\r\n\r\n&gt; [Core 77](http://www.core77.com/posts/26324/safety-first-pentagram-cleverly-absolves-acme-in-a-design-fiction-starring-wile-e-coyote-26324)', '2015-07-23 23:17:50', '2015-08-20 18:47:17', 'dfc0e8f7-c40a-4d6a-97ad-a8b542bbfd9d'),
(204, 262, 'en_us', 'BIGAPPS SYMBOL WEB', NULL, NULL, NULL, '2015-07-23 23:33:49', '2015-07-23 23:33:49', '845f7a61-2a3b-4e2c-9286-4ea7c62c3fa4'),
(205, 263, 'en_us', 'bigapps program WEB', NULL, NULL, NULL, '2015-07-23 23:40:15', '2015-07-23 23:40:15', '6361a792-3686-481e-a5dc-a8946ba143f9'),
(206, 264, 'en_us', 'bigapps mayor WEB', NULL, NULL, NULL, '2015-07-23 23:40:36', '2015-07-23 23:40:36', '611f0bc7-5b3b-4fa5-ada7-75d5f335a55e'),
(207, 265, 'en_us', 'bigapps poster detail WEB', NULL, NULL, NULL, '2015-07-23 23:40:49', '2015-07-23 23:40:49', 'fcb0f626-51ef-4452-885a-aef6a55bc748'),
(208, 266, 'en_us', 'Big Apps NYC', 'Identity for Big Apps NYC, a competition aimed at building better technology for the residents of New York City through open sourced data. ', 'Project:\r\n\r\nBig Apps NYC\r\n\r\nClient:\r\n\r\nCity of New York\r\n\r\nCR DIR:\r\n\r\nMichael Beirut\r\n\r\nFirm:\r\n\r\nPentagram', '&gt; [Pentagram](http://new.pentagram.com/2013/07/nyc-bigapps-2013-winners-announced/)\r\n\r\n&gt; [bigapps.nyc](http://bigapps.nyc)', '2015-07-23 23:41:04', '2015-08-22 02:10:43', 'cae68963-d12b-47d2-8638-6c915d719463'),
(209, 267, 'en_us', 'bigapps poster v4 WEB', NULL, NULL, NULL, '2015-07-23 23:41:23', '2015-07-23 23:41:23', '74d1d78e-ae89-4945-a242-8f1570cd421f'),
(210, 268, 'en_us', 'bigapps screen circles web', NULL, NULL, NULL, '2015-07-23 23:41:30', '2015-07-23 23:41:30', '715666d1-8e15-47cc-a67b-8fbd957bbe03'),
(211, 269, 'en_us', 'BigApps Bus-Shelter 2014 11 CC WEB', NULL, NULL, NULL, '2015-07-23 23:50:12', '2015-07-23 23:50:12', '21d82911-b4e6-4f91-95ca-7751d2ffedfb'),
(212, 270, 'en_us', 'BigApps Bus-Shelter 2014 50 WEB', NULL, NULL, NULL, '2015-07-23 23:50:24', '2015-07-23 23:50:24', 'd107b3c8-d3f1-4ae3-9514-88d6b9e5ab4e'),
(213, 271, 'en_us', 'bigapps button WEB', NULL, NULL, NULL, '2015-07-23 23:53:20', '2015-07-23 23:53:20', 'c157d695-cc99-4c2c-929e-d08f3d6e85ea'),
(214, 272, 'en_us', 'BIGAPPS BLOOM WEB', NULL, NULL, NULL, '2015-07-23 23:56:16', '2015-07-23 23:56:16', 'dbb076cc-16b7-4a20-a0d9-da1d7de7bada'),
(215, 279, 'en_us', 'JessicaJorgensen-AbsintheMakesTheHeartGrowFonder web', NULL, NULL, NULL, '2015-08-01 04:08:47', '2015-08-01 04:08:47', '97c5f30c-b538-43e7-ae0e-675d2d3d8911'),
(216, 280, 'en_us', 'Walk NYC', 'Icon design for the New York City pedestrian wayfinding program, Walk NYC. The signs are positioned on the street level throughout all five boroughs of NYC. ', 'Project:\r\n\r\nWalk NYC\r\n\r\nClient:\r\n\r\nDOT NYC\r\n\r\nCR DIR:\r\n\r\nMichael Beirut\r\n\r\nDESIGN:\r\n\r\nHamish Smyth / Tracey Cameron\r\n\r\nFirm:\r\n\r\nPentagram\r\n\r\nPLANNING:\r\n\r\nCity ID\r\n\r\nIND DES:\r\n\r\nT-Kartor\r\n\r\nPhotos:\r\n\r\nMartin Seck', '&gt; [pentagram](http://new.pentagram.com/2013/06/new-work-nyc-wayfinding/)\r\n\r\n&gt; [pentacity group](http://www.pentacitygroup.com)', '2015-08-07 17:38:17', '2015-08-22 02:00:58', '4d594dd3-8828-47fd-8316-fe43487bd8b4'),
(217, 281, 'en_us', 'DOT Taxi 140407 WEB', NULL, NULL, NULL, '2015-08-07 17:42:48', '2015-08-07 17:42:48', 'ec05e701-41a8-4a2b-84e7-1d3caca79214'),
(218, 282, 'en_us', 'DOT TruckLoading2 140407 WEB', NULL, NULL, NULL, '2015-08-07 17:46:16', '2015-08-07 17:46:16', '14b6ef24-5d50-40a9-90d9-1c0255c9ef91'),
(220, 285, 'en_us', 'DOT ICONS', NULL, NULL, NULL, '2015-08-16 21:14:13', '2015-08-16 21:56:00', '2442ffb9-bd9f-467d-b67a-2f8ad5ce9429'),
(222, 287, 'en_us', 'DOT ICONS2', NULL, NULL, NULL, '2015-08-16 21:55:13', '2015-08-16 21:55:13', 'eb18b09b-0a14-4dd1-bee0-7faf58879e7c'),
(223, 288, 'en_us', 'DOT ICONS3', NULL, NULL, NULL, '2015-08-16 21:55:15', '2015-08-16 21:55:15', '43e42d93-fc47-4aa8-99d7-2f085446c59c'),
(224, 289, 'en_us', 'DOT ICONS4', NULL, NULL, NULL, '2015-08-16 21:55:16', '2015-08-16 21:55:16', 'ab80c436-cbee-451b-8091-18963a5fdb5c'),
(225, 290, 'en_us', 'DOT ICONS5', NULL, NULL, NULL, '2015-08-16 21:55:18', '2015-08-16 21:55:18', '546d5f0d-9eb8-4939-af1f-e9f3112970fb'),
(226, 291, 'en_us', 'DOT ICONS6', NULL, NULL, NULL, '2015-08-16 21:55:20', '2015-08-16 21:55:20', 'e81d4525-e1b4-4866-8b05-2a64865a85b9'),
(227, 292, 'en_us', 'DOT ICONS7', NULL, NULL, NULL, '2015-08-16 21:55:22', '2015-08-16 21:55:22', '69d6ae7e-1f48-4d79-92a8-3e55585d8a03'),
(228, 293, 'en_us', 'DOT ICONS8', NULL, NULL, NULL, '2015-08-16 21:55:23', '2015-08-16 21:55:23', '28f7e512-35a8-4e7a-af08-9c24e28614bc'),
(229, 294, 'en_us', 'DOT ICONS9', NULL, NULL, NULL, '2015-08-16 21:55:24', '2015-08-16 21:55:24', '9fea9cfe-eef5-41a5-ad2e-0ea467ce1637'),
(230, 295, 'en_us', 'DOT ICONS10', NULL, NULL, NULL, '2015-08-16 21:55:26', '2015-08-16 21:55:26', 'fc6b9a6b-a205-445d-88f2-a82f55e98b2e'),
(231, 296, 'en_us', 'DOT ICONS11', NULL, NULL, NULL, '2015-08-16 21:55:27', '2015-08-16 21:55:27', 'e2110f69-8add-414a-893b-8e75e68d2b3c'),
(232, 297, 'en_us', 'DOT ICONS12', NULL, NULL, NULL, '2015-08-16 21:55:30', '2015-08-16 21:55:30', '5da2fec2-3459-495d-83eb-5e97b743fbf7'),
(233, 298, 'en_us', 'DOT ICONS13', NULL, NULL, NULL, '2015-08-16 21:55:31', '2015-08-16 21:55:31', 'f918f921-822e-4d6f-a047-bbdce4846766'),
(234, 299, 'en_us', 'DOT ICONS14', NULL, NULL, NULL, '2015-08-16 21:55:33', '2015-08-16 21:55:33', '38174202-3ad4-4e62-b2b4-2db0660213f9'),
(235, 300, 'en_us', 'DOT ICONS15', NULL, NULL, NULL, '2015-08-16 21:55:35', '2015-08-16 21:55:35', '4b884236-e485-46fa-8bd0-03fa3f6233f7'),
(236, 301, 'en_us', 'DOT ICONS16', NULL, NULL, NULL, '2015-08-16 21:55:36', '2015-08-16 21:55:36', 'de5f7c75-8a58-4dfc-b8e8-a3133b503d92'),
(237, 302, 'en_us', 'DOT ICONS17', NULL, NULL, NULL, '2015-08-16 21:55:37', '2015-08-16 21:55:37', 'a80b8945-5bb4-4ffe-b679-58ac288be00e'),
(238, 303, 'en_us', 'DOT ICONS18', NULL, NULL, NULL, '2015-08-16 21:55:39', '2015-08-16 21:55:39', '89f665a4-49f2-4b86-a419-4ddafb8220af'),
(239, 304, 'en_us', 'DOT ICONS19', NULL, NULL, NULL, '2015-08-16 21:55:40', '2015-08-16 21:55:40', 'f3cd1d8e-38aa-4fe7-8d86-9d357331f13a'),
(240, 305, 'en_us', 'DOT ICONS20', NULL, NULL, NULL, '2015-08-16 21:55:42', '2015-08-16 21:55:42', 'a3b4917d-0a34-4e38-879f-8bbe8ba3e4e6'),
(241, 306, 'en_us', 'DOT ICONS21', NULL, NULL, NULL, '2015-08-16 21:55:44', '2015-08-16 21:55:44', '14dac6b5-0348-45e0-897b-76511e60cdbe'),
(242, 307, 'en_us', 'DOT ICONS22', NULL, NULL, NULL, '2015-08-16 21:55:46', '2015-08-16 21:55:46', '1417c0cf-0307-48b9-9a35-460416de45a6'),
(243, 308, 'en_us', 'DOT ICONS23', NULL, NULL, NULL, '2015-08-16 21:55:47', '2015-08-16 21:55:47', 'ab313cad-e0e5-4b1a-aea9-3db7e1a8f301'),
(244, 311, 'en_us', 'DOT SIGN CHINATOWN WEB', NULL, NULL, NULL, '2015-08-18 13:29:18', '2015-08-18 13:29:18', '55fe884a-e540-4b1d-bce0-956a7b523c8f'),
(245, 312, 'en_us', 'DOT TYPE', NULL, NULL, NULL, '2015-08-18 13:46:46', '2015-08-18 13:46:46', '400c8ffd-b343-4a50-834f-28d09c33adc0'),
(246, 313, 'en_us', 'DOT TYPE2', NULL, NULL, NULL, '2015-08-18 13:46:47', '2015-08-18 13:46:47', 'fcbc08bc-cf85-4755-8529-266b2681f8e1'),
(247, 318, 'en_us', 'DOT SECK1', NULL, NULL, NULL, '2015-08-19 02:43:13', '2015-08-19 02:43:13', '41aac30c-8bdc-4523-aaea-7abd058d3b45'),
(248, 319, 'en_us', 'DOT SECK2', NULL, NULL, NULL, '2015-08-19 02:43:15', '2015-08-19 02:43:15', '980c9b4d-d83f-4034-9232-7c87385a3714'),
(249, 320, 'en_us', 'DOT SECK3', NULL, NULL, NULL, '2015-08-19 02:43:17', '2015-08-19 02:43:17', '14bdb785-d5d0-4aa7-aaa9-16f90340758a'),
(250, 321, 'en_us', 'DOT SECK4', NULL, NULL, NULL, '2015-08-19 02:43:19', '2015-08-19 02:43:19', 'b18bd5a2-a016-4fb7-a3a4-3b7d4f7cf84c'),
(251, 325, 'en_us', 'DOT RATION', NULL, NULL, NULL, '2015-08-19 03:35:05', '2015-08-19 03:35:05', 'fbf323e5-da6a-46ec-944f-bd1a97124cb5'),
(252, 326, 'en_us', 'DOT GRID', NULL, NULL, NULL, '2015-08-19 03:42:05', '2015-08-19 03:42:05', '59c24031-9e65-45de-bf9e-8020391429b1'),
(253, 327, 'en_us', 'DOT TYPE MED', NULL, NULL, NULL, '2015-08-19 03:52:10', '2015-08-19 03:52:10', 'fa7eaa78-080e-4425-bcd0-db53d532f95f'),
(254, 328, 'en_us', 'DOT TYPE MED2', NULL, NULL, NULL, '2015-08-19 03:52:14', '2015-08-19 03:52:14', 'ed01f3be-9b01-4ecc-9526-88ea414c7d6c'),
(255, 331, 'en_us', 'JRFO LOGOTYPE CROPS', NULL, NULL, NULL, '2015-08-20 19:36:37', '2015-08-20 19:36:37', 'd6cfa6fb-1a3b-4ccc-8e58-ad993958d43a'),
(256, 332, 'en_us', 'JRFO LOGOTYPE CROPS2', NULL, NULL, NULL, '2015-08-20 19:36:40', '2015-08-20 19:36:40', '3f5ac0f5-163a-4ba3-8cab-4569b3ec5eb8'),
(257, 333, 'en_us', 'JRFO LOGOTYPE', NULL, NULL, NULL, '2015-08-20 19:36:44', '2015-08-20 19:36:44', '621b7ab7-8f3f-42a6-badd-789a80609505'),
(258, 334, 'en_us', 'JRFO LOGOTYPE2', NULL, NULL, NULL, '2015-08-20 19:36:47', '2015-08-20 19:36:47', '76cc49d3-5061-40fe-a46f-2ea7e272d3f4'),
(259, 335, 'en_us', 'JRFO LOGOTYPE3', NULL, NULL, NULL, '2015-08-20 19:36:50', '2015-08-20 19:36:50', '0290515a-8a76-4a38-a194-b02645543aa4'),
(260, 336, 'en_us', 'JRFO LOGOTYPE4', NULL, NULL, NULL, '2015-08-20 19:36:54', '2015-08-20 19:36:54', 'ad876f9c-7ce8-4239-ab1e-8dab5a4fe789'),
(261, 337, 'en_us', 'JRFO LOGOTYPE5', NULL, NULL, NULL, '2015-08-20 19:36:57', '2015-08-20 19:36:57', '7d56dc09-fff5-4d64-bab0-a57e0f5f38b0'),
(262, 338, 'en_us', 'JRFO LOGOTYPE6', NULL, NULL, NULL, '2015-08-20 19:37:00', '2015-08-20 19:37:00', '5da00b28-3b80-4e4c-a23a-fdb850a0d021'),
(263, 339, 'en_us', 'JRFO LOGOTYPE7', NULL, NULL, NULL, '2015-08-20 19:37:04', '2015-08-20 19:37:04', 'abde238b-18b4-4527-b519-06d68c1d1b2f'),
(264, 340, 'en_us', 'JRFO LOGOTYPE8', NULL, NULL, NULL, '2015-08-20 19:37:07', '2015-08-20 19:37:07', '00b23f56-b63f-4782-bbc5-f88eeed6352e'),
(265, 341, 'en_us', 'JRFO LOGOTYPE9', NULL, NULL, NULL, '2015-08-20 19:37:10', '2015-08-20 19:37:10', '52561d87-767d-4e3b-b7be-05d0045f17c8'),
(266, 342, 'en_us', 'JRFO LOGOTYPE10', NULL, NULL, NULL, '2015-08-20 19:37:13', '2015-08-20 19:37:13', '48d9a202-37ee-4326-9603-e9466c707c40'),
(267, 343, 'en_us', 'MARKS & LOGOTYPES', 'Various marks and logotypes. Creative direction by Michael Bierut, Pentagram, for: M&T Bank, Penguin Press, Penguin Random House, Hudson Yards, and Midwood. ', 'Client:\r\n\r\nVarious\r\n', '', '2015-08-20 19:39:03', '2015-08-22 04:48:01', 'f931cc31-d499-4278-9f31-916f84d9883e');

-- --------------------------------------------------------

--
-- Table structure for table `craft_deprecationerrors`
--

CREATE TABLE IF NOT EXISTS `craft_deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) unsigned NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templateLine` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_elements`
--

CREATE TABLE IF NOT EXISTS `craft_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_elements_type_idx` (`type`),
  KEY `craft_elements_enabled_idx` (`enabled`),
  KEY `craft_elements_archived_dateCreated_idx` (`archived`,`dateCreated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=362 ;

--
-- Dumping data for table `craft_elements`
--

INSERT INTO `craft_elements` (`id`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'User', 1, 0, '2014-12-30 09:30:28', '2014-12-30 09:32:19', 'd7f93e8e-5b95-4077-bf54-a7a66fcceef9'),
(13, 'Entry', 1, 0, '2015-05-05 19:46:22', '2015-08-22 02:08:23', 'ccbd1570-ab31-4665-b220-12875e57cc7d'),
(15, 'Entry', 1, 0, '2015-05-05 20:26:19', '2015-08-22 02:16:03', '7b6ad736-3b44-4fea-ab22-715c6aa2cd85'),
(56, 'Asset', 1, 0, '2015-06-01 04:56:30', '2015-06-01 04:56:30', '47895cf3-4323-4b51-95e3-9841278168a3'),
(58, 'Asset', 1, 0, '2015-06-01 04:57:42', '2015-06-01 04:57:42', 'ca42cee2-6872-4a30-a1eb-cac5c9d3419f'),
(59, 'Asset', 1, 0, '2015-06-01 04:57:43', '2015-06-01 04:57:43', 'efe4122c-4dd8-4a97-8d7c-936d1bf6b8ef'),
(73, 'Entry', 1, 0, '2015-07-13 21:05:33', '2015-08-22 02:29:17', '0fe21c37-6555-468a-9e1a-f897da1c4566'),
(100, 'MatrixBlock', 1, 0, '2015-07-14 19:50:22', '2015-08-22 02:29:17', '506905c6-0b7f-42a9-9ca1-692b9377f9a3'),
(101, 'MatrixBlock', 1, 0, '2015-07-14 19:50:22', '2015-08-22 02:29:17', '6398980f-149f-434f-b906-95f9308a1d6d'),
(102, 'MatrixBlock', 1, 0, '2015-07-14 19:50:22', '2015-08-22 02:29:18', 'ecbbf48f-6105-4e71-a0b2-a3ad91325896'),
(103, 'MatrixBlock', 1, 0, '2015-07-14 19:50:22', '2015-08-22 02:29:18', '485cad16-8587-414e-b9c2-0b4dfd8962d1'),
(104, 'MatrixBlock', 1, 0, '2015-07-14 19:50:22', '2015-08-22 02:29:18', 'f1495c9f-d249-402e-abcd-aa786c8ddcb5'),
(105, 'MatrixBlock', 1, 0, '2015-07-14 19:50:22', '2015-08-22 02:29:18', '82e51fae-0d17-49ca-8895-d337692fbe78'),
(106, 'MatrixBlock', 1, 0, '2015-07-14 19:50:23', '2015-08-22 02:29:18', 'c503fd61-dba5-4223-b8f7-b97074e6dd7b'),
(107, 'MatrixBlock', 1, 0, '2015-07-14 19:50:23', '2015-08-22 02:29:18', '83bbf7b1-a472-4e91-b60c-98cab0113baa'),
(127, 'MatrixBlock', 1, 0, '2015-07-14 21:21:25', '2015-08-22 02:08:24', 'a014c1b7-3a96-4915-8ef8-856b29190c1c'),
(128, 'MatrixBlock', 1, 0, '2015-07-14 21:21:25', '2015-08-22 02:08:24', '3d8903c6-56dc-4906-a074-f91783f55bc1'),
(132, 'MatrixBlock', 1, 0, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '342066ef-d5ad-43fb-9b33-e5bd9e1cf2c9'),
(133, 'MatrixBlock', 1, 0, '2015-07-14 21:33:27', '2015-08-22 02:08:24', 'b468b7d8-6582-4519-a369-11fa51cc84c4'),
(134, 'MatrixBlock', 1, 0, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '333538ab-6370-43f9-ba61-6137b20d4f7c'),
(135, 'MatrixBlock', 1, 0, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '2fc01d8c-60a9-4c48-ac40-15c3f9fd25a5'),
(155, 'MatrixBlock', 1, 0, '2015-07-14 22:01:42', '2015-08-22 02:16:03', '29411a94-152f-4e91-88b5-427b7166d417'),
(156, 'MatrixBlock', 1, 0, '2015-07-14 22:01:42', '2015-08-22 02:16:03', '34794a2f-70ae-4fda-848b-a70a457ed881'),
(157, 'MatrixBlock', 1, 0, '2015-07-14 22:01:43', '2015-08-22 02:16:03', '1c03c5bd-5ed5-4143-8374-39c09cdd3431'),
(159, 'Asset', 1, 0, '2015-07-21 01:57:03', '2015-07-21 01:57:03', '86e2ea04-793c-47f4-93ab-f1721bb7891c'),
(160, 'Asset', 1, 0, '2015-07-21 01:57:54', '2015-07-21 01:57:54', '3fb47413-f66a-4487-bcfc-f294c9e42fb8'),
(161, 'Asset', 1, 0, '2015-07-21 01:57:58', '2015-07-21 01:57:58', 'f4fa01a2-8582-403c-a913-f2e9d8368367'),
(162, 'Asset', 1, 0, '2015-07-21 01:58:01', '2015-07-21 01:58:01', 'b80b10de-01f7-4ac0-9a30-c1881ff50384'),
(163, 'Asset', 1, 0, '2015-07-21 01:58:05', '2015-07-21 01:58:05', '33b9d6c2-db95-4b5b-b6d8-714235f8de90'),
(164, 'Asset', 1, 0, '2015-07-21 01:58:09', '2015-07-21 01:58:09', '74221bf5-8f51-4e83-9e53-8df390bbef1a'),
(165, 'Asset', 1, 0, '2015-07-21 01:58:13', '2015-07-21 01:58:13', '6b388336-d24b-4cc8-88ef-2c30379d64ba'),
(166, 'Asset', 1, 0, '2015-07-21 01:58:19', '2015-07-21 01:58:19', '8423d70d-e4ef-4a7f-876c-327433dd5f82'),
(167, 'Asset', 1, 0, '2015-07-21 01:58:23', '2015-07-21 01:58:23', 'fa0ffe0c-3051-4d83-a511-9787c8559cb7'),
(168, 'Asset', 1, 0, '2015-07-21 01:58:28', '2015-07-21 01:58:28', '50cb452b-2380-4947-9754-f85774300756'),
(169, 'Asset', 1, 0, '2015-07-21 01:58:33', '2015-07-21 01:58:33', '21c5cfbc-8224-45fd-80ac-29cd232b9eeb'),
(170, 'Asset', 1, 0, '2015-07-21 01:58:36', '2015-07-21 01:58:36', '65526ec0-c20a-49f8-97fb-804726606508'),
(171, 'Asset', 1, 0, '2015-07-21 01:58:41', '2015-07-21 01:58:41', 'eac55eaa-ad34-4ea8-9b04-6178767b4811'),
(172, 'Asset', 1, 0, '2015-07-21 01:58:46', '2015-07-21 01:58:46', 'c0c1f62a-56e6-4eef-ad17-fafe32736b5e'),
(173, 'Asset', 1, 0, '2015-07-21 01:58:50', '2015-07-21 01:58:50', 'ffdfa7a2-bd51-4846-9e37-7e5927fea5c1'),
(174, 'Asset', 1, 0, '2015-07-21 01:58:55', '2015-07-21 01:58:55', '9fb1fb89-0914-4667-8969-4d4e9a2b559c'),
(175, 'Asset', 1, 0, '2015-07-21 01:58:59', '2015-07-21 01:58:59', 'd4130bb1-653c-44ae-a25c-89f20f57e76c'),
(176, 'Asset', 1, 0, '2015-07-21 01:59:03', '2015-07-21 01:59:03', 'fc7f6e3a-988a-4bc6-af22-67d001480f6a'),
(177, 'Asset', 1, 0, '2015-07-21 01:59:08', '2015-07-21 01:59:08', '2c64b745-d091-4543-a7a1-f05079956fdf'),
(178, 'Asset', 1, 0, '2015-07-21 01:59:13', '2015-07-21 01:59:13', '7f56b402-0a56-4971-8f5b-9558ede39289'),
(179, 'Asset', 1, 0, '2015-07-21 01:59:17', '2015-07-21 01:59:17', '65c4fe0a-c1f8-4cf8-a3f4-044719a4e91c'),
(180, 'Asset', 1, 0, '2015-07-21 01:59:20', '2015-07-21 01:59:20', '8ee57019-2567-4f9e-b371-cfc01357f1a2'),
(181, 'Asset', 1, 0, '2015-07-21 01:59:25', '2015-07-21 01:59:25', '3a574e0d-e3d3-4d5a-a65d-ddb4864783e8'),
(182, 'Asset', 1, 0, '2015-07-21 01:59:31', '2015-07-21 01:59:31', 'e939f521-ce97-4bfb-9c75-addb5c568b96'),
(183, 'Asset', 1, 0, '2015-07-21 01:59:36', '2015-07-21 01:59:36', '53f0142e-9e75-4966-9876-92c93faee36a'),
(184, 'Asset', 1, 0, '2015-07-21 02:05:35', '2015-07-21 02:05:35', 'b03055fa-71c2-4cb8-aecc-fd1e1c0c4f59'),
(185, 'Asset', 1, 0, '2015-07-21 02:11:36', '2015-07-21 02:11:36', '18981f4e-34b4-42be-babe-9e014e4ab2b8'),
(186, 'Asset', 1, 0, '2015-07-21 02:11:38', '2015-07-21 02:11:38', 'df0dcd35-8985-4893-a10f-c43e23848fd0'),
(187, 'Asset', 1, 0, '2015-07-21 02:11:40', '2015-07-21 02:11:40', '762deb6d-0629-4ee5-9b46-a1795992c6ee'),
(188, 'Asset', 1, 0, '2015-07-21 02:11:41', '2015-07-21 02:11:41', 'd1280087-d125-4901-b737-f550b22948ca'),
(189, 'Asset', 1, 0, '2015-07-21 02:11:44', '2015-07-21 02:11:44', '8dcb4daa-f6d8-44a7-9504-71fb03819aa6'),
(190, 'Asset', 1, 0, '2015-07-21 02:11:46', '2015-07-21 02:11:46', '17904869-27d7-4293-9302-0d6fe4ac5ae2'),
(191, 'Asset', 1, 0, '2015-07-21 02:11:48', '2015-07-21 02:11:48', 'b884616e-9f4c-46a4-bb3a-5cb769170b79'),
(192, 'Asset', 1, 0, '2015-07-21 02:11:50', '2015-07-21 02:11:50', 'e0645e6a-9acd-4d50-bb38-517c15120225'),
(193, 'Asset', 1, 0, '2015-07-21 02:11:52', '2015-07-21 02:11:52', '049565cd-14ef-4b40-ab86-981a38bda0ff'),
(194, 'Asset', 1, 0, '2015-07-21 02:11:54', '2015-07-21 02:11:54', 'a1883b85-850e-4a6c-a2f1-7e50721e0c34'),
(195, 'Asset', 1, 0, '2015-07-21 02:11:55', '2015-07-21 02:11:55', '522f03a3-400c-4791-abc7-fb08f064fb2e'),
(196, 'Asset', 1, 0, '2015-07-21 02:11:58', '2015-07-21 02:11:58', '716686f5-dcc1-440b-8e9a-80e36cb6ed43'),
(197, 'Asset', 1, 0, '2015-07-21 02:12:00', '2015-07-21 02:12:00', '74caf984-82b8-4fd4-a71a-3f49416a3c9a'),
(198, 'Asset', 1, 0, '2015-07-21 02:12:01', '2015-07-21 02:12:01', '5b331506-7039-49e9-b969-05b29150f326'),
(199, 'Asset', 1, 0, '2015-07-21 02:12:03', '2015-07-21 02:12:03', 'bcb9ae6d-4fce-4bff-9d49-206d94012616'),
(200, 'Asset', 1, 0, '2015-07-21 02:12:05', '2015-07-21 02:12:05', 'c841e870-b731-44b6-ab17-be13351f8119'),
(201, 'Asset', 1, 0, '2015-07-21 02:12:07', '2015-07-21 02:12:07', '6ade4da6-258d-4e62-a2b9-1ba3c8a89042'),
(202, 'Asset', 1, 0, '2015-07-21 02:12:10', '2015-07-21 02:12:10', '42cee4c8-46db-47e0-a6ad-c818d6330a83'),
(203, 'Asset', 1, 0, '2015-07-21 02:37:04', '2015-07-21 02:37:04', '482b0161-01fc-453b-a49b-aa979c7ccd67'),
(204, 'Asset', 1, 0, '2015-07-21 02:37:06', '2015-07-21 02:37:06', 'af42ad70-4fe4-4425-81f9-844cdd547917'),
(205, 'Asset', 1, 0, '2015-07-21 02:37:08', '2015-07-21 02:37:08', '6142ec32-8012-4594-9dad-159d6c884cb1'),
(206, 'Asset', 1, 0, '2015-07-21 02:37:11', '2015-07-21 02:37:11', 'b1ce1b7a-5759-4063-838c-89c0b7850e1f'),
(207, 'Asset', 1, 0, '2015-07-21 02:37:13', '2015-07-21 02:37:13', '2c586eeb-c7c0-46e2-bbf9-66ed00713064'),
(208, 'Asset', 1, 0, '2015-07-21 02:37:15', '2015-07-21 02:37:15', 'dd0de34d-457b-4f33-81b8-43bd51022f4b'),
(209, 'Asset', 1, 0, '2015-07-21 02:37:18', '2015-07-21 02:37:18', 'c2f0d358-9db6-4ad3-bed9-457bbb5cb00a'),
(210, 'Asset', 1, 0, '2015-07-21 02:37:20', '2015-07-21 02:37:20', '08dc9f32-3d09-4cfd-ab0a-cddc2becd1d2'),
(211, 'Asset', 1, 0, '2015-07-21 02:37:22', '2015-07-21 02:37:22', 'd08ed06f-f40b-449d-b9f1-f51c8cea58d1'),
(212, 'Asset', 1, 0, '2015-07-21 02:39:47', '2015-07-21 02:39:47', '0f6cdfa5-cc96-4677-8d89-ebc6811d7c39'),
(213, 'MatrixBlock', 1, 0, '2015-07-21 02:48:57', '2015-08-22 02:16:04', 'c9f29741-553b-4d6c-8392-6289ec113d4d'),
(214, 'MatrixBlock', 1, 0, '2015-07-21 02:48:57', '2015-08-22 02:16:04', '150acbf9-b0d8-4efa-8d6c-5b3573e69359'),
(215, 'Entry', 1, 0, '2015-07-21 03:12:40', '2015-08-20 18:46:55', '32fad833-aa62-4803-a071-a74fa9e1a640'),
(216, 'Asset', 1, 0, '2015-07-21 03:21:31', '2015-07-21 03:21:31', 'e0cf81d7-21df-495f-aeeb-6c9efdd5f36f'),
(217, 'Asset', 1, 0, '2015-07-21 03:21:35', '2015-07-21 03:21:35', '96d4c123-aa75-4a2c-aa2d-65baf053c1d8'),
(218, 'Asset', 1, 0, '2015-07-21 03:21:39', '2015-07-21 03:21:39', '84de16e3-58d5-45a1-8adb-8e961129122d'),
(219, 'Asset', 1, 0, '2015-07-21 03:21:43', '2015-07-21 03:21:43', 'eed31b5a-f6a3-4ce4-95d6-b0b9c944978e'),
(220, 'Asset', 1, 0, '2015-07-21 03:21:47', '2015-07-21 03:21:47', '378e3988-4565-4bd2-b9d4-9452d8936fb1'),
(221, 'Asset', 1, 0, '2015-07-21 03:21:50', '2015-07-21 03:21:50', 'a659a1a4-e578-4603-b8d5-9193fe5fe185'),
(222, 'Asset', 1, 0, '2015-07-21 03:21:53', '2015-07-21 03:21:53', '4acb98ba-2892-4881-b9d1-63413ddb0764'),
(223, 'Asset', 1, 0, '2015-07-21 03:21:56', '2015-07-21 03:21:56', 'b7f9a2e6-26f5-4cd2-9c15-88f00aa02d06'),
(224, 'Asset', 1, 0, '2015-07-21 03:21:59', '2015-07-21 03:21:59', '1a44d779-2256-45d9-8a92-abfeccde3bd1'),
(225, 'Asset', 1, 0, '2015-07-21 03:22:02', '2015-07-21 03:22:02', '98384575-c755-4aab-a1fb-8b97994e41b8'),
(226, 'Asset', 1, 0, '2015-07-21 03:22:04', '2015-07-21 03:22:04', '5328a298-d124-4473-b7ec-38c3878d4d6a'),
(227, 'Asset', 1, 0, '2015-07-21 03:22:09', '2015-07-21 03:22:09', '6b079fd4-99f5-476e-81cb-f51df267fff5'),
(228, 'Asset', 1, 0, '2015-07-21 03:22:11', '2015-07-21 03:22:11', 'f535f8b9-03fb-45e5-8dbf-1552b6b430c1'),
(229, 'Asset', 1, 0, '2015-07-21 03:22:19', '2015-07-21 03:22:19', '89cb28d6-9cb5-4aeb-83ac-47c2d336bc01'),
(230, 'Asset', 1, 0, '2015-07-21 03:22:27', '2015-07-21 03:22:27', 'bc7d4af4-b2ac-46fb-bf52-02a730525e21'),
(231, 'Asset', 1, 0, '2015-07-21 03:22:30', '2015-07-21 03:22:30', '016987c1-1046-469e-a1a7-bc8c96c1225f'),
(232, 'Asset', 1, 0, '2015-07-21 03:22:33', '2015-07-21 03:22:33', '5f94fc15-ea85-48ba-8f2a-3b1ede9580e0'),
(233, 'MatrixBlock', 1, 0, '2015-07-21 03:38:42', '2015-08-20 18:46:55', '0b74c510-5319-4009-9095-865ec84c6f77'),
(234, 'MatrixBlock', 1, 0, '2015-07-21 03:41:09', '2015-08-20 18:46:55', '063ec8e6-cd97-4d7c-a59f-b66a22a01b72'),
(235, 'MatrixBlock', 1, 0, '2015-07-21 03:46:28', '2015-08-20 18:46:55', 'ff8eb4b8-bb31-4347-89bf-549eecc47073'),
(236, 'MatrixBlock', 1, 0, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '2dbd2ef3-6965-40fb-9d2e-015cc418786a'),
(237, 'MatrixBlock', 1, 0, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '6f221757-85f5-4f91-b491-6f68f6f8b562'),
(238, 'MatrixBlock', 1, 0, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '3a174f0a-84c3-45ba-8554-5b8df9a2a7ac'),
(239, 'MatrixBlock', 1, 0, '2015-07-21 03:46:28', '2015-08-20 18:46:55', 'd580c912-aae3-49af-95e6-ede9e517c5f9'),
(240, 'MatrixBlock', 1, 0, '2015-07-21 03:46:28', '2015-08-20 18:46:56', 'fafb2f68-b7ea-4dc5-ac0e-8d084acab9ae'),
(241, 'MatrixBlock', 1, 0, '2015-07-21 03:46:28', '2015-08-20 18:46:56', '0acbd942-eed1-46a3-b4e7-5fa3f1d906f0'),
(242, 'Asset', 1, 0, '2015-07-23 23:03:47', '2015-07-23 23:03:47', 'ec667e95-af16-4bc5-84aa-6d941f4262be'),
(243, 'Asset', 1, 0, '2015-07-23 23:03:48', '2015-07-23 23:03:48', '3a434062-832c-49e4-833c-458987027f01'),
(244, 'Asset', 1, 0, '2015-07-23 23:03:49', '2015-07-23 23:03:49', 'b1e206c0-be12-4f32-b6b2-144bde460f3f'),
(245, 'Asset', 1, 0, '2015-07-23 23:03:50', '2015-07-23 23:03:50', '014d0d3e-f5b7-43dc-967b-c8d1e474c4c7'),
(246, 'Asset', 1, 0, '2015-07-23 23:03:52', '2015-07-23 23:03:52', 'f682165f-56c3-472a-bdee-d315db0b13f8'),
(247, 'Asset', 1, 0, '2015-07-23 23:03:53', '2015-07-23 23:03:53', 'fd65c78a-b44a-4723-851b-d3fefc95fe26'),
(248, 'Asset', 1, 0, '2015-07-23 23:03:55', '2015-07-23 23:03:55', '1e8d9711-2436-4a79-9b0b-ad5e5caadbca'),
(249, 'Asset', 1, 0, '2015-07-23 23:03:56', '2015-07-23 23:03:56', 'f3a7aec7-923f-4a17-bab1-b0ac2bb4b04c'),
(250, 'Asset', 1, 0, '2015-07-23 23:03:58', '2015-07-23 23:03:58', '9dd97585-7caa-4107-b97f-b5ce594fbf31'),
(251, 'Asset', 1, 0, '2015-07-23 23:04:00', '2015-07-23 23:04:00', '598eae95-bb0d-40f4-9eaa-dbfbe887782f'),
(252, 'Asset', 1, 0, '2015-07-23 23:04:02', '2015-07-23 23:04:02', '0a446b47-43c6-4bda-b6a5-06a3a9213abf'),
(253, 'Asset', 1, 0, '2015-07-23 23:04:03', '2015-07-23 23:04:03', '00228653-2bd9-456f-8a60-345920602abc'),
(254, 'Asset', 1, 0, '2015-07-23 23:04:05', '2015-07-23 23:04:05', 'f03256e7-fda2-4a88-9628-2a8541401b49'),
(255, 'Entry', 1, 0, '2015-07-23 23:17:50', '2015-08-20 18:47:17', 'f6cfac9d-315a-4632-80f8-1870e703e929'),
(256, 'MatrixBlock', 1, 0, '2015-07-23 23:17:51', '2015-08-20 18:47:17', 'a86cf755-938c-4014-b9f6-3cbe0c227165'),
(257, 'MatrixBlock', 1, 0, '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'e4c06b39-b394-4e94-b80c-78a8893d0036'),
(258, 'MatrixBlock', 1, 0, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '9f126d46-7251-4ca9-87f5-7d1f8a780b2c'),
(259, 'MatrixBlock', 1, 0, '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'c5fe37a0-e66b-4eb8-af70-7831b2fb9137'),
(260, 'MatrixBlock', 1, 0, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '8b6e7d7d-e9e0-467c-a935-5c1760b2b3a1'),
(261, 'MatrixBlock', 1, 0, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '1cfba7f6-b35c-4414-92a9-a1db594fb7ee'),
(262, 'Asset', 1, 0, '2015-07-23 23:33:49', '2015-07-23 23:33:49', '3a676539-76ea-4c14-a401-2bbfc603d968'),
(263, 'Asset', 1, 0, '2015-07-23 23:40:15', '2015-07-23 23:40:15', '505c37ee-9996-4e5d-8e28-8e220fa5ee6b'),
(264, 'Asset', 1, 0, '2015-07-23 23:40:36', '2015-07-23 23:40:36', 'f8686b7a-431c-437f-af67-ea1f9a120918'),
(265, 'Asset', 1, 0, '2015-07-23 23:40:49', '2015-07-23 23:40:49', '267d4b80-1576-4901-845a-d49f5613c3ad'),
(266, 'Entry', 1, 0, '2015-07-23 23:41:04', '2015-08-22 02:10:43', 'dc43f7e8-3751-4fc3-9330-9aba349aebf7'),
(267, 'Asset', 1, 0, '2015-07-23 23:41:23', '2015-07-23 23:41:23', 'bdad4c2b-2619-494a-88fb-fbc5833fe409'),
(268, 'Asset', 1, 0, '2015-07-23 23:41:30', '2015-07-23 23:41:30', '5a846b58-f9e4-4e06-9859-0189a1c74179'),
(269, 'Asset', 1, 0, '2015-07-23 23:50:12', '2015-07-23 23:50:12', '9d343951-3db4-4e4d-acb4-f91d869bb50b'),
(270, 'Asset', 1, 0, '2015-07-23 23:50:24', '2015-07-23 23:50:24', 'cee8c5ff-aa22-49c8-824c-96087ec36a03'),
(271, 'Asset', 1, 0, '2015-07-23 23:53:20', '2015-07-23 23:53:20', '5954150c-8fea-4458-a4b4-a0326c0cb5e0'),
(272, 'Asset', 1, 0, '2015-07-23 23:56:16', '2015-07-23 23:56:16', '8a254ed2-c2b3-4ab7-be77-7ecc0ddced47'),
(273, 'MatrixBlock', 1, 0, '2015-07-23 23:56:41', '2015-08-22 02:10:43', '2516a36a-a9cf-4069-8861-e7225c90508e'),
(274, 'MatrixBlock', 1, 0, '2015-07-23 23:56:41', '2015-08-22 02:10:43', 'bd5ddfe0-8960-4aa3-9251-154d4a649f40'),
(275, 'MatrixBlock', 1, 0, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '184b27d3-c22b-4b7d-acea-1f07252c70ee'),
(276, 'MatrixBlock', 1, 0, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '66e06882-9bb2-4859-9b6b-79208beb2f33'),
(277, 'MatrixBlock', 1, 0, '2015-07-23 23:56:41', '2015-08-22 02:10:44', 'c790a562-014a-4ebb-bf88-b016d94c4123'),
(278, 'MatrixBlock', 1, 0, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '1690d412-757a-4c85-8707-8ed7ada64d29'),
(279, 'Asset', 1, 0, '2015-08-01 04:08:47', '2015-08-01 04:08:47', '757cf876-bd00-44b4-82f4-f071b1c3641a'),
(280, 'Entry', 1, 0, '2015-08-07 17:38:17', '2015-08-22 02:00:58', 'abcaf425-7a5b-4b5e-a849-080f086507af'),
(281, 'Asset', 1, 0, '2015-08-07 17:42:48', '2015-08-07 17:42:48', '61b2b55d-d370-487b-a9e6-2a98cb633c8f'),
(282, 'Asset', 1, 0, '2015-08-07 17:46:16', '2015-08-07 17:46:16', '6502406c-41e3-4fb6-af80-649a34f3333e'),
(283, 'MatrixBlock', 1, 0, '2015-08-16 21:06:05', '2015-08-22 02:00:59', '36c822b2-6e87-4332-8746-237bedac30cd'),
(285, 'Asset', 1, 0, '2015-08-16 21:14:13', '2015-08-16 21:56:00', '465b23e4-187e-47ba-b4b6-674406cdab11'),
(287, 'Asset', 1, 0, '2015-08-16 21:55:13', '2015-08-16 21:55:13', 'f8a7ad69-62d9-4ba1-9242-94c8b71a8d8d'),
(288, 'Asset', 1, 0, '2015-08-16 21:55:15', '2015-08-16 21:55:15', '3cbed1c5-0b0e-4f17-96a7-d93222e8c1ed'),
(289, 'Asset', 1, 0, '2015-08-16 21:55:16', '2015-08-16 21:55:16', 'da48dfc7-1e3f-48fc-8249-b2965211a908'),
(290, 'Asset', 1, 0, '2015-08-16 21:55:18', '2015-08-16 21:55:18', '3f327749-0df9-4fb1-946d-6922154fcb8c'),
(291, 'Asset', 1, 0, '2015-08-16 21:55:20', '2015-08-16 21:55:20', 'addc5d52-823f-4fb4-8dcf-8f9968132571'),
(292, 'Asset', 1, 0, '2015-08-16 21:55:22', '2015-08-16 21:55:22', '90080262-c092-4012-b5d6-5b594eb2e0ac'),
(293, 'Asset', 1, 0, '2015-08-16 21:55:23', '2015-08-16 21:55:23', '8f9dc981-af55-4d89-b62a-dc7ee725d420'),
(294, 'Asset', 1, 0, '2015-08-16 21:55:24', '2015-08-16 21:55:24', '953ee0ee-2612-4d07-b590-fbe01f178115'),
(295, 'Asset', 1, 0, '2015-08-16 21:55:26', '2015-08-16 21:55:26', 'cb3d01c3-d53f-4b69-a7c3-bda7b3cc306d'),
(296, 'Asset', 1, 0, '2015-08-16 21:55:27', '2015-08-16 21:55:27', 'dd4710cd-5bba-4361-890c-388143a0ebfc'),
(297, 'Asset', 1, 0, '2015-08-16 21:55:30', '2015-08-16 21:55:30', '1c89b65a-3cec-4b00-b7b4-eb0790a424a4'),
(298, 'Asset', 1, 0, '2015-08-16 21:55:31', '2015-08-16 21:55:31', '4d63fa70-e8d2-4b02-8ff5-1b23128b08ed'),
(299, 'Asset', 1, 0, '2015-08-16 21:55:33', '2015-08-16 21:55:33', '338c9080-0865-46d2-a2fe-05b2b5af4a19'),
(300, 'Asset', 1, 0, '2015-08-16 21:55:35', '2015-08-16 21:55:35', '62a179a0-803c-4d12-a71f-431646ff9c91'),
(301, 'Asset', 1, 0, '2015-08-16 21:55:36', '2015-08-16 21:55:36', '2e2f3177-7659-4c7f-afd5-e9c0a61b373d'),
(302, 'Asset', 1, 0, '2015-08-16 21:55:37', '2015-08-16 21:55:37', '336d28e5-e027-4b04-ab98-b3f8a1e6e013'),
(303, 'Asset', 1, 0, '2015-08-16 21:55:39', '2015-08-16 21:55:39', '207ce96d-1bb4-4b38-9b17-2c0639512ce4'),
(304, 'Asset', 1, 0, '2015-08-16 21:55:40', '2015-08-16 21:55:40', '6776b776-0011-4acb-bbf6-65d43829f826'),
(305, 'Asset', 1, 0, '2015-08-16 21:55:42', '2015-08-16 21:55:42', '30757746-e682-4c58-a37e-5f6daac7bfca'),
(306, 'Asset', 1, 0, '2015-08-16 21:55:44', '2015-08-16 21:55:44', '00aa20d8-3ad0-4aae-a916-c93e9bbd0eed'),
(307, 'Asset', 1, 0, '2015-08-16 21:55:46', '2015-08-16 21:55:46', 'd297dc66-6beb-4119-9133-d8da8946cd50'),
(308, 'Asset', 1, 0, '2015-08-16 21:55:47', '2015-08-16 21:55:47', '36e20b6e-6b0b-411f-818a-83f410c714e2'),
(309, 'MatrixBlock', 1, 0, '2015-08-16 21:58:19', '2015-08-22 02:00:59', '353269ff-0baa-4fe2-a4dd-4a0aeee04f06'),
(310, 'MatrixBlock', 1, 0, '2015-08-16 21:58:19', '2015-08-22 02:00:59', '514b6680-fd6b-4bb4-be9a-2e461d0e66d4'),
(311, 'Asset', 1, 0, '2015-08-18 13:29:18', '2015-08-18 13:29:18', '85d2fac1-de14-4d2d-9200-99483905e085'),
(312, 'Asset', 1, 0, '2015-08-18 13:46:46', '2015-08-18 13:46:46', 'ee859491-badd-4291-9e59-30aa1031fe9f'),
(313, 'Asset', 1, 0, '2015-08-18 13:46:47', '2015-08-18 13:46:47', '86181e12-dd9c-4d53-84dc-71d85ffba675'),
(314, 'MatrixBlock', 1, 0, '2015-08-18 14:06:35', '2015-08-22 02:00:59', '71bee568-7a56-4074-9c53-d81f60579c00'),
(315, 'MatrixBlock', 1, 0, '2015-08-18 14:06:35', '2015-08-22 02:00:59', 'c4b8a40e-8caf-4ad9-883b-4e0d3f45711f'),
(316, 'MatrixBlock', 1, 0, '2015-08-18 14:06:35', '2015-08-22 02:00:59', '04e3b9b0-34aa-49d1-ba7a-d511dfbd03f2'),
(317, 'MatrixBlock', 1, 0, '2015-08-18 14:06:35', '2015-08-22 02:00:59', '870824c3-257c-44da-8d1b-e84ef4d0178b'),
(318, 'Asset', 1, 0, '2015-08-19 02:43:13', '2015-08-19 02:43:13', '92651fc5-011e-425c-a1a8-a89a87b45d8a'),
(319, 'Asset', 1, 0, '2015-08-19 02:43:15', '2015-08-19 02:43:15', '117444b9-d26e-46f2-aa34-3fc0110ef427'),
(320, 'Asset', 1, 0, '2015-08-19 02:43:17', '2015-08-19 02:43:17', '48faa67c-4f89-43e9-ac49-ed8e5c123722'),
(321, 'Asset', 1, 0, '2015-08-19 02:43:19', '2015-08-19 02:43:19', '15ecb2e9-e38a-4b24-ae6c-481cb5bddef5'),
(322, 'MatrixBlock', 1, 0, '2015-08-19 02:47:43', '2015-08-22 02:00:59', '1145c8e1-414d-4021-9a5b-9b469505d3d9'),
(323, 'MatrixBlock', 1, 0, '2015-08-19 02:47:43', '2015-08-22 02:00:59', '154dc81a-c39c-4e90-8ca1-84f8c4507a1e'),
(324, 'MatrixBlock', 1, 0, '2015-08-19 02:47:44', '2015-08-22 02:00:59', 'b13e4460-9a0a-4499-a5a4-0d6a7ecc797c'),
(325, 'Asset', 1, 0, '2015-08-19 03:35:05', '2015-08-19 03:35:05', 'a3e4fc7c-1193-4e44-86c5-0b2c2ee135eb'),
(326, 'Asset', 1, 0, '2015-08-19 03:42:05', '2015-08-19 03:42:05', 'b1423f11-3666-4696-a908-e7eb500322f7'),
(327, 'Asset', 1, 0, '2015-08-19 03:52:10', '2015-08-19 03:52:10', 'cbbc7576-ec08-49ec-b3e3-c3967a956366'),
(328, 'Asset', 1, 0, '2015-08-19 03:52:14', '2015-08-19 03:52:14', 'b9d7bd0d-eecb-4ec8-aee3-0fc68d8ea821'),
(329, 'MatrixBlock', 1, 0, '2015-08-19 03:53:43', '2015-08-22 02:00:59', '4c2859a9-16ad-4fb6-87a0-b50c33cbb9c8'),
(330, 'MatrixBlock', 1, 0, '2015-08-19 03:53:43', '2015-08-22 02:00:59', '50595969-a0a2-4862-bccf-4c8be6129467'),
(331, 'Asset', 1, 0, '2015-08-20 19:36:37', '2015-08-20 19:36:37', 'd2baaa69-631c-4d2c-a455-04c56372352a'),
(332, 'Asset', 1, 0, '2015-08-20 19:36:40', '2015-08-20 19:36:40', 'e9277098-85ba-433f-b46e-2ca22c369269'),
(333, 'Asset', 1, 0, '2015-08-20 19:36:44', '2015-08-20 19:36:44', '1807df37-4704-4dd7-81ea-193d791143e0'),
(334, 'Asset', 1, 0, '2015-08-20 19:36:47', '2015-08-20 19:36:47', '0cf983e3-3687-4547-9c37-4973ab8bae2c'),
(335, 'Asset', 1, 0, '2015-08-20 19:36:50', '2015-08-20 19:36:50', '7378f7a2-225e-4dae-85b8-5e09d0f5ea1d'),
(336, 'Asset', 1, 0, '2015-08-20 19:36:54', '2015-08-20 19:36:54', '94dd99f9-6361-43b8-99b5-7c354b973061'),
(337, 'Asset', 1, 0, '2015-08-20 19:36:57', '2015-08-20 19:36:57', 'd2cb2af8-8743-402c-854e-316f7f347421'),
(338, 'Asset', 1, 0, '2015-08-20 19:37:00', '2015-08-20 19:37:00', '56945f29-128f-41d8-9d2e-488fa21dd0b0'),
(339, 'Asset', 1, 0, '2015-08-20 19:37:04', '2015-08-20 19:37:04', '5235beeb-e004-4dee-8fec-d527a23b86c0'),
(340, 'Asset', 1, 0, '2015-08-20 19:37:07', '2015-08-20 19:37:07', '9786e421-d60a-4ba6-a8a2-e2690ce38461'),
(341, 'Asset', 1, 0, '2015-08-20 19:37:10', '2015-08-20 19:37:10', '798a83cf-75a4-4d7e-9f7d-711b7e433907'),
(342, 'Asset', 1, 0, '2015-08-20 19:37:13', '2015-08-20 19:37:13', '25afc83e-3b51-4be6-a56f-9ea71aaffc8b'),
(343, 'Entry', 1, 0, '2015-08-20 19:39:03', '2015-08-22 04:48:01', 'c6ad20d0-7fd3-49b7-a6c9-0fdc0ec14b48'),
(344, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'c4d11bf6-661d-4c80-8523-163e898b59ed'),
(345, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '14c72aa3-08a9-4e6c-a111-678ee1f73b54'),
(346, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '2b64c24e-8e4d-427e-9003-21d4e7caa44c'),
(347, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'ca070be4-f65b-4491-a14c-d1723c82545f'),
(348, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '7d68cc74-e24b-41d6-ab88-17048b6be895'),
(349, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '4eb9ef32-5942-4f3a-9603-4f80347e5c4f'),
(350, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '5cbd9493-cf1e-47ca-9fbf-6c6f327acb6a'),
(351, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'a6f1d08a-8cee-40ec-b6dc-31dfb92f0a65'),
(352, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '03245965-0b32-4712-9c3d-6f4ca444e566'),
(353, 'MatrixBlock', 1, 0, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '8236b0a1-8080-471c-8558-fd169734eeb4'),
(354, 'MatrixBlock', 1, 0, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '73c57103-d1b5-41ca-937d-a614006eecbd'),
(355, 'MatrixBlock', 1, 0, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '07d23f73-4461-4f52-80a8-7d5a405f544c'),
(356, 'MatrixBlock', 1, 0, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'b520517b-c7b5-4356-a3a2-82225e703f0a'),
(357, 'MatrixBlock', 1, 0, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '8d712ba7-a281-4db0-86bc-3f19104401de'),
(358, 'MatrixBlock', 1, 0, '2015-08-22 02:08:24', '2015-08-22 02:08:24', 'e59c6388-0817-457a-810c-abc9e5bf0af4'),
(359, 'MatrixBlock', 1, 0, '2015-08-22 02:29:18', '2015-08-22 02:29:18', 'ef71e201-3975-4f2c-86ce-19688cdf8161'),
(360, 'MatrixBlock', 1, 0, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '4aecc154-6f4e-47ca-9d1c-b3a67df15765'),
(361, 'MatrixBlock', 1, 0, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '9b007161-cc96-4939-8580-b90928c94eab');

-- --------------------------------------------------------

--
-- Table structure for table `craft_elements_i18n`
--

CREATE TABLE IF NOT EXISTS `craft_elements_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_elements_i18n_elementId_locale_unq_idx` (`elementId`,`locale`),
  UNIQUE KEY `craft_elements_i18n_uri_locale_unq_idx` (`uri`,`locale`),
  KEY `craft_elements_i18n_slug_locale_idx` (`slug`,`locale`),
  KEY `craft_elements_i18n_enabled_idx` (`enabled`),
  KEY `craft_elements_i18n_locale_fk` (`locale`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=365 ;

--
-- Dumping data for table `craft_elements_i18n`
--

INSERT INTO `craft_elements_i18n` (`id`, `elementId`, `locale`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'en_us', '', NULL, 1, '2014-12-30 09:30:28', '2014-12-30 09:32:19', '2bc5130d-39f6-44c3-9bab-83edf8f8f9ef'),
(16, 13, 'en_us', 'gato', 'projects/gato', 1, '2015-05-05 19:46:22', '2015-08-22 02:08:24', '8ae524a1-6c6b-4d69-b489-1af95f275b4d'),
(18, 15, 'en_us', 'saks-fifth-avenue', 'projects/saks-fifth-avenue', 1, '2015-05-05 20:26:19', '2015-08-22 02:16:03', '43498d6c-6013-4e48-b27f-b47ee2cf95aa'),
(59, 56, 'en_us', 'ms-ge-30-copy-web-960', NULL, 1, '2015-06-01 04:56:30', '2015-06-01 04:56:30', '5498459e-4670-4113-a2dc-3eabbb105d93'),
(61, 58, 'en_us', 'ms-ge-24-copy-web-960', NULL, 1, '2015-06-01 04:57:42', '2015-06-01 04:57:42', '1e5c4809-7b4a-4dc8-88e9-cc19a9eaeeea'),
(62, 59, 'en_us', 'ms-ge-17-copy-web-960', NULL, 1, '2015-06-01 04:57:43', '2015-06-01 04:57:43', 'b7794ece-024e-46cf-ae38-80e0e34b733d'),
(76, 73, 'en_us', 'nycta-graphics-standards-manual', 'projects/nycta-graphics-standards-manual', 1, '2015-07-13 21:05:33', '2015-08-22 02:29:17', '644dfd56-8122-4284-be50-e2a752b5b3b5'),
(103, 100, 'en_us', '', NULL, 1, '2015-07-14 19:50:22', '2015-08-22 02:29:17', '8e821855-3295-435c-a229-5a391838ec35'),
(104, 101, 'en_us', '', NULL, 1, '2015-07-14 19:50:22', '2015-08-22 02:29:18', '48aec180-a201-4b39-a6a7-5bf79cf223fc'),
(105, 102, 'en_us', '', NULL, 1, '2015-07-14 19:50:22', '2015-08-22 02:29:18', '830c2258-29ee-4506-9e5d-e55e12144491'),
(106, 103, 'en_us', '', NULL, 1, '2015-07-14 19:50:22', '2015-08-22 02:29:18', 'b3ad147f-1a88-4282-8a27-a948ba015ca9'),
(107, 104, 'en_us', '', NULL, 1, '2015-07-14 19:50:22', '2015-08-22 02:29:18', '34d16400-b7f6-40c5-b35c-8cf49dba5a68'),
(108, 105, 'en_us', '', NULL, 1, '2015-07-14 19:50:22', '2015-08-22 02:29:18', '76cfae0d-5052-4d70-8aa6-8b4d49b20ac3'),
(109, 106, 'en_us', '', NULL, 1, '2015-07-14 19:50:23', '2015-08-22 02:29:18', '2ef466f2-670c-450b-a086-268cf2b612af'),
(110, 107, 'en_us', '', NULL, 1, '2015-07-14 19:50:23', '2015-08-22 02:29:18', '4d246f5e-2042-449b-9438-72263ea621c6'),
(130, 127, 'en_us', '', NULL, 1, '2015-07-14 21:21:25', '2015-08-22 02:08:24', '317cf308-3109-4d7a-9dd9-76988079d1aa'),
(131, 128, 'en_us', '', NULL, 1, '2015-07-14 21:21:25', '2015-08-22 02:08:24', '3e2b873a-d270-4ae0-91c6-e4dda7009087'),
(135, 132, 'en_us', '', NULL, 1, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '2e5aa6ae-e6d7-4167-990f-c94e93b8807b'),
(136, 133, 'en_us', '', NULL, 1, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '09d20242-f137-47bb-a8e9-2fa14bddf58c'),
(137, 134, 'en_us', '', NULL, 1, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '0bada37b-7211-400c-a90f-3b4b46863006'),
(138, 135, 'en_us', '', NULL, 1, '2015-07-14 21:33:28', '2015-08-22 02:08:24', '6ccc63bd-2097-495a-8420-3ebfac0ac58d'),
(158, 155, 'en_us', '', NULL, 1, '2015-07-14 22:01:42', '2015-08-22 02:16:03', '08e3b901-d1f5-41b9-a63e-3cdf0639c579'),
(159, 156, 'en_us', '', NULL, 1, '2015-07-14 22:01:43', '2015-08-22 02:16:03', '57781eed-b3ba-4c4c-9712-ce1ed83d9426'),
(160, 157, 'en_us', '', NULL, 1, '2015-07-14 22:01:43', '2015-08-22 02:16:03', '50edb22c-99d1-4fee-8670-d548d24312ee'),
(162, 159, 'en_us', 'nycta-graphics-standards-manual-detail-2-web', NULL, 1, '2015-07-21 01:57:03', '2015-07-21 01:57:03', 'ccc0dc01-4fab-47d3-925a-caeaeae3b774'),
(163, 160, 'en_us', 'nycta-graphics-standards-manual-2-web', NULL, 1, '2015-07-21 01:57:55', '2015-07-21 01:57:55', '6b5ba3af-17ff-4f72-acb5-f8761c2591b2'),
(164, 161, 'en_us', 'nycta-graphics-standards-manual-4-web', NULL, 1, '2015-07-21 01:57:58', '2015-07-21 01:57:58', '82fddd59-9d54-4222-b8ee-05f879451ff9'),
(165, 162, 'en_us', 'nycta-graphics-standards-manual-7-web', NULL, 1, '2015-07-21 01:58:02', '2015-07-21 01:58:02', 'b34dd0f2-4f5b-4a58-a819-3e35e3d47562'),
(166, 163, 'en_us', 'nycta-graphics-standards-manual-8-web', NULL, 1, '2015-07-21 01:58:06', '2015-07-21 01:58:06', 'd98434ea-009b-457f-8973-a44ea607b4b1'),
(167, 164, 'en_us', 'nycta-graphics-standards-manual-13-web', NULL, 1, '2015-07-21 01:58:10', '2015-07-21 01:58:10', '28425006-53b7-4b94-9c72-2b178eedb10e'),
(168, 165, 'en_us', 'nycta-graphics-standards-manual-14-web', NULL, 1, '2015-07-21 01:58:14', '2015-07-21 01:58:14', '3125524c-8e71-4bb2-acc3-e46c6e49a63c'),
(169, 166, 'en_us', 'nycta-graphics-standards-manual-16-web', NULL, 1, '2015-07-21 01:58:19', '2015-07-21 01:58:19', 'b6bd4083-d0f5-4ba8-a29f-848ce801e0cb'),
(170, 167, 'en_us', 'nycta-graphics-standards-manual-20-web', NULL, 1, '2015-07-21 01:58:25', '2015-07-21 01:58:25', 'd7134328-1827-446d-bdb5-c02117e89856'),
(171, 168, 'en_us', 'nycta-graphics-standards-manual-26-web', NULL, 1, '2015-07-21 01:58:30', '2015-07-21 01:58:30', 'e8f7be32-33d2-4dc4-af03-f04356b8ac9a'),
(172, 169, 'en_us', 'nycta-graphics-standards-manual-30-web', NULL, 1, '2015-07-21 01:58:33', '2015-07-21 01:58:33', 'a79d97ef-98c0-4212-87e8-87a66db16741'),
(173, 170, 'en_us', 'nycta-graphics-standards-manual-39-web', NULL, 1, '2015-07-21 01:58:37', '2015-07-21 01:58:37', '0aef64a4-dc2c-4f63-be9b-4b8e4d0538c2'),
(174, 171, 'en_us', 'nycta-graphics-standards-manual-43-web', NULL, 1, '2015-07-21 01:58:41', '2015-07-21 01:58:41', 'dd8e9eb8-f59d-42b4-b94d-c19e4eff2042'),
(175, 172, 'en_us', 'nycta-graphics-standards-manual-44-web', NULL, 1, '2015-07-21 01:58:46', '2015-07-21 01:58:46', '47cc1681-a67d-4e3f-b6ab-d6e0e9d22594'),
(176, 173, 'en_us', 'nycta-graphics-standards-manual-45-web', NULL, 1, '2015-07-21 01:58:51', '2015-07-21 01:58:51', 'cbbb6509-f872-4e80-a1e5-e6f26dba59ce'),
(177, 174, 'en_us', 'nycta-graphics-standards-manual-46-web', NULL, 1, '2015-07-21 01:58:56', '2015-07-21 01:58:56', '8f6d06aa-f8d1-4f94-afaa-c6c91db2f752'),
(178, 175, 'en_us', 'nycta-graphics-standards-manual-47-web', NULL, 1, '2015-07-21 01:58:59', '2015-07-21 01:58:59', 'f53b966f-9da0-44eb-9ef3-c5b38c126233'),
(179, 176, 'en_us', 'nycta-graphics-standards-manual-48-web', NULL, 1, '2015-07-21 01:59:04', '2015-07-21 01:59:04', '49bd8a52-c291-404c-9be9-1a30ef36b4fc'),
(180, 177, 'en_us', 'nycta-graphics-standards-manual-cover-web', NULL, 1, '2015-07-21 01:59:08', '2015-07-21 01:59:08', '136f242a-5f18-4ee6-904d-a16400e849bf'),
(181, 178, 'en_us', 'nycta-graphics-standards-manual-detail-5-web', NULL, 1, '2015-07-21 01:59:14', '2015-07-21 01:59:14', '3495ef1c-2519-426a-af1f-8cf011104244'),
(182, 179, 'en_us', 'nycta-graphics-standards-manual-detail-7-web', NULL, 1, '2015-07-21 01:59:17', '2015-07-21 01:59:17', 'dddfa14b-5311-455c-a254-66b3faf17757'),
(183, 180, 'en_us', 'nycta-graphics-standards-manual-detail-10-web', NULL, 1, '2015-07-21 01:59:21', '2015-07-21 01:59:21', 'c791157e-ca49-4963-a63a-37e5bc709519'),
(184, 181, 'en_us', 'nycta-graphics-standards-manual-detail-16-web', NULL, 1, '2015-07-21 01:59:25', '2015-07-21 01:59:25', '676f2bff-2fe1-45ad-9946-c41cd77dfe82'),
(185, 182, 'en_us', 'nycta-graphics-standards-manual-detail-17-web', NULL, 1, '2015-07-21 01:59:31', '2015-07-21 01:59:31', 'dc1806d4-57e8-46ea-a541-7addd6f4afe6'),
(186, 183, 'en_us', 'nycta-graphics-standards-manual-detail-25-web', NULL, 1, '2015-07-21 01:59:36', '2015-07-21 01:59:36', 'fc06af0e-64a2-4c7f-ac21-04f550d0e33c'),
(187, 184, 'en_us', 'nycta-graphics-standards-manual-1-web', NULL, 1, '2015-07-21 02:05:36', '2015-07-21 02:05:36', 'c5fc6a52-46e6-462f-ab4a-d5f5dfa8442b'),
(188, 185, 'en_us', 'gato-web-1', NULL, 1, '2015-07-21 02:11:36', '2015-07-21 02:11:36', 'f18019c0-fe5d-4e32-bf48-0f93757a9bfd'),
(189, 186, 'en_us', 'gato-web-2', NULL, 1, '2015-07-21 02:11:38', '2015-07-21 02:11:38', 'de876c3f-0553-4821-9414-df357d69516f'),
(190, 187, 'en_us', 'gato-web-3', NULL, 1, '2015-07-21 02:11:40', '2015-07-21 02:11:40', 'd03acfc8-cfb8-4aad-bc19-bb85a123e84e'),
(191, 188, 'en_us', 'gato-web-4', NULL, 1, '2015-07-21 02:11:42', '2015-07-21 02:11:42', '7c3b7b05-5cd1-4ca7-bb3b-b2609b177b09'),
(192, 189, 'en_us', 'gato-web-5', NULL, 1, '2015-07-21 02:11:44', '2015-07-21 02:11:44', 'fb891dc3-51cc-4949-a8a7-1e00f17fe146'),
(193, 190, 'en_us', 'gato-web-6', NULL, 1, '2015-07-21 02:11:46', '2015-07-21 02:11:46', '9e915b32-074b-40dd-8458-e7352e4b9ed2'),
(194, 191, 'en_us', 'gato-web-7', NULL, 1, '2015-07-21 02:11:48', '2015-07-21 02:11:48', '866342ce-9907-402a-bc82-daa4c3c70b04'),
(195, 192, 'en_us', 'gato-web-8', NULL, 1, '2015-07-21 02:11:50', '2015-07-21 02:11:50', 'f10300f5-4ae1-4f28-b4b6-4df799d623a4'),
(196, 193, 'en_us', 'gato-web-9', NULL, 1, '2015-07-21 02:11:52', '2015-07-21 02:11:52', '8a902ac6-0f1b-4d0e-9688-ddfd24c9576a'),
(197, 194, 'en_us', 'gato-web-10', NULL, 1, '2015-07-21 02:11:54', '2015-07-21 02:11:54', '8e12ee63-e094-4ddd-8b7f-c1358a0c5b97'),
(198, 195, 'en_us', 'gato-web-11', NULL, 1, '2015-07-21 02:11:55', '2015-07-21 02:11:55', 'fcce362d-6086-49d0-bc7b-c4901067b207'),
(199, 196, 'en_us', 'gato-web-12', NULL, 1, '2015-07-21 02:11:58', '2015-07-21 02:11:58', '991529ae-b5af-4240-91f5-8c7cdf6becc2'),
(200, 197, 'en_us', 'gato-web-13', NULL, 1, '2015-07-21 02:12:00', '2015-07-21 02:12:00', 'da38d4ab-e53a-4d25-9c4a-c582a84e4ad1'),
(201, 198, 'en_us', 'gato-web-14', NULL, 1, '2015-07-21 02:12:01', '2015-07-21 02:12:01', '3476bf51-6587-4437-a548-3098792bd75b'),
(202, 199, 'en_us', 'gato-web-15', NULL, 1, '2015-07-21 02:12:03', '2015-07-21 02:12:03', '478cfae0-27f5-47f3-9f06-662a66800318'),
(203, 200, 'en_us', 'gato-web-16', NULL, 1, '2015-07-21 02:12:05', '2015-07-21 02:12:05', 'de5c67e4-371c-44f8-8d36-347dae0328d9'),
(204, 201, 'en_us', 'gato-web-17rev', NULL, 1, '2015-07-21 02:12:07', '2015-07-21 02:12:07', '7e119429-42d3-4447-8e09-8a2cb000f0ea'),
(205, 202, 'en_us', 'gato-web-18', NULL, 1, '2015-07-21 02:12:10', '2015-07-21 02:12:10', 'd0546d63-3e47-4e3e-92e7-051e8d5d89a5'),
(206, 203, 'en_us', 'saks-bag-street-5-trix-web', NULL, 1, '2015-07-21 02:37:04', '2015-07-21 02:37:04', 'a460e9f6-ea60-4044-b86b-54383be2ebcf'),
(207, 204, 'en_us', 'saks-bag-street-8-trix-web', NULL, 1, '2015-07-21 02:37:06', '2015-07-21 02:37:06', '41c4d936-78ad-441e-ac9f-e546d828dcff'),
(208, 205, 'en_us', 'saks-bag-street-9-trix-web', NULL, 1, '2015-07-21 02:37:08', '2015-07-21 02:37:08', '4e8579e2-fbaf-45ca-b9af-c07c438fab84'),
(209, 206, 'en_us', 'saks-bag-street-16-trix-web', NULL, 1, '2015-07-21 02:37:11', '2015-07-21 02:37:11', '3871f831-8a64-497a-a1b6-42bc7c5a07b3'),
(210, 207, 'en_us', 'saks-look-mark', NULL, 1, '2015-07-21 02:37:13', '2015-07-21 02:37:13', '99a433c4-6768-42af-87d3-e8c335a4d275'),
(211, 208, 'en_us', 'sfa-look-cataglogue-aug1-trix-web', NULL, 1, '2015-07-21 02:37:15', '2015-07-21 02:37:15', '50fe92f5-ee72-4dce-b28d-6d4b1bd6419c'),
(212, 209, 'en_us', 'sfa-look-cataglogue-june-1-trix-web', NULL, 1, '2015-07-21 02:37:18', '2015-07-21 02:37:18', '831fda4e-30af-41fc-a4ec-f974e85ee042'),
(213, 210, 'en_us', 'sfa-look-cataglogue-new1-trix-web', NULL, 1, '2015-07-21 02:37:20', '2015-07-21 02:37:20', 'a359b2fe-be2d-4e48-b20d-3129fcd501e5'),
(214, 211, 'en_us', 'sfa-look-cataglogue-new2-trix-web', NULL, 1, '2015-07-21 02:37:22', '2015-07-21 02:37:22', 'f80b93cb-f7c9-48f3-8113-f52ea47426e1'),
(215, 212, 'en_us', 'saks-bag-street-4-trix-web', NULL, 1, '2015-07-21 02:39:47', '2015-07-21 02:39:47', '21d4a829-e329-4164-964f-9d37337468da'),
(216, 213, 'en_us', '', NULL, 1, '2015-07-21 02:48:57', '2015-08-22 02:16:04', '20e8651e-d4ed-4cf2-a1e8-cb9e86ae799c'),
(217, 214, 'en_us', '', NULL, 1, '2015-07-21 02:48:57', '2015-08-22 02:16:04', '45a96977-e780-46c8-a847-fd25133af48f'),
(218, 215, 'en_us', 'german-expressionism-the-graphic-impulse', 'projects/german-expressionism-the-graphic-impulse', 1, '2015-07-21 03:12:40', '2015-08-20 18:46:55', 'd2e3c034-683a-4751-8d03-167fea08dfd8'),
(219, 216, 'en_us', 'ms-ge-17-web', NULL, 1, '2015-07-21 03:21:31', '2015-07-21 03:21:31', '84ab532b-a06e-4b81-bad0-9d0b1b9aedc7'),
(220, 217, 'en_us', 'ms-ge-21-web', NULL, 1, '2015-07-21 03:21:35', '2015-07-21 03:21:35', '43c7f4af-7f7f-44d2-b5e1-15fc573cea8e'),
(221, 218, 'en_us', 'ms-ge-24-web', NULL, 1, '2015-07-21 03:21:40', '2015-07-21 03:21:40', '5afb41fc-bdef-4125-9cf5-9d80e99d50f8'),
(222, 219, 'en_us', 'ms-ge-26-web', NULL, 1, '2015-07-21 03:21:43', '2015-07-21 03:21:43', 'bdcd94e6-27bd-41c8-9e30-28b35080b55c'),
(223, 220, 'en_us', 'ms-ge-30-web', NULL, 1, '2015-07-21 03:21:47', '2015-07-21 03:21:47', '345f6452-be68-4645-b7c7-281d49bf01e3'),
(224, 221, 'en_us', 'ms-ge-36-web', NULL, 1, '2015-07-21 03:21:50', '2015-07-21 03:21:50', '38ca3c13-cbe5-4036-9c49-ffa1d518e4b5'),
(225, 222, 'en_us', 'ms-south-africa-german-express-5481-web', NULL, 1, '2015-07-21 03:21:53', '2015-07-21 03:21:53', '166c0472-3089-4cb8-901d-5f2c8063aef9'),
(226, 223, 'en_us', 'ms-south-africa-german-express-5484-web', NULL, 1, '2015-07-21 03:21:56', '2015-07-21 03:21:56', '31207760-ac16-45e9-96ea-7bae36daf0f6'),
(227, 224, 'en_us', 'ms-south-africa-german-express-5487-web', NULL, 1, '2015-07-21 03:21:59', '2015-07-21 03:21:59', 'fee73983-e9a0-4f3f-bd73-ce604416d91d'),
(228, 225, 'en_us', 'ms-south-africa-german-express-5490-web', NULL, 1, '2015-07-21 03:22:02', '2015-07-21 03:22:02', '640676ae-be08-431b-aba2-04647a86e410'),
(229, 226, 'en_us', 'ms-south-africa-german-express-5499-web', NULL, 1, '2015-07-21 03:22:04', '2015-07-21 03:22:04', '072f7915-0d88-4731-874f-f508e2896ff6'),
(230, 227, 'en_us', 'ms-south-africa-german-express-5509-web', NULL, 1, '2015-07-21 03:22:09', '2015-07-21 03:22:09', 'd951f73e-93a7-4dda-8f8c-799f1900163f'),
(231, 228, 'en_us', 'ms-south-africa-german-express-5511-web', NULL, 1, '2015-07-21 03:22:11', '2015-07-21 03:22:11', '76ce3d36-2d4f-488a-a256-afd123bdf8de'),
(232, 229, 'en_us', 'ms-south-africa-german-express-5518-web', NULL, 1, '2015-07-21 03:22:20', '2015-07-21 03:22:20', 'b24c29ee-b86c-4d56-8dd8-abbdd118de72'),
(233, 230, 'en_us', 'ms-south-africa-german-express-5526-web', NULL, 1, '2015-07-21 03:22:27', '2015-07-21 03:22:27', 'd8e07550-7966-4877-a07a-7a5bd4ed7e40'),
(234, 231, 'en_us', 'ms-south-africa-german-express-5561-web', NULL, 1, '2015-07-21 03:22:30', '2015-07-21 03:22:30', 'e38d83b4-6f71-4fb5-a304-f375b44dcba3'),
(235, 232, 'en_us', 'ms-south-africa-german-express-5585-web', NULL, 1, '2015-07-21 03:22:33', '2015-07-21 03:22:33', 'fc9eede4-2584-4d25-8c4f-c8cbbc63a50c'),
(236, 233, 'en_us', '', NULL, 1, '2015-07-21 03:38:42', '2015-08-20 18:46:55', '3b26aefa-c71b-4e36-a4d6-9d9d334e6831'),
(237, 234, 'en_us', '', NULL, 1, '2015-07-21 03:41:09', '2015-08-20 18:46:55', '18591a28-922a-490c-92af-c2b337abaa6d'),
(238, 235, 'en_us', '', NULL, 1, '2015-07-21 03:46:28', '2015-08-20 18:46:55', 'e1e6f775-44c8-4da8-a8e6-fd6a889096aa'),
(239, 236, 'en_us', '', NULL, 1, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '3ce5100a-adb0-4dfc-9779-a2bc63c6142b'),
(240, 237, 'en_us', '', NULL, 1, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '02bf5836-f1c5-4a38-ab6a-7a9f0671e5e3'),
(241, 238, 'en_us', '', NULL, 1, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '9f5efda7-132b-4439-ba5f-b45b3fecc4fb'),
(242, 239, 'en_us', '', NULL, 1, '2015-07-21 03:46:28', '2015-08-20 18:46:55', 'e95a454f-61f2-4f9b-a76b-d63f125d5777'),
(243, 240, 'en_us', '', NULL, 1, '2015-07-21 03:46:28', '2015-08-20 18:46:56', '5a4bbdca-c3a9-4398-a792-07c0dffae735'),
(244, 241, 'en_us', '', NULL, 1, '2015-07-21 03:46:28', '2015-08-20 18:46:56', '1d764cae-0b4b-44c4-a83e-20ad664e8a46'),
(245, 242, 'en_us', 'coyote-v-acme-01-web', NULL, 1, '2015-07-23 23:03:47', '2015-07-23 23:03:47', 'f4f18b7c-d314-441e-b235-a438468cb1e4'),
(246, 243, 'en_us', 'coyote-v-acme-02-web', NULL, 1, '2015-07-23 23:03:48', '2015-07-23 23:03:48', 'fab6e6ea-1b3a-43c8-b790-4be832cb166c'),
(247, 244, 'en_us', 'coyote-v-acme-03-web', NULL, 1, '2015-07-23 23:03:49', '2015-07-23 23:03:49', '9f63eb4f-b551-4dac-adaa-64df56659406'),
(248, 245, 'en_us', 'coyote-v-acme-06-web', NULL, 1, '2015-07-23 23:03:50', '2015-07-23 23:03:50', '4f0fcd3e-b21d-40a0-9c40-b163c2a98b50'),
(249, 246, 'en_us', 'coyote-v-acme-11-web', NULL, 1, '2015-07-23 23:03:52', '2015-07-23 23:03:52', '502a8ebc-0781-4d24-9a06-1d71f3804a68'),
(250, 247, 'en_us', 'coyote-v-acme-13-web', NULL, 1, '2015-07-23 23:03:53', '2015-07-23 23:03:53', '1e661bc6-ab55-446e-9d7e-4ab7c0b860cc'),
(251, 248, 'en_us', 'coyote-v-acme-16-web', NULL, 1, '2015-07-23 23:03:55', '2015-07-23 23:03:55', '72f31f46-98cc-47f0-88aa-40aee1a6bf33'),
(252, 249, 'en_us', 'coyote-v-acme-18-web', NULL, 1, '2015-07-23 23:03:56', '2015-07-23 23:03:56', '58311ec2-3800-4641-9649-daa5f7551c99'),
(253, 250, 'en_us', 'coyote-v-acme-19-web', NULL, 1, '2015-07-23 23:03:58', '2015-07-23 23:03:58', '09dd673f-d537-4902-b2d4-cfdd912b9d8a'),
(254, 251, 'en_us', 'coyote-v-acme-20-web', NULL, 1, '2015-07-23 23:04:00', '2015-07-23 23:04:00', 'e6286b43-1b27-4b2a-b588-7e867dfef8cd'),
(255, 252, 'en_us', 'coyote-v-acme-23-web', NULL, 1, '2015-07-23 23:04:02', '2015-07-23 23:04:02', 'a11b9a7c-ef02-46f8-9c1e-1c7f7cdc80f0'),
(256, 253, 'en_us', 'coyote-v-acme-28-web', NULL, 1, '2015-07-23 23:04:03', '2015-07-23 23:04:03', '6fc5f49a-14ff-4536-b5c3-9cc6f941c264'),
(257, 254, 'en_us', 'coyote-v-acme-29-web', NULL, 1, '2015-07-23 23:04:05', '2015-07-23 23:04:05', 'f5bedcaf-99f9-49a2-a723-19800348dd29'),
(258, 255, 'en_us', 'coyote-v-acme', 'projects/coyote-v-acme', 1, '2015-07-23 23:17:51', '2015-08-20 18:47:17', '62358a35-bccb-40cf-b983-c3d9274b9e65'),
(259, 256, 'en_us', '', NULL, 1, '2015-07-23 23:17:51', '2015-08-20 18:47:17', '89ff0d30-e12a-4e19-a602-61c41887a1ba'),
(260, 257, 'en_us', '', NULL, 1, '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'ad3beb34-0150-43a8-8c4e-76490e20fa88'),
(261, 258, 'en_us', '', NULL, 1, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '114d2c61-33ed-424a-9f72-9bed96109377'),
(262, 259, 'en_us', '', NULL, 1, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '06a71638-c8b2-4aaa-a275-450ac8e5d4de'),
(263, 260, 'en_us', '', NULL, 1, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '9fbfc1f8-13d2-40a3-a1bb-4c26eeb5b833'),
(264, 261, 'en_us', '', NULL, 1, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '2c5c7f81-b585-4459-89ac-62bc62794d54'),
(265, 262, 'en_us', 'bigapps-symbol-web', NULL, 1, '2015-07-23 23:33:50', '2015-07-23 23:33:50', '965d2d03-5255-4558-949b-5e546cd3354e'),
(266, 263, 'en_us', 'bigapps-program-web', NULL, 1, '2015-07-23 23:40:15', '2015-07-23 23:40:15', '8cb71ecf-889f-4432-9d8f-e4fcbacb3b1e'),
(267, 264, 'en_us', 'bigapps-mayor-web', NULL, 1, '2015-07-23 23:40:36', '2015-07-23 23:40:36', '4df1028d-78f9-4408-bc10-3f17081cfb9e'),
(268, 265, 'en_us', 'bigapps-poster-detail-web', NULL, 1, '2015-07-23 23:40:49', '2015-07-23 23:40:49', 'cb2dd3ba-b0db-485c-a16e-a7f07d3c2bde'),
(269, 266, 'en_us', 'big-apps-nyc', 'projects/big-apps-nyc', 1, '2015-07-23 23:41:05', '2015-08-22 02:10:43', '4e5b4548-44dd-4f26-af52-0bd223d48035'),
(270, 267, 'en_us', 'bigapps-poster-v4-web', NULL, 1, '2015-07-23 23:41:23', '2015-07-23 23:41:23', '17c327a5-1638-4c4e-b6dc-b010882f1d2d'),
(271, 268, 'en_us', 'bigapps-screen-circles-web', NULL, 1, '2015-07-23 23:41:31', '2015-07-23 23:41:31', '9c76c2e4-490e-416a-b420-49697cde6df0'),
(272, 269, 'en_us', 'bigapps-bus-shelter-2014-11-cc-web', NULL, 1, '2015-07-23 23:50:12', '2015-07-23 23:50:12', 'c57f67e6-68e8-45ae-b589-5f65d4210217'),
(273, 270, 'en_us', 'bigapps-bus-shelter-2014-50-web', NULL, 1, '2015-07-23 23:50:24', '2015-07-23 23:50:24', '084f0fa3-add2-464b-bf72-10a6c9bbcbfd'),
(274, 271, 'en_us', 'bigapps-button-web', NULL, 1, '2015-07-23 23:53:21', '2015-07-23 23:53:21', 'e629f41f-c488-4af8-9a1f-c3e0032390ba'),
(275, 272, 'en_us', 'bigapps-bloom-web', NULL, 1, '2015-07-23 23:56:16', '2015-07-23 23:56:16', 'be13172a-15de-43c0-bc4b-8fdc0b4f47cf'),
(276, 273, 'en_us', '', NULL, 1, '2015-07-23 23:56:41', '2015-08-22 02:10:43', '0a58aef2-db59-42a1-8790-dff408fa9347'),
(277, 274, 'en_us', '', NULL, 1, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '7331663a-6b8d-4513-981c-b85d6415cb2b'),
(278, 275, 'en_us', '', NULL, 1, '2015-07-23 23:56:41', '2015-08-22 02:10:44', 'ec914b8a-dbda-480f-9b11-aba21e9bcc18'),
(279, 276, 'en_us', '', NULL, 1, '2015-07-23 23:56:41', '2015-08-22 02:10:44', 'e90c8b25-eb3c-4e09-ae3e-5c3da03fcbf9'),
(280, 277, 'en_us', '', NULL, 1, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '9b256335-70b8-4cbf-9077-452b2c61ad31'),
(281, 278, 'en_us', '', NULL, 1, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '69f8b471-5a2d-4da8-b891-d7194ed18c86'),
(282, 279, 'en_us', 'jessicajorgensen-absinthemakestheheartgrowfonder-web', NULL, 1, '2015-08-01 04:08:47', '2015-08-01 04:08:47', '491a7b0e-e211-4420-8a04-baebcefd6050'),
(283, 280, 'en_us', 'new-york-city-pedestrian-wayfinding', 'projects/new-york-city-pedestrian-wayfinding', 1, '2015-08-07 17:38:17', '2015-08-22 02:00:59', '546400a8-cd88-443a-966e-b63ad5e051e9'),
(284, 281, 'en_us', 'dot-taxi-140407-web', NULL, 1, '2015-08-07 17:42:48', '2015-08-07 17:42:48', 'e1f4f2d6-a94d-4212-a7e5-21791c8457e6'),
(285, 282, 'en_us', 'dot-truckloading2-140407-web', NULL, 1, '2015-08-07 17:46:17', '2015-08-07 17:46:17', '30dc4d63-d34d-42f8-9d03-7cbbee6e6110'),
(286, 283, 'en_us', '', NULL, 1, '2015-08-16 21:06:05', '2015-08-22 02:00:59', 'fe65c060-9bbe-44cd-910e-d38ea8c7b45f'),
(288, 285, 'en_us', 'dot-icons', NULL, 1, '2015-08-16 21:14:13', '2015-08-16 21:56:00', 'cb46bcff-9ae6-4714-9f7f-e9fc2c17e938'),
(290, 287, 'en_us', 'dot-icons2', NULL, 1, '2015-08-16 21:55:13', '2015-08-16 21:55:13', 'ba6f5344-5edf-44cb-9a27-f80a50b7eeb6'),
(291, 288, 'en_us', 'dot-icons3', NULL, 1, '2015-08-16 21:55:15', '2015-08-16 21:55:15', 'a98cf727-e86b-4afc-8706-91140199a329'),
(292, 289, 'en_us', 'dot-icons4', NULL, 1, '2015-08-16 21:55:16', '2015-08-16 21:55:16', '66ad12d6-9a15-4b53-b888-0e5da068dcd7'),
(293, 290, 'en_us', 'dot-icons5', NULL, 1, '2015-08-16 21:55:18', '2015-08-16 21:55:18', 'f6e5865e-22f7-4c12-a67e-722b6a27f061'),
(294, 291, 'en_us', 'dot-icons6', NULL, 1, '2015-08-16 21:55:20', '2015-08-16 21:55:20', '52d33e79-c31a-4318-b904-08e38e33bdf9'),
(295, 292, 'en_us', 'dot-icons7', NULL, 1, '2015-08-16 21:55:22', '2015-08-16 21:55:22', '4d2de3cb-b320-4423-8da1-3ba09f578d4d'),
(296, 293, 'en_us', 'dot-icons8', NULL, 1, '2015-08-16 21:55:23', '2015-08-16 21:55:23', 'd46cae80-a8a2-4b97-a85a-1e39ee540054'),
(297, 294, 'en_us', 'dot-icons9', NULL, 1, '2015-08-16 21:55:25', '2015-08-16 21:55:25', 'e29c84dd-e622-4eb1-9d6b-f874549d6095'),
(298, 295, 'en_us', 'dot-icons10', NULL, 1, '2015-08-16 21:55:26', '2015-08-16 21:55:26', '798e506c-12ec-4b3d-b442-32362ca1aca4'),
(299, 296, 'en_us', 'dot-icons11', NULL, 1, '2015-08-16 21:55:27', '2015-08-16 21:55:27', '1aadf5dc-67f0-4bf2-aa6b-d2ffdf71bfad'),
(300, 297, 'en_us', 'dot-icons12', NULL, 1, '2015-08-16 21:55:30', '2015-08-16 21:55:30', 'df11d5ae-06fa-4c3b-99d6-bd557925e849'),
(301, 298, 'en_us', 'dot-icons13', NULL, 1, '2015-08-16 21:55:31', '2015-08-16 21:55:31', '2bc5793b-0e76-4997-8c1a-a5f3e84ac721'),
(302, 299, 'en_us', 'dot-icons14', NULL, 1, '2015-08-16 21:55:33', '2015-08-16 21:55:33', 'b1a6cc6f-38c3-4e4a-bb8f-83fef1ee7001'),
(303, 300, 'en_us', 'dot-icons15', NULL, 1, '2015-08-16 21:55:35', '2015-08-16 21:55:35', '98e12ee1-2f4d-4546-b49a-cf8a4c6f7eee'),
(304, 301, 'en_us', 'dot-icons16', NULL, 1, '2015-08-16 21:55:36', '2015-08-16 21:55:36', '644217a2-67e0-4df4-a172-b446ac234148'),
(305, 302, 'en_us', 'dot-icons17', NULL, 1, '2015-08-16 21:55:37', '2015-08-16 21:55:37', 'f9ce97c2-88e6-45e0-9be5-26e003820b3b'),
(306, 303, 'en_us', 'dot-icons18', NULL, 1, '2015-08-16 21:55:39', '2015-08-16 21:55:39', '806f5ec2-fd66-4f2c-9701-d69cb8096806'),
(307, 304, 'en_us', 'dot-icons19', NULL, 1, '2015-08-16 21:55:41', '2015-08-16 21:55:41', '79b0215a-65e0-4e16-a6df-fa1268c07465'),
(308, 305, 'en_us', 'dot-icons20', NULL, 1, '2015-08-16 21:55:42', '2015-08-16 21:55:42', '97906bd7-7985-482a-ae91-d3327e0c9747'),
(309, 306, 'en_us', 'dot-icons21', NULL, 1, '2015-08-16 21:55:44', '2015-08-16 21:55:44', '418bb012-0f84-4ef3-a486-717ce57b776d'),
(310, 307, 'en_us', 'dot-icons22', NULL, 1, '2015-08-16 21:55:46', '2015-08-16 21:55:46', 'a1e10d27-b3b3-4ba5-910f-b3b480521b3e'),
(311, 308, 'en_us', 'dot-icons23', NULL, 1, '2015-08-16 21:55:47', '2015-08-16 21:55:47', '939e0c2c-c187-4853-bbdb-793fcda83df8'),
(312, 309, 'en_us', '', NULL, 1, '2015-08-16 21:58:19', '2015-08-22 02:00:59', 'b2902fd9-e73b-4279-977b-04a4e40b6e9c'),
(313, 310, 'en_us', '', NULL, 1, '2015-08-16 21:58:19', '2015-08-22 02:00:59', '25502215-8935-49f5-9100-08c3b33c8c92'),
(314, 311, 'en_us', 'dot-sign-chinatown-web', NULL, 1, '2015-08-18 13:29:19', '2015-08-18 13:29:19', '44669499-f59c-48fe-9a12-e893b46d0ad8'),
(315, 312, 'en_us', 'dot-type', NULL, 1, '2015-08-18 13:46:46', '2015-08-18 13:46:46', '24e2b24a-abb6-463b-8528-da35d23b23d7'),
(316, 313, 'en_us', 'dot-type2', NULL, 1, '2015-08-18 13:46:47', '2015-08-18 13:46:47', 'f2350109-53b9-44c3-a787-323e4f757d4c'),
(317, 314, 'en_us', '', NULL, 1, '2015-08-18 14:06:35', '2015-08-22 02:00:59', '70069a83-40bc-4604-a701-fe90f365d63f'),
(318, 315, 'en_us', '', NULL, 1, '2015-08-18 14:06:35', '2015-08-22 02:00:59', 'dd3761eb-5f94-4153-9cb9-094e0dd7bd90'),
(319, 316, 'en_us', '', NULL, 1, '2015-08-18 14:06:35', '2015-08-22 02:00:59', '210d7196-7c08-4f42-9216-df8d86d75a31'),
(320, 317, 'en_us', '', NULL, 1, '2015-08-18 14:06:36', '2015-08-22 02:00:59', '9e4c3fe1-b800-4fac-9a9e-9f27d601a1f5'),
(321, 318, 'en_us', 'dot-seck1', NULL, 1, '2015-08-19 02:43:13', '2015-08-19 02:43:13', 'bdfb4644-9602-41da-b92a-d603cc796be9'),
(322, 319, 'en_us', 'dot-seck2', NULL, 1, '2015-08-19 02:43:15', '2015-08-19 02:43:15', 'dded3729-bc6f-406d-9a17-3801fcb5639d'),
(323, 320, 'en_us', 'dot-seck3', NULL, 1, '2015-08-19 02:43:17', '2015-08-19 02:43:17', '932c4549-9d7f-4cbb-9fae-3a8a811c3991'),
(324, 321, 'en_us', 'dot-seck4', NULL, 1, '2015-08-19 02:43:19', '2015-08-19 02:43:19', '69caef8a-ddff-4482-939d-2fef67c3904a'),
(325, 322, 'en_us', '', NULL, 1, '2015-08-19 02:47:43', '2015-08-22 02:00:59', '036b9a56-11f7-46e3-8797-eea7fc94c60b'),
(326, 323, 'en_us', '', NULL, 1, '2015-08-19 02:47:43', '2015-08-22 02:00:59', 'fe190fe6-22db-4929-8645-cce37f0b7a9a'),
(327, 324, 'en_us', '', NULL, 1, '2015-08-19 02:47:44', '2015-08-22 02:00:59', '11e4b570-8f12-4545-8291-0c8b22da165b'),
(328, 325, 'en_us', 'dot-ration', NULL, 1, '2015-08-19 03:35:05', '2015-08-19 03:35:05', '74fce37f-6be1-4a86-8d74-21fc252b7339'),
(329, 326, 'en_us', 'dot-grid', NULL, 1, '2015-08-19 03:42:06', '2015-08-19 03:42:06', '4458976f-0197-43bf-b6b9-bca2c8850f89'),
(330, 327, 'en_us', 'dot-type-med', NULL, 1, '2015-08-19 03:52:10', '2015-08-19 03:52:10', '535ca787-ed5f-4e64-83ba-75d8867d6d1b'),
(331, 328, 'en_us', 'dot-type-med2', NULL, 1, '2015-08-19 03:52:14', '2015-08-19 03:52:14', '446f85cd-0de2-43d0-911e-c6ef00cb9e58'),
(332, 329, 'en_us', '', NULL, 1, '2015-08-19 03:53:43', '2015-08-22 02:00:59', 'c9c3f17d-400a-4cbd-95ba-18f192f60c99'),
(333, 330, 'en_us', '', NULL, 1, '2015-08-19 03:53:43', '2015-08-22 02:00:59', '1177c10b-5fa0-47a2-8e72-78aa563858c6'),
(334, 331, 'en_us', 'jrfo-logotype-crops', NULL, 1, '2015-08-20 19:36:37', '2015-08-20 19:36:37', '65115b0a-bbff-4203-af5d-b18909593285'),
(335, 332, 'en_us', 'jrfo-logotype-crops2', NULL, 1, '2015-08-20 19:36:40', '2015-08-20 19:36:40', '19249df7-2f50-4879-88ec-ed3ee116ca01'),
(336, 333, 'en_us', 'jrfo-logotype', NULL, 1, '2015-08-20 19:36:44', '2015-08-20 19:36:44', '3e02ba93-fcbe-4225-9b02-1f0985d7c3f2'),
(337, 334, 'en_us', 'jrfo-logotype2', NULL, 1, '2015-08-20 19:36:47', '2015-08-20 19:36:47', 'df30e9be-0907-4437-8303-6cbd09460e19'),
(338, 335, 'en_us', 'jrfo-logotype3', NULL, 1, '2015-08-20 19:36:50', '2015-08-20 19:36:50', 'a61c2b0d-d3eb-4985-a724-5404e3f52e2e'),
(339, 336, 'en_us', 'jrfo-logotype4', NULL, 1, '2015-08-20 19:36:54', '2015-08-20 19:36:54', '1b9cc8ab-e83a-4e59-a381-e18487337f5f'),
(340, 337, 'en_us', 'jrfo-logotype5', NULL, 1, '2015-08-20 19:36:57', '2015-08-20 19:36:57', '52927125-2628-434b-ad36-4ac980504389'),
(341, 338, 'en_us', 'jrfo-logotype6', NULL, 1, '2015-08-20 19:37:00', '2015-08-20 19:37:00', '3118750c-4dad-497d-a104-32deb90422f4'),
(342, 339, 'en_us', 'jrfo-logotype7', NULL, 1, '2015-08-20 19:37:04', '2015-08-20 19:37:04', '24f9b1c0-d753-4fc1-a0db-00953399c094'),
(343, 340, 'en_us', 'jrfo-logotype8', NULL, 1, '2015-08-20 19:37:07', '2015-08-20 19:37:07', 'a559676e-7034-410f-b3e9-e8924e6207e6'),
(344, 341, 'en_us', 'jrfo-logotype9', NULL, 1, '2015-08-20 19:37:10', '2015-08-20 19:37:10', 'af21f3e8-94d1-4e45-ba28-99745d45a1ee'),
(345, 342, 'en_us', 'jrfo-logotype10', NULL, 1, '2015-08-20 19:37:13', '2015-08-20 19:37:13', '19f61ff7-0f4e-463b-ba14-8c2b735beb67'),
(346, 343, 'en_us', 'marks-logotypes', 'projects/marks-logotypes', 1, '2015-08-20 19:39:03', '2015-08-22 04:48:01', 'd228bc3c-b4b4-4d84-9de8-6c70226949ee'),
(347, 344, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '62ef8b2c-cd5b-4352-b530-6014adb14499'),
(348, 345, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '27128610-d0a8-4da6-9838-ff888adba4b3'),
(349, 346, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '42edeeab-a568-43e8-8aa1-3b026b295509'),
(350, 347, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'd15b0f39-b59a-4c39-a27e-ef4351624c29'),
(351, 348, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'e00dddcf-a225-4e76-925a-9c739d8f7f97'),
(352, 349, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'f28bba4d-4906-40d6-b88b-05a7f1185735'),
(353, 350, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '1e27fe65-3284-4566-aae9-54d3bb993bbc'),
(354, 351, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '0a8275eb-dc79-44e5-beff-f2cbf25b0b41'),
(355, 352, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'bde2bd1e-fad2-45bd-9202-f96a60453d1b'),
(356, 353, 'en_us', '', NULL, 1, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '2eaeb52b-a5fc-4c6e-a0ff-a78977c36773'),
(357, 354, 'en_us', '', NULL, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '1c4dd895-39f2-4b0d-8ceb-54a4055dcb1c'),
(358, 355, 'en_us', '', NULL, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '3505115a-54f5-4e1e-a029-1a94257fe8ae'),
(359, 356, 'en_us', '', NULL, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'e2e2b3a4-cfa3-4c26-a30f-73ccd5ef1214'),
(360, 357, 'en_us', '', NULL, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', 'cb287d31-1abe-4037-84a6-0b9c0798c00f'),
(361, 358, 'en_us', '', NULL, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '49aaeb05-ad61-4b39-9727-946d0b68df0b'),
(362, 359, 'en_us', '', NULL, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '12532345-d6dd-4aa9-9f1e-c871481fad82'),
(363, 360, 'en_us', '', NULL, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '14d6ea57-0b2f-4470-8acd-df96dc275a5c'),
(364, 361, 'en_us', '', NULL, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '132f7a5c-3091-4a82-9761-01c5d11aa0dc');

-- --------------------------------------------------------

--
-- Table structure for table `craft_emailmessages`
--

CREATE TABLE IF NOT EXISTS `craft_emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_emailmessages_key_locale_unq_idx` (`key`,`locale`),
  KEY `craft_emailmessages_locale_fk` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_entries`
--

CREATE TABLE IF NOT EXISTS `craft_entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entries_sectionId_idx` (`sectionId`),
  KEY `craft_entries_typeId_idx` (`typeId`),
  KEY `craft_entries_postDate_idx` (`postDate`),
  KEY `craft_entries_expiryDate_idx` (`expiryDate`),
  KEY `craft_entries_authorId_fk` (`authorId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `craft_entries`
--

INSERT INTO `craft_entries` (`id`, `sectionId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(13, 6, 8, 1, '2015-08-06 19:46:00', NULL, '2015-05-05 19:46:22', '2015-08-22 02:08:24', '6e9e987e-cb74-4fa3-9469-ae3fb5671d12'),
(15, 6, 8, 1, '2015-08-02 20:26:00', NULL, '2015-05-05 20:26:19', '2015-08-22 02:16:04', 'dbdd442f-92a7-4094-9c84-21c480d47d2a'),
(73, 6, 8, 1, '2015-08-07 21:05:00', NULL, '2015-07-13 21:05:34', '2015-08-22 02:29:18', '1ea018ea-46ba-4647-b219-943e19554202'),
(215, 6, 8, 1, '2015-08-01 03:12:00', NULL, '2015-07-21 03:12:40', '2015-08-20 18:46:56', '150459d4-2ea2-43cd-912a-410da9a39459'),
(255, 6, 8, 1, '2015-08-03 23:17:00', NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '73577641-75c7-4766-aeb1-28b4c5b6ec0f'),
(266, 6, 8, 1, '2015-08-04 23:41:00', NULL, '2015-07-23 23:41:05', '2015-08-22 02:10:44', 'd075e51b-1242-4c52-8c7b-59345d27e859'),
(280, 6, 8, 1, '2015-08-05 17:38:00', NULL, '2015-08-07 17:38:17', '2015-08-22 02:00:59', '5326287b-844f-46a0-a902-92bb7068aee8'),
(343, 6, 8, 1, '2015-07-31 19:39:00', NULL, '2015-08-20 19:39:03', '2015-08-22 04:48:01', 'd13970bd-9e83-4d0e-a68b-3efeb4046ed2');

-- --------------------------------------------------------

--
-- Table structure for table `craft_entrydrafts`
--

CREATE TABLE IF NOT EXISTS `craft_entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entrydrafts_entryId_locale_idx` (`entryId`,`locale`),
  KEY `craft_entrydrafts_sectionId_fk` (`sectionId`),
  KEY `craft_entrydrafts_creatorId_fk` (`creatorId`),
  KEY `craft_entrydrafts_locale_fk` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_entrytypes`
--

CREATE TABLE IF NOT EXISTS `craft_entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Title',
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `craft_entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `craft_entrytypes_sectionId_fk` (`sectionId`),
  KEY `craft_entrytypes_fieldLayoutId_fk` (`fieldLayoutId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `craft_entrytypes`
--

INSERT INTO `craft_entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(8, 6, 45, 'projects', 'projects', 1, 'Title', NULL, NULL, '2015-05-05 19:43:47', '2015-07-12 02:35:24', '42cb0c3a-a442-49fb-bb4c-7dfd58301df7');

-- --------------------------------------------------------

--
-- Table structure for table `craft_entryversions`
--

CREATE TABLE IF NOT EXISTS `craft_entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entryversions_entryId_locale_idx` (`entryId`,`locale`),
  KEY `craft_entryversions_sectionId_fk` (`sectionId`),
  KEY `craft_entryversions_creatorId_fk` (`creatorId`),
  KEY `craft_entryversions_locale_fk` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_fieldgroups`
--

CREATE TABLE IF NOT EXISTS `craft_fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `craft_fieldgroups`
--

INSERT INTO `craft_fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Default', '2014-12-30 09:30:30', '2014-12-30 09:30:30', 'a9f6d185-ae49-413f-bebc-da9ef0ad4e62');

-- --------------------------------------------------------

--
-- Table structure for table `craft_fieldlayoutfields`
--

CREATE TABLE IF NOT EXISTS `craft_fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `craft_fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayoutfields_tabId_fk` (`tabId`),
  KEY `craft_fieldlayoutfields_fieldId_fk` (`fieldId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=78 ;

--
-- Dumping data for table `craft_fieldlayoutfields`
--

INSERT INTO `craft_fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 3, 1, 1, 1, 1, '2014-12-30 09:30:30', '2014-12-30 09:30:30', 'b87d9675-6679-4487-8419-7c8a5d908937'),
(6, 10, 5, 3, 0, 1, '2014-12-30 21:21:17', '2014-12-30 21:21:17', '141fc7c9-d9d1-44b9-a772-c84532c2baf3'),
(9, 10, 5, 1, 0, 4, '2014-12-30 21:21:17', '2014-12-30 21:21:17', '686be97b-5f72-4b9e-b91f-4bf136b390fb'),
(24, 25, 17, 1, 0, 1, '2015-05-05 19:16:34', '2015-05-05 19:16:34', 'c6e892b7-2ea4-44cb-ac15-ce5c302dbc37'),
(25, 25, 17, 3, 0, 2, '2015-05-05 19:16:34', '2015-05-05 19:16:34', '96cd3565-c511-49f1-96ca-5e1fdf5daf8d'),
(64, 45, 31, 10, 0, 1, '2015-07-12 02:35:24', '2015-07-12 02:35:24', 'aded492a-9ec1-424d-9996-5dc2750cb525'),
(65, 45, 31, 11, 0, 2, '2015-07-12 02:35:24', '2015-07-12 02:35:24', 'e35f506d-3897-4323-b855-8220c6c6af7f'),
(66, 45, 31, 3, 0, 3, '2015-07-12 02:35:24', '2015-07-12 02:35:24', 'd3510986-b6e3-4d59-9926-10a0daeec21f'),
(67, 45, 31, 17, 0, 4, '2015-07-12 02:35:24', '2015-07-12 02:35:24', '59c367d9-0956-4fd1-bd93-74bd99d53f52'),
(68, 45, 31, 9, 0, 5, '2015-07-12 02:35:24', '2015-07-12 02:35:24', '95afad20-27b5-4a2b-ba48-6ddb2fcaa17e'),
(69, 45, 31, 1, 0, 6, '2015-07-12 02:35:24', '2015-07-12 02:35:24', '5382f1ed-464d-4920-8c08-2ea75847a515'),
(70, 46, 32, 6, 0, 1, '2015-07-12 02:37:17', '2015-07-12 02:37:17', '0e3f2da7-0a88-4b83-82fe-2b4e03040f34'),
(71, 47, 33, 7, 0, 1, '2015-07-12 02:37:17', '2015-07-12 02:37:17', '5ddfd2b8-2718-4ffe-97dd-2cc93bd74944'),
(72, 48, 34, 8, 0, 1, '2015-07-12 02:37:17', '2015-07-12 02:37:17', '18d357bd-0b99-47e3-8e0c-4ba53a6f35c2'),
(73, 48, 34, 12, 0, 2, '2015-07-12 02:37:17', '2015-07-12 02:37:17', '9c785f05-6052-4cf5-892c-f1e0311e7945'),
(74, 49, 35, 13, 0, 1, '2015-07-12 02:37:17', '2015-07-12 02:37:17', '003127c5-25e9-4269-bced-81b3e19fe6ce'),
(75, 49, 35, 14, 0, 2, '2015-07-12 02:37:17', '2015-07-12 02:37:17', '5816d7ca-3280-4e54-b985-33a71ab83466'),
(76, 49, 35, 15, 0, 3, '2015-07-12 02:37:17', '2015-07-12 02:37:17', 'dcbf37e9-c599-4603-bbc3-b127a7258a4b'),
(77, 49, 35, 16, 0, 4, '2015-07-12 02:37:17', '2015-07-12 02:37:17', 'b26e2aa4-9a6f-436d-84b0-fd5dfa03bba6');

-- --------------------------------------------------------

--
-- Table structure for table `craft_fieldlayouts`
--

CREATE TABLE IF NOT EXISTS `craft_fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

--
-- Dumping data for table `craft_fieldlayouts`
--

INSERT INTO `craft_fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Tag', '2014-12-30 09:30:30', '2014-12-30 09:30:30', 'b1a3e058-fffa-4fab-a824-d4d8baddf487'),
(3, 'Entry', '2014-12-30 09:30:30', '2014-12-30 09:30:30', 'fcdd34f9-58fd-4311-a5ba-eabb147c7c9b'),
(6, 'Entry', '2014-12-30 21:10:36', '2014-12-30 21:10:36', 'ca87be60-4a4a-4316-8a0b-65c4bf777869'),
(10, 'Entry', '2014-12-30 21:21:17', '2014-12-30 21:21:17', '8798120b-0e82-42e2-be2b-e474e01100ce'),
(25, 'Entry', '2015-05-05 19:16:34', '2015-05-05 19:16:34', '626c242a-c21c-49cc-ad2c-9ae35f6ac09c'),
(26, 'Entry', '2015-05-05 19:18:09', '2015-05-05 19:18:09', '9ce30ad4-7952-4341-94c9-b3fcf81cdb57'),
(34, 'Asset', '2015-05-08 06:36:34', '2015-05-08 06:36:34', 'e0981c42-e657-4eda-9f4e-e3ec4d129168'),
(45, 'Entry', '2015-07-12 02:35:24', '2015-07-12 02:35:24', '883bfe63-026d-4a9d-8aae-33286b1252f0'),
(46, 'MatrixBlock', '2015-07-12 02:37:17', '2015-07-12 02:37:17', '07ee3262-c766-40e2-b648-a1b44c579cf2'),
(47, 'MatrixBlock', '2015-07-12 02:37:17', '2015-07-12 02:37:17', '9b54280b-1896-4dbe-880c-dea128b40e10'),
(48, 'MatrixBlock', '2015-07-12 02:37:17', '2015-07-12 02:37:17', 'adbfbaf0-127c-447e-994b-c87b52973061'),
(49, 'MatrixBlock', '2015-07-12 02:37:17', '2015-07-12 02:37:17', '3213193b-1a4f-4522-bc90-e41e1f1b8d99');

-- --------------------------------------------------------

--
-- Table structure for table `craft_fieldlayouttabs`
--

CREATE TABLE IF NOT EXISTS `craft_fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayouttabs_layoutId_fk` (`layoutId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `craft_fieldlayouttabs`
--

INSERT INTO `craft_fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 3, 'Content', 1, '2014-12-30 09:30:30', '2014-12-30 09:30:30', 'b6ea12bd-e31a-4e80-8558-12c57f3804c5'),
(5, 10, 'Tab 1', 1, '2014-12-30 21:21:17', '2014-12-30 21:21:17', '55965773-80b6-46a8-9698-f094c0910998'),
(17, 25, 'Default', 1, '2015-05-05 19:16:34', '2015-05-05 19:16:34', '6f9b0249-87e4-436c-a215-01b8e12953b2'),
(31, 45, 'Default', 1, '2015-07-12 02:35:24', '2015-07-12 02:35:24', 'ea30a8fe-d3d1-4ec6-88ea-16df8e48c1aa'),
(32, 46, 'Content', 1, '2015-07-12 02:37:17', '2015-07-12 02:37:17', '40477598-0088-4d6c-9d16-90090aa26607'),
(33, 47, 'Content', 1, '2015-07-12 02:37:17', '2015-07-12 02:37:17', 'f09e2859-c3bc-47da-bac6-40f04adc12d1'),
(34, 48, 'Content', 1, '2015-07-12 02:37:17', '2015-07-12 02:37:17', 'a5428e4a-3611-487d-b3c4-4b640b360a9b'),
(35, 49, 'Content', 1, '2015-07-12 02:37:17', '2015-07-12 02:37:17', '41ff64ab-8efb-42b5-b957-dcb7d5d9a0e3');

-- --------------------------------------------------------

--
-- Table structure for table `craft_fields`
--

CREATE TABLE IF NOT EXISTS `craft_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(58) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `translatable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `craft_fields_context_idx` (`context`),
  KEY `craft_fields_groupId_fk` (`groupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `craft_fields`
--

INSERT INTO `craft_fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `translatable`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'Body', 'body', 'global', '', 0, 'Matrix', '{"maxBlocks":null}', '2014-12-30 09:30:30', '2015-07-12 02:37:17', '0cac6113-5dea-4286-9db6-ec96dc43600e'),
(3, 1, 'Description', 'description', 'global', '', 0, 'PlainText', '{"placeholder":"","maxLength":"200","multiline":"","initialRows":"4"}', '2014-12-30 21:12:13', '2015-05-05 18:35:57', 'ffddbd95-1fbe-4bc9-9c9e-7979685b0b5f'),
(6, NULL, 'Text', 'text', 'matrixBlockType:1', '', 0, 'PlainText', '{"placeholder":"","maxLength":"","multiline":"1","initialRows":"10"}', '2014-12-30 21:19:48', '2015-07-12 02:37:17', '4b4288f6-541d-467f-9175-a5d7e8691523'),
(7, NULL, 'image', 'image', 'matrixBlockType:2', '', 0, 'Assets', '{"useSingleFolder":"","sources":"*","defaultUploadLocationSource":"1","defaultUploadLocationSubpath":"\\/build\\/jessereed\\/html\\/_uploads\\/","singleUploadLocationSource":"1","singleUploadLocationSubpath":"","restrictFiles":"1","allowedKinds":["image"],"limit":"1"}', '2014-12-30 21:19:48', '2015-07-12 02:37:17', '0e6460ba-5064-4fc8-b72a-a6e4a13188af'),
(8, NULL, 'image', 'image', 'matrixBlockType:3', '', 0, 'Assets', '{"useSingleFolder":"1","sources":"*","defaultUploadLocationSource":"1","defaultUploadLocationSubpath":"\\/build\\/jessereed\\/html\\/_uploads\\/","singleUploadLocationSource":"1","singleUploadLocationSubpath":"","restrictFiles":"1","allowedKinds":["image"],"limit":"1"}', '2015-05-05 18:46:57', '2015-07-12 02:37:17', '2c4da3b7-beb3-45d0-981c-46acfb7db24a'),
(9, 1, 'First Image', 'mainThumbnail', 'global', '', 0, 'Assets', '{"useSingleFolder":"1","sources":"*","defaultUploadLocationSource":"1","defaultUploadLocationSubpath":"","singleUploadLocationSource":"1","singleUploadLocationSubpath":"","restrictFiles":"1","allowedKinds":["image"],"limit":"1"}', '2015-05-07 23:32:52', '2015-07-12 02:36:38', '3983a49d-ddaa-4e34-b60c-4da757ff1d0c'),
(10, 1, 'Meta', 'meta', 'global', 'Each credit separated by line break. Formatting (tabs) will happen automatically on the front end based on even/odd lines', 0, 'PlainText', '{"placeholder":"","maxLength":"","multiline":"1","initialRows":"10"}', '2015-05-08 00:01:03', '2015-05-08 00:02:41', 'e1df7d7b-03d2-41a6-9e4e-5f840b0fc3bf'),
(11, 1, 'Links', 'links', 'global', 'Use Markdown. Separate each link with double line break to create a single line break on front-end', 0, 'PlainText', '{"placeholder":"","maxLength":"","multiline":"1","initialRows":"5"}', '2015-05-08 00:12:11', '2015-05-08 00:12:11', '0947eb5b-4c6a-41ed-926c-ec73ffb2b35e'),
(12, NULL, 'image2', 'image2', 'matrixBlockType:3', '', 0, 'Assets', '{"useSingleFolder":"","sources":"*","defaultUploadLocationSource":"1","defaultUploadLocationSubpath":"","singleUploadLocationSource":"1","singleUploadLocationSubpath":"","restrictFiles":"","limit":"1"}', '2015-05-14 06:14:13', '2015-07-12 02:37:17', '0dca456d-b3e6-4053-86b2-7af81925237d'),
(13, NULL, 'image', 'image', 'matrixBlockType:4', '', 0, 'Assets', '{"useSingleFolder":"","sources":"*","defaultUploadLocationSource":"1","defaultUploadLocationSubpath":"","singleUploadLocationSource":"1","singleUploadLocationSubpath":"","restrictFiles":"","limit":"1"}', '2015-05-14 06:14:13', '2015-07-12 02:37:17', '4b9f74c9-cbaa-476b-8805-76e0e0b466e4'),
(14, NULL, 'caption', 'caption', 'matrixBlockType:4', '', 0, 'PlainText', '{"placeholder":"","maxLength":"","multiline":"","initialRows":"4"}', '2015-05-14 06:14:13', '2015-07-12 02:37:17', 'bf593188-38ad-4867-a2c3-03fc42787d5c'),
(15, NULL, 'image2', 'image2', 'matrixBlockType:4', '', 0, 'Assets', '{"useSingleFolder":"","sources":"*","defaultUploadLocationSource":"1","defaultUploadLocationSubpath":"","singleUploadLocationSource":"1","singleUploadLocationSubpath":"","restrictFiles":"","limit":"1"}', '2015-05-14 06:14:13', '2015-07-12 02:37:17', 'ca24f4bc-45e3-4018-95f5-9ab426385965'),
(16, NULL, 'caption2', 'caption2', 'matrixBlockType:4', '', 0, 'PlainText', '{"placeholder":"","maxLength":"","multiline":"","initialRows":"4"}', '2015-05-14 06:14:13', '2015-07-12 02:37:17', 'bb96c094-8cc0-4548-ae94-ee5f40151537'),
(17, 1, 'Thumbnails', 'thumbnails', 'global', '', 0, 'Assets', '{"useSingleFolder":"","sources":"*","defaultUploadLocationSource":"1","defaultUploadLocationSubpath":"","singleUploadLocationSource":"1","singleUploadLocationSubpath":"","restrictFiles":"","limit":""}', '2015-07-12 02:00:51', '2015-07-12 02:00:51', '92add108-f8c3-484f-a7e5-38da2798c38f');

-- --------------------------------------------------------

--
-- Table structure for table `craft_globalsets`
--

CREATE TABLE IF NOT EXISTS `craft_globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `craft_globalsets_handle_unq_idx` (`handle`),
  KEY `craft_globalsets_fieldLayoutId_fk` (`fieldLayoutId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `craft_info`
--

CREATE TABLE IF NOT EXISTS `craft_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `build` int(11) unsigned NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `releaseDate` datetime NOT NULL,
  `edition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `siteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `track` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `craft_info`
--

INSERT INTO `craft_info` (`id`, `version`, `build`, `schemaVersion`, `releaseDate`, `edition`, `siteName`, `siteUrl`, `timezone`, `on`, `maintenance`, `track`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, '2.3', 2644, '2.3.4', '2015-04-22 20:03:42', 0, 'Jesse Reed', 'http://jessereedfromohio.com.s206686.gridserver.com/', 'UTC', 1, 0, 'stable', '2014-12-30 09:30:25', '2015-05-08 06:30:47', 'f0293326-3fe3-433b-b60d-0cf87da69ced');

-- --------------------------------------------------------

--
-- Table structure for table `craft_locales`
--

CREATE TABLE IF NOT EXISTS `craft_locales` (
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`locale`),
  KEY `craft_locales_sortOrder_idx` (`sortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `craft_locales`
--

INSERT INTO `craft_locales` (`locale`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
('en_us', 1, '2014-12-30 09:30:25', '2014-12-30 09:30:25', 'be588baa-a5d6-479e-874c-40f6bc5b9ac8');

-- --------------------------------------------------------

--
-- Table structure for table `craft_matrixblocks`
--

CREATE TABLE IF NOT EXISTS `craft_matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_matrixblocks_ownerId_idx` (`ownerId`),
  KEY `craft_matrixblocks_fieldId_idx` (`fieldId`),
  KEY `craft_matrixblocks_typeId_idx` (`typeId`),
  KEY `craft_matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `craft_matrixblocks_ownerLocale_fk` (`ownerLocale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `craft_matrixblocks`
--

INSERT INTO `craft_matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `ownerLocale`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(100, 73, 1, 2, 1, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:17', 'b44bc52e-6fe9-42ea-9bf3-17267a97d405'),
(101, 73, 1, 3, 2, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:18', '4618f572-0bdb-4906-882d-c173fd287f24'),
(102, 73, 1, 2, 3, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:18', 'eeb41ca0-a380-4b38-92e0-8518e9edb2e1'),
(103, 73, 1, 4, 4, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:18', 'e00bf046-18de-402d-b0c2-c82121487544'),
(104, 73, 1, 2, 5, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:18', 'aaad2f9c-8b91-47f6-aeab-2ae29b731704'),
(105, 73, 1, 3, 6, NULL, '2015-07-14 19:50:23', '2015-08-22 02:29:18', '7f69fcfd-68b7-47e5-88bb-095bdcd739c1'),
(106, 73, 1, 4, 7, NULL, '2015-07-14 19:50:23', '2015-08-22 02:29:18', 'e130c1f8-f687-406f-95c8-f304cb9c873d'),
(107, 73, 1, 2, 8, NULL, '2015-07-14 19:50:23', '2015-08-22 02:29:18', '8a8be347-ff08-4898-92ad-651459333261'),
(127, 13, 1, 4, 1, NULL, '2015-07-14 21:21:25', '2015-08-22 02:08:24', 'f9c83324-55b2-40a5-a82f-ee6ce6b08fb1'),
(128, 13, 1, 2, 2, NULL, '2015-07-14 21:21:25', '2015-08-22 02:08:24', '64070c8a-81b8-4f5d-affa-e3a44f2cfc1d'),
(132, 13, 1, 4, 3, NULL, '2015-07-14 21:33:27', '2015-08-22 02:08:24', 'c2228cac-1002-4821-b925-5dfda06b65f5'),
(133, 13, 1, 2, 6, NULL, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '82dab46c-df9e-4d6d-b20b-274cb80e7861'),
(134, 13, 1, 3, 7, NULL, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '75129e08-db85-42f8-9fb9-786ffaf19e4c'),
(135, 13, 1, 2, 8, NULL, '2015-07-14 21:33:28', '2015-08-22 02:08:24', '53448652-769c-4462-ab81-1de6e3043f3f'),
(155, 15, 1, 4, 1, NULL, '2015-07-14 22:01:42', '2015-08-22 02:16:03', '4aaa3f0c-7091-44d4-89f5-c0f6949cb55b'),
(156, 15, 1, 2, 2, NULL, '2015-07-14 22:01:43', '2015-08-22 02:16:03', '6553cfbe-7cf1-41ff-a777-5695430f027d'),
(157, 15, 1, 3, 3, NULL, '2015-07-14 22:01:43', '2015-08-22 02:16:04', 'bf459aa7-1c67-47c6-ba56-ca2dcf36f860'),
(213, 15, 1, 4, 4, NULL, '2015-07-21 02:48:57', '2015-08-22 02:16:04', 'c5103468-e782-42e0-ba8d-2e10c8156d8f'),
(214, 15, 1, 2, 5, NULL, '2015-07-21 02:48:57', '2015-08-22 02:16:04', 'f3e4b224-260f-4ac4-8f86-1584cb5205af'),
(233, 215, 1, 2, 1, NULL, '2015-07-21 03:38:42', '2015-08-20 18:46:55', '64596d5c-3f88-4a26-a753-b8e65985821b'),
(234, 215, 1, 4, 2, NULL, '2015-07-21 03:41:09', '2015-08-20 18:46:55', '26e63a42-cb27-48df-8e15-2b9a70db59f2'),
(235, 215, 1, 2, 3, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '15ac8197-217b-41a0-b09b-0e9531b6b203'),
(236, 215, 1, 4, 4, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:55', 'af38fd72-d7cd-4d36-81f5-5a27aad352ec'),
(237, 215, 1, 3, 5, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:55', 'a7ff01fa-e08c-45a8-b33b-baf1569e5be6'),
(238, 215, 1, 2, 6, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '7279a6be-6f6b-48f1-a3ea-8a4556fd67d6'),
(239, 215, 1, 3, 7, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:56', '60f0705a-c023-4447-aa40-494b0b0a605b'),
(240, 215, 1, 3, 8, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:56', '5dc15bcd-4fdd-4e75-895e-63143623de12'),
(241, 215, 1, 2, 9, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:56', '2318497b-d13c-49d2-ba6d-502e3caf589b'),
(256, 255, 1, 2, 1, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:17', '026559c6-9201-4317-be6d-86bc2073411a'),
(257, 255, 1, 4, 2, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'ed78954e-09a7-4c59-b625-ab05fb467fe7'),
(258, 255, 1, 3, 3, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'e26b121d-9aa8-469e-b691-4bc0b0c92b24'),
(259, 255, 1, 2, 4, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '7185a2fe-7fc8-44f3-9817-4088dfc4f7d2'),
(260, 255, 1, 2, 5, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'dd7f8a76-92cc-4311-be3a-716e5ce15d97'),
(261, 255, 1, 2, 6, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '1e86b061-a5f2-457d-b947-8f268a9f5aef'),
(273, 266, 1, 2, 1, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:43', '99d1397b-4160-4d08-bcd4-4b6a539c3960'),
(274, 266, 1, 4, 2, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '1dd64280-e909-4a32-ba8e-1262288ef825'),
(275, 266, 1, 2, 3, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '81c081f1-9d78-42f4-9488-bc55977bc84b'),
(276, 266, 1, 3, 4, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '61018568-bfac-4b09-bff9-0a4ea06c961c'),
(277, 266, 1, 4, 5, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '650a0ee3-c155-4e64-81a5-accdc59856f3'),
(278, 266, 1, 2, 6, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:44', 'f0ded235-2b53-4dd6-ad3d-39cc59babb14'),
(283, 280, 1, 3, 4, NULL, '2015-08-16 21:06:05', '2015-08-22 02:00:59', 'f62a2d4a-338a-4217-9e9c-970d0959a2ed'),
(309, 280, 1, 3, 5, NULL, '2015-08-16 21:58:19', '2015-08-22 02:00:59', '21c0ef36-33d6-4209-ab63-4653702f0e04'),
(310, 280, 1, 2, 6, NULL, '2015-08-16 21:58:19', '2015-08-22 02:00:59', '253c3e68-261a-4a43-a229-524f5bc83965'),
(314, 280, 1, 4, 1, NULL, '2015-08-18 14:06:35', '2015-08-22 02:00:59', 'f636ffc1-20d9-4e73-8579-26ff687ad8b0'),
(315, 280, 1, 3, 8, NULL, '2015-08-18 14:06:35', '2015-08-22 02:00:59', '7c980dc1-7731-42bb-89f1-918f4c6d2ae0'),
(316, 280, 1, 4, 10, NULL, '2015-08-18 14:06:35', '2015-08-22 02:00:59', '3514d05b-ebec-4b4b-9d63-7a8d96001965'),
(317, 280, 1, 2, 11, NULL, '2015-08-18 14:06:36', '2015-08-22 02:00:59', '150e67f3-a4d8-4f0a-83d2-8c555bdcb1bf'),
(322, 280, 1, 2, 7, NULL, '2015-08-19 02:47:43', '2015-08-22 02:00:59', 'ef93b118-b8d1-4929-93dc-33c2872bf90d'),
(323, 280, 1, 3, 9, NULL, '2015-08-19 02:47:44', '2015-08-22 02:00:59', '71aef323-fc86-4113-8bf2-587f94b1ff32'),
(324, 280, 1, 2, 3, NULL, '2015-08-19 02:47:44', '2015-08-22 02:00:59', '2a6a18bd-e32f-4d89-b4ba-3f97ed217cc3'),
(329, 280, 1, 4, 2, NULL, '2015-08-19 03:53:43', '2015-08-22 02:00:59', '267fd31b-4a0a-407e-bdfc-d05494d13d65'),
(330, 280, 1, 3, 13, NULL, '2015-08-19 03:53:44', '2015-08-22 02:00:59', '9c446fd4-0e01-46ae-ba2f-a697fb29c8cb'),
(344, 343, 1, 2, 1, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '8bd2282e-4784-4626-abf6-e0525da1315a'),
(345, 343, 1, 2, 6, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'a83f08ed-b451-402b-a2df-83c55dc14bc7'),
(346, 343, 1, 2, 2, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '5a16405b-2bfd-4b34-a7d9-9466a53ab5d5'),
(347, 343, 1, 2, 3, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '2e908aaf-df1e-4377-8ad8-bae5bf7cac99'),
(348, 343, 1, 2, 4, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '8c349724-9b8d-46dc-a50b-da8b8e74337a'),
(349, 343, 1, 2, 5, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'f482dbb1-0150-4195-8f91-d697ed45dc6d'),
(350, 343, 1, 2, 7, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '86c7dad8-fc75-42f2-9975-ac457b1d168b'),
(351, 343, 1, 2, 8, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'bb0406d3-c9a5-4a0e-b88d-e3326c681103'),
(352, 343, 1, 2, 9, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '32f9f2b8-feae-4cd2-bf68-9581bffba89e'),
(353, 343, 1, 2, 10, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'bf5432bd-0ebc-4ac4-9181-db724139f4f4'),
(354, 280, 1, 2, 12, NULL, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '3dcae93a-7aa1-4331-9ebe-9269358dfa6e'),
(355, 280, 1, 3, 14, NULL, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'cbe27b38-3f1b-4bb0-9631-8727962fc993'),
(356, 280, 1, 2, 15, NULL, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '7f6d689b-eae2-465e-90f0-90eabe410a0a'),
(357, 13, 1, 2, 4, NULL, '2015-08-22 02:08:24', '2015-08-22 02:08:24', 'b1e43c50-9d34-4a4f-9aa7-087736342343'),
(358, 13, 1, 2, 5, NULL, '2015-08-22 02:08:24', '2015-08-22 02:08:24', 'dc102ab3-30f6-4279-9fd1-ad9b63d44414'),
(359, 73, 1, 3, 9, NULL, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '3172e76a-5361-4b75-a6c7-2e796a803e6b'),
(360, 73, 1, 2, 10, NULL, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '1dc0351a-22c4-46ee-9fe8-e122788a614a'),
(361, 73, 1, 2, 11, NULL, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '13bfa122-38b3-4794-862e-63d0ecc2cbfe');

-- --------------------------------------------------------

--
-- Table structure for table `craft_matrixblocktypes`
--

CREATE TABLE IF NOT EXISTS `craft_matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `craft_matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `craft_matrixblocktypes_fieldId_fk` (`fieldId`),
  KEY `craft_matrixblocktypes_fieldLayoutId_fk` (`fieldLayoutId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `craft_matrixblocktypes`
--

INSERT INTO `craft_matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 46, 'Text', 'text', 1, '2014-12-30 21:19:48', '2015-07-12 02:37:17', 'f1581249-9e2f-45b1-9313-51ce122290c4'),
(2, 1, 47, 'Image - Full', 'imageFull', 2, '2014-12-30 21:19:48', '2015-07-12 02:37:17', '1cb1b610-5183-4174-8701-eada24853c63'),
(3, 1, 48, 'Image - Half', 'imageHalf', 3, '2015-05-05 18:46:57', '2015-07-12 02:37:17', '865f9f03-ba79-4552-a37a-1d5f393e8eae'),
(4, 1, 49, 'Image - Couple', 'imageCouple', 4, '2015-05-14 06:14:13', '2015-07-12 02:37:17', '90e52975-1fb8-455b-9b85-4c1ec2b61003');

-- --------------------------------------------------------

--
-- Table structure for table `craft_matrixcontent_body`
--

CREATE TABLE IF NOT EXISTS `craft_matrixcontent_body` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_text_text` text COLLATE utf8_unicode_ci,
  `field_imageCouple_caption` text COLLATE utf8_unicode_ci,
  `field_imageCouple_caption2` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixcontent_body_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `craft_matrixcontent_body_locale_fk` (`locale`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=89 ;

--
-- Dumping data for table `craft_matrixcontent_body`
--

INSERT INTO `craft_matrixcontent_body` (`id`, `elementId`, `locale`, `field_text_text`, `field_imageCouple_caption`, `field_imageCouple_caption2`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(15, 100, 'en_us', NULL, NULL, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:17', '195a22f5-fac5-4203-9d19-e16d4f12b995'),
(16, 101, 'en_us', NULL, NULL, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:17', '7124c44d-a329-40d3-a8bd-141271ca6091'),
(17, 102, 'en_us', NULL, NULL, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:18', 'b6b92dfc-564e-4dc5-b148-5217407c5b59'),
(18, 103, 'en_us', NULL, 'Every page of THE MANUAL WAS SCANNED FROM THE PRIVATE COLLECTION OF LELLA AND MASSIMO VIGNELLI, provided by their son, Luca Vignelli.', '', '2015-07-14 19:50:22', '2015-08-22 02:29:18', '16c8ecc8-ac75-452f-b255-b25ce77a6ef9'),
(19, 104, 'en_us', NULL, NULL, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:18', 'ed37336d-524e-466a-b9e1-0602c62a5fb2'),
(20, 105, 'en_us', NULL, NULL, NULL, '2015-07-14 19:50:22', '2015-08-22 02:29:18', '2137bcfa-37ee-4645-aa23-02b096a752ba'),
(21, 106, 'en_us', NULL, 'The pages were reproduced at 100% scale without any modifications.', 'A stochastic printing method was used in order to achieve the best reproduction from the original manual.', '2015-07-14 19:50:23', '2015-08-22 02:29:18', 'aa1d9efa-ab16-4099-a968-986f4f88d7f4'),
(22, 107, 'en_us', NULL, NULL, NULL, '2015-07-14 19:50:23', '2015-08-22 02:29:18', '6f2bdcfe-38ed-4dac-8f19-764469f01724'),
(24, 127, 'en_us', NULL, 'The identity uses a typeface called ''Lisbon'', inspired by the house tiles found within the city''s streets — designed by Colophon Foundry.', 'A custom ball terminal was designed for the ''G'' in the logotype.', '2015-07-14 21:21:25', '2015-08-22 02:08:24', 'd97139e3-b0d3-41e8-904f-6c2ff966dd67'),
(25, 128, 'en_us', NULL, NULL, NULL, '2015-07-14 21:21:25', '2015-08-22 02:08:24', '940b815c-1c6d-4828-a906-fb12bef70d4b'),
(28, 132, 'en_us', NULL, 'Menus and other collateral use ''Pitch'' as the supporting typeface.', '', '2015-07-14 21:33:27', '2015-08-22 02:08:24', '0d8adcb8-814d-44af-a381-f7e35bb5ae97'),
(29, 133, 'en_us', NULL, NULL, NULL, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '3989be15-4448-4294-a0db-d49b8d3dbf35'),
(30, 134, 'en_us', NULL, NULL, NULL, '2015-07-14 21:33:27', '2015-08-22 02:08:24', 'd2c141bb-5313-42b8-a9e0-77eecd867927'),
(31, 135, 'en_us', NULL, NULL, NULL, '2015-07-14 21:33:27', '2015-08-22 02:08:24', '0a5a7397-a3cb-4ba6-ae57-b19431557ece'),
(32, 155, 'en_us', NULL, 'Various COMBINATIONS ARE CREATED OUT OF THE GEOMETRIC LETTERFORMS TO accomplish UNIQUE BAG AND CATALOG COVERS.', '', '2015-07-14 22:01:42', '2015-08-22 02:16:03', '0283b55b-59e1-499e-8f50-169f3c40c3ac'),
(33, 156, 'en_us', NULL, NULL, NULL, '2015-07-14 22:01:42', '2015-08-22 02:16:03', '45f2266b-69ba-4b19-958e-9ec3a8a5f5ff'),
(34, 157, 'en_us', NULL, NULL, NULL, '2015-07-14 22:01:43', '2015-08-22 02:16:03', '7c3254ea-a21f-4f6a-8682-5ef5cfadc0b8'),
(36, 213, 'en_us', NULL, 'Every catalog cover featured a unique combination of the logotype and integration of photography.', '', '2015-07-21 02:48:57', '2015-08-22 02:16:04', '28a99a3f-4321-464a-94d9-8215694daf75'),
(37, 214, 'en_us', NULL, NULL, NULL, '2015-07-21 02:48:57', '2015-08-22 02:16:04', '7239a673-c51b-4182-8b12-5740102d6c3a'),
(38, 233, 'en_us', NULL, NULL, NULL, '2015-07-21 03:38:42', '2015-08-20 18:46:55', '54a2eae6-e58e-4ca8-b14f-b46d9c439bd9'),
(39, 234, 'en_us', NULL, 'The identity was inspired by woodblock prints contained within the exhibition. ', 'For the title wall, 10'' letters were hand-painted to recreated a woodblock print texture.', '2015-07-21 03:41:09', '2015-08-20 18:46:55', '598d9ab9-23e7-4bb4-92cb-e2cb958c2e4d'),
(40, 235, 'en_us', NULL, NULL, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '39365193-fd00-4b4f-9438-0f461ec67c76'),
(41, 236, 'en_us', NULL, 'As a way to promote the show, a free newspaper was designed to showcase various prints from the exhibition, as well as expand on the print making process.', 'In addition to German Expressionism, a second print-related exhibition was also included in the paper. ', '2015-07-21 03:46:28', '2015-08-20 18:46:55', '3b10b056-a0bc-474c-8903-277c1c8817da'),
(42, 237, 'en_us', NULL, NULL, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '8e791291-490d-46ee-8fe6-cf34afe8e2fb'),
(43, 238, 'en_us', NULL, NULL, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:55', '99b3eac9-e6f8-49a9-8220-501c6fcc56aa'),
(44, 239, 'en_us', NULL, NULL, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:55', 'd311c64e-7319-412e-8a04-9b0f867dbeb7'),
(45, 240, 'en_us', NULL, NULL, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:56', '840dea32-59c8-41ac-856c-dcb9b32276b9'),
(46, 241, 'en_us', NULL, NULL, NULL, '2015-07-21 03:46:28', '2015-08-20 18:46:56', 'b39af228-f9ce-472b-94d9-4a3ae48b625e'),
(47, 256, 'en_us', NULL, NULL, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:17', 'f62bde22-4034-4bbd-9355-343e1196f0ff'),
(48, 257, 'en_us', NULL, 'The booklets were distributed to clients, friends, and family as the 2013 holiday card from Pentagram.', '', '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'a89ed7ca-ce1f-49d2-805c-ba7302030a40'),
(49, 258, 'en_us', NULL, NULL, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', '171e5256-a099-4cd1-bbaf-441a4356cca0'),
(50, 259, 'en_us', NULL, NULL, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'd4eb445b-6e06-4f61-9eb2-8f8cdaced33f'),
(51, 260, 'en_us', NULL, NULL, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'b6b64560-602b-403e-aa1d-54ee74e4fe1d'),
(52, 261, 'en_us', NULL, NULL, NULL, '2015-07-23 23:17:51', '2015-08-20 18:47:18', 'dc7e5bff-1512-4fc5-9fd4-c4dbb5dbcf7c'),
(53, 273, 'en_us', NULL, NULL, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:43', '06e824f7-7ae2-4640-8d9e-1199a4f39905'),
(54, 274, 'en_us', NULL, 'The new identity has been used for the past three years of the competition.', '', '2015-07-23 23:56:41', '2015-08-22 02:10:44', '24fd38c6-7818-4e5d-b9c6-25bc26bc532f'),
(55, 275, 'en_us', NULL, NULL, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '1ac19833-c1a8-475a-990d-2045abe3bf40'),
(56, 276, 'en_us', NULL, NULL, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '390a2b12-619b-4bc0-a662-12d644af086f'),
(57, 277, 'en_us', NULL, 'A primary concept for the symbol was it''s ability to communicate dynamic information, as seen in this limited edition poster given away at the 2013 awards ceremony.', '', '2015-07-23 23:56:41', '2015-08-22 02:10:44', '8718d15a-4f89-47cb-ab1a-3b96c7f47021'),
(58, 278, 'en_us', NULL, NULL, NULL, '2015-07-23 23:56:41', '2015-08-22 02:10:44', '13200bae-d794-4f29-aa29-ee579c9d833e'),
(59, 283, 'en_us', NULL, NULL, NULL, '2015-08-16 21:06:05', '2015-08-22 02:00:59', '3024fd03-c19e-43a8-8d9e-534e9c458c5e'),
(60, 309, 'en_us', NULL, NULL, NULL, '2015-08-16 21:58:19', '2015-08-22 02:00:59', '5d83f811-60cb-4625-8611-3064d5c8bca7'),
(61, 310, 'en_us', NULL, NULL, NULL, '2015-08-16 21:58:19', '2015-08-22 02:00:59', '0f4096b2-9e9e-4d6d-8847-a2d5edbba384'),
(62, 314, 'en_us', NULL, 'A custom cut of Helvetica Neue (named after our client, the Department of Transportation / DOT) was developed specifically for this project — design by Hamish Smyth and Monotype.', '', '2015-08-18 14:06:35', '2015-08-22 02:00:59', 'd506bb9d-160c-4e36-99f7-3dea4b05ec2e'),
(63, 315, 'en_us', NULL, NULL, NULL, '2015-08-18 14:06:35', '2015-08-22 02:00:59', '65c9dcf0-2ae0-4e58-b2b6-7676bc03445a'),
(64, 316, 'en_us', NULL, 'Icons were later applied to redesigned parking signage for the city of New York, also designed by Pentagram.', '', '2015-08-18 14:06:35', '2015-08-22 02:00:59', 'b2392ff9-4096-4565-9031-f25bde5197e0'),
(65, 317, 'en_us', NULL, NULL, NULL, '2015-08-18 14:06:35', '2015-08-22 02:00:59', 'e695e26b-7829-40a0-9952-e26b1be670e9'),
(66, 322, 'en_us', NULL, NULL, NULL, '2015-08-19 02:47:43', '2015-08-22 02:00:59', '7d98ef1f-89ac-4d81-a834-8b8571accf14'),
(67, 323, 'en_us', NULL, NULL, NULL, '2015-08-19 02:47:43', '2015-08-22 02:00:59', 'a780a9db-28d5-48ba-bc9b-efe20691c457'),
(68, 324, 'en_us', NULL, NULL, NULL, '2015-08-19 02:47:44', '2015-08-22 02:00:59', 'bb3f87b8-e832-445b-b74d-3a62eef98edc'),
(69, 329, 'en_us', NULL, 'With reference to the AIGA symbol signs, every icon was redrawn to share relationships to Helvetica Neue DOT at 16 pt (as set on the maps).', 'This concept, along with a strict grid system, developed the family of icons.', '2015-08-19 03:53:43', '2015-08-22 02:00:59', 'cbb24013-ee19-494c-bdc0-5e277e7b969f'),
(70, 330, 'en_us', NULL, NULL, NULL, '2015-08-19 03:53:43', '2015-08-22 02:00:59', '759d2022-3b82-4eaa-af24-b71f50250351'),
(71, 344, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'c682c0df-11e2-439e-8a1c-0a0d5b5ece15'),
(72, 345, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '787a8795-7772-477d-85bf-6025bdfd3f4f'),
(73, 346, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '56d6e23b-d8fd-494d-b309-83f1cfc87cd6'),
(74, 347, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '14406402-caea-45da-8cd9-4455dbe6c128'),
(75, 348, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '4528e9b7-34b1-484c-9f93-77d978963451'),
(76, 349, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '43dae41e-5e51-4e4e-a206-379ea88ef85e'),
(77, 350, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'b8ff11ad-3cf2-41c1-b38b-cb65c1186a03'),
(78, 351, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', '5beb2eac-c652-4204-969a-7d1f2dabc4d9'),
(79, 352, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'c808d94c-1ac7-4ad0-8777-b3161e408847'),
(80, 353, 'en_us', NULL, NULL, NULL, '2015-08-20 19:46:04', '2015-08-22 04:48:01', 'c20844f4-7d88-497d-9012-4c9636947930'),
(81, 354, 'en_us', NULL, NULL, NULL, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'a3fb6170-ff56-44bb-b2fb-e45dd68ecba0'),
(82, 355, 'en_us', NULL, NULL, NULL, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '85a19e66-6563-4055-b70c-4b84a0d37244'),
(83, 356, 'en_us', NULL, NULL, NULL, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '335293f2-80d3-465a-beb1-da2f19e8faa8'),
(84, 357, 'en_us', NULL, NULL, NULL, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '9e5364c2-f87e-49aa-a350-741d35d16e3c'),
(85, 358, 'en_us', NULL, NULL, NULL, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '73d7886c-eda7-42fa-8336-cd8ebfa22645'),
(86, 359, 'en_us', NULL, NULL, NULL, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '40916577-494f-4354-af4f-db8a628d5aa0'),
(87, 360, 'en_us', NULL, NULL, NULL, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '861c9bba-81b4-4954-aeb6-eda9265ab930'),
(88, 361, 'en_us', NULL, NULL, NULL, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '2f73f073-5e5f-4e37-97b0-f56e7c7f9422');

-- --------------------------------------------------------

--
-- Table structure for table `craft_migrations`
--

CREATE TABLE IF NOT EXISTS `craft_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_migrations_version_unq_idx` (`version`),
  KEY `craft_migrations_pluginId_fk` (`pluginId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `craft_migrations`
--

INSERT INTO `craft_migrations` (`id`, `pluginId`, `version`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, NULL, 'm000000_000000_base', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '8a6812fe-4633-4dcb-a9fd-0977f67d4f6d'),
(2, NULL, 'm140730_000001_add_filename_and_format_to_transformindex', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '5f0d96ce-0e23-452d-932d-846535d1892c'),
(3, NULL, 'm140815_000001_add_format_to_transforms', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', 'f0a3a893-bf51-4cf3-abe7-4bebf9b074f4'),
(4, NULL, 'm140822_000001_allow_more_than_128_items_per_field', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', 'fe34e94d-341f-40fe-af6e-a5455183d688'),
(5, NULL, 'm140829_000001_single_title_formats', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', 'd32ceb19-350c-4af7-bb97-eb3bb303c36a'),
(6, NULL, 'm140831_000001_extended_cache_keys', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', 'c119c2e5-dac7-4a4b-876c-d31628656f3b'),
(7, NULL, 'm140922_000001_delete_orphaned_matrix_blocks', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '3b0a1e10-c217-4740-990c-ea9434bc2570'),
(8, NULL, 'm141008_000001_elements_index_tune', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '5037c726-0c02-48ca-9676-db04bd2b5161'),
(9, NULL, 'm141009_000001_assets_source_handle', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '701d28be-9e5d-44b7-922c-8b504ab76b75'),
(10, NULL, 'm141024_000001_field_layout_tabs', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '4f53539c-9387-44b9-bd67-977cf5b5b36a'),
(11, NULL, 'm141030_000001_drop_structure_move_permission', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '57ecefbc-075c-4fe8-bcd8-5a18e8c8d692'),
(12, NULL, 'm141103_000001_tag_titles', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '3b709b19-87fe-4a2e-a947-1568bc69d818'),
(13, NULL, 'm141109_000001_user_status_shuffle', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '7a584871-5dd5-4501-86e8-522599b56ef1'),
(14, NULL, 'm141126_000001_user_week_start_day', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '2014-12-30 09:30:25', '539686c6-7dfd-4750-8e28-6a8daae05ff8'),
(15, NULL, 'm150210_000001_adjust_user_photo_size', '2015-05-05 18:30:43', '2015-05-05 18:30:43', '2015-05-05 18:30:43', '7aafc59d-cc43-4c10-9ff1-486dbdf64710');

-- --------------------------------------------------------

--
-- Table structure for table `craft_plugins`
--

CREATE TABLE IF NOT EXISTS `craft_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `version` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `settings` text COLLATE utf8_unicode_ci,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_rackspaceaccess`
--

CREATE TABLE IF NOT EXISTS `craft_rackspaceaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connectionKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdnUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_rackspaceaccess_connectionKey_unq_idx` (`connectionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_relations`
--

CREATE TABLE IF NOT EXISTS `craft_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_relations_fieldId_sourceId_sourceLocale_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceLocale`,`targetId`),
  KEY `craft_relations_sourceId_fk` (`sourceId`),
  KEY `craft_relations_sourceLocale_fk` (`sourceLocale`),
  KEY `craft_relations_targetId_fk` (`targetId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=946 ;

--
-- Dumping data for table `craft_relations`
--

INSERT INTO `craft_relations` (`id`, `fieldId`, `sourceId`, `sourceLocale`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(702, 17, 215, NULL, 220, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '911bbb36-5117-4730-aac5-e6bc6aef6616'),
(703, 17, 215, NULL, 221, 2, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '42bc6261-3b3a-4f47-aea8-31d997771449'),
(704, 17, 215, NULL, 232, 3, '2015-08-20 18:46:55', '2015-08-20 18:46:55', 'f269129d-063c-456a-8efe-58849dafbf3c'),
(705, 17, 215, NULL, 224, 4, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '40256170-8914-4839-b7b9-f4d33650a159'),
(706, 9, 215, NULL, 216, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '8ac663c7-068c-49fe-99c1-e9066bd4cda5'),
(707, 7, 233, NULL, 220, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', 'b2dbb2dd-2a72-405e-968a-2775360fef27'),
(708, 13, 234, NULL, 218, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '9bd06417-52b1-42b9-a5f9-0fb8d925d860'),
(709, 15, 234, NULL, 219, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '12fde69a-785c-45f9-ba8c-f2a8b276e086'),
(710, 7, 235, NULL, 217, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', 'dc4f84bf-1f46-4417-8cac-d755e20b0cb0'),
(711, 13, 236, NULL, 222, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '35e6ec2c-71db-4ff0-a553-ea74ee36249f'),
(712, 15, 236, NULL, 223, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '5b574631-8eb7-4efb-b118-df74fe04a169'),
(713, 8, 237, NULL, 224, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '39b0fb81-12c5-4455-9ba4-03accbb09cd4'),
(714, 12, 237, NULL, 225, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '48d4db89-c9e8-4092-a1b9-f23c8a356ccf'),
(715, 7, 238, NULL, 229, 1, '2015-08-20 18:46:55', '2015-08-20 18:46:55', '465e36b8-afe3-48d7-ac11-726e8989a8dc'),
(716, 8, 239, NULL, 226, 1, '2015-08-20 18:46:56', '2015-08-20 18:46:56', 'ae2496bd-bc69-4a90-b677-49add9eb4012'),
(717, 12, 239, NULL, 227, 1, '2015-08-20 18:46:56', '2015-08-20 18:46:56', '06f0eaaf-b835-4957-bfbe-3b66a30df8a0'),
(718, 8, 240, NULL, 228, 1, '2015-08-20 18:46:56', '2015-08-20 18:46:56', '268d4af9-a523-422c-b3fe-e87fb007776b'),
(719, 12, 240, NULL, 232, 1, '2015-08-20 18:46:56', '2015-08-20 18:46:56', '83068360-8876-4735-ade9-cb73b4a9665c'),
(720, 7, 241, NULL, 231, 1, '2015-08-20 18:46:56', '2015-08-20 18:46:56', 'ad76027f-67cc-430e-a6cd-c29273a71d8c'),
(733, 17, 255, NULL, 254, 1, '2015-08-20 18:47:17', '2015-08-20 18:47:17', 'e8a708ce-f6b9-46e6-9174-5dc0f72e50ba'),
(734, 17, 255, NULL, 251, 2, '2015-08-20 18:47:17', '2015-08-20 18:47:17', '2ea2d9ea-6d7e-4648-9552-e502c719d55c'),
(735, 17, 255, NULL, 244, 3, '2015-08-20 18:47:17', '2015-08-20 18:47:17', 'd0c215c7-77f3-494d-a5e8-ce1d4e5c67d2'),
(736, 17, 255, NULL, 245, 4, '2015-08-20 18:47:17', '2015-08-20 18:47:17', '559ac0aa-0714-4bba-afa3-bbb6b9374371'),
(737, 9, 255, NULL, 242, 1, '2015-08-20 18:47:17', '2015-08-20 18:47:17', '04a44d55-ff4f-46c9-a3d3-7f1537a9b3e4'),
(738, 7, 256, NULL, 243, 1, '2015-08-20 18:47:17', '2015-08-20 18:47:17', '69f91011-6dfb-4f60-b31b-0dba2d92cb66'),
(739, 13, 257, NULL, 244, 1, '2015-08-20 18:47:18', '2015-08-20 18:47:18', '7b3ad57f-5c19-4be4-86dd-2eac82e6093c'),
(740, 15, 257, NULL, 245, 1, '2015-08-20 18:47:18', '2015-08-20 18:47:18', '0513516d-2a69-40d0-9ebb-31ce2658d98b'),
(741, 8, 258, NULL, 246, 1, '2015-08-20 18:47:18', '2015-08-20 18:47:18', '8ad0a2f8-5426-47ac-bca5-3656d50c12d2'),
(742, 12, 258, NULL, 247, 1, '2015-08-20 18:47:18', '2015-08-20 18:47:18', '9aae018a-ffa3-447f-bf54-1c62ea2d1c05'),
(743, 7, 259, NULL, 250, 1, '2015-08-20 18:47:18', '2015-08-20 18:47:18', '871c86fc-397c-4374-8088-b9edaf528792'),
(744, 7, 260, NULL, 253, 1, '2015-08-20 18:47:18', '2015-08-20 18:47:18', '48ad0429-c5df-4713-8250-a59892a35a7d'),
(745, 7, 261, NULL, 252, 1, '2015-08-20 18:47:18', '2015-08-20 18:47:18', '532e2fa4-44d4-446a-9503-dea7180a78f1'),
(843, 17, 280, NULL, 318, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '7ad05c0d-a6c9-4539-b334-5af023d2c731'),
(844, 17, 280, NULL, 293, 2, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '2dacb51f-2bf3-4540-9503-dc313d5f8e55'),
(845, 17, 280, NULL, 281, 3, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '20382c3b-226a-48c0-a750-7bd44dd67406'),
(846, 17, 280, NULL, 302, 4, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'ef740818-ed43-4278-812f-7ae66ac7d927'),
(847, 9, 280, NULL, 321, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '8ba2b8ce-963d-4707-8c29-f936755f4ade'),
(848, 13, 314, NULL, 327, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '12ca0dcd-a641-4653-8023-d6ff6483f17b'),
(849, 15, 314, NULL, 328, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'dacd3e17-3cfc-4ce8-9e83-cce868c06f94'),
(850, 13, 329, NULL, 325, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '92de6c13-97ca-4f79-b9de-5c61cbc96c8d'),
(851, 15, 329, NULL, 326, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '7576118a-ae19-4dce-bbc7-83ecae54ec92'),
(852, 7, 324, NULL, 320, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'bf27677f-a818-46a5-a633-5da02ec87aed'),
(853, 8, 283, NULL, 305, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '8000da6d-673d-4987-95e3-4d45a7e88a3e'),
(854, 12, 283, NULL, 304, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'fc507b34-f01b-41f2-b536-0d6ad606457b'),
(855, 8, 309, NULL, 308, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '29d49ba8-7f0d-40b5-a26b-eafd56286a42'),
(856, 12, 309, NULL, 288, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'beb50770-befe-417b-90c6-2fa664d37462'),
(857, 7, 310, NULL, 293, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '209585c1-498c-4ed1-810c-e73c931a9a5a'),
(858, 7, 322, NULL, 318, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '08e859fe-fcb9-4354-9e8c-10ad72cd7ada'),
(859, 8, 315, NULL, 307, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'e65d098c-163e-41d8-a03b-dd70ec58a3dc'),
(860, 12, 315, NULL, 302, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '0b814e0c-19fe-4ad3-888a-8044a070bed7'),
(861, 8, 323, NULL, 291, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'f4a4b8b2-ee6a-4d3a-aac1-8184b0faf0c7'),
(862, 12, 323, NULL, 292, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '4460339d-6dad-4dbc-a8c6-f278b42e7164'),
(863, 13, 316, NULL, 281, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '44a55187-9326-49c7-a96c-164d768d5d21'),
(864, 15, 316, NULL, 289, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '081643f1-1086-44ce-80e2-1451a91e2c55'),
(865, 7, 317, NULL, 306, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '836dfb45-af64-4fc4-8d81-a9daeee39a56'),
(866, 7, 354, NULL, 282, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'd417a061-3050-485c-b962-311c3afcc713'),
(867, 8, 330, NULL, 296, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '6246925c-fd03-49b4-9c14-4b31f24c3ea1'),
(868, 12, 330, NULL, 287, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '091e811f-6e70-4dad-af36-f8b5846639d7'),
(869, 8, 355, NULL, 290, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'd0a9cae2-e24b-41a3-9b60-14327d2d35f0'),
(870, 12, 355, NULL, 301, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', 'a8682443-24d3-49d2-9509-ac5e91d48e11'),
(871, 7, 356, NULL, 319, 1, '2015-08-22 02:00:59', '2015-08-22 02:00:59', '646747c8-4aa9-4207-bbbe-7bb5646a07dc'),
(872, 17, 13, NULL, 200, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '023b8d67-89a5-462a-86e6-bab83b4d8111'),
(873, 17, 13, NULL, 185, 2, '2015-08-22 02:08:24', '2015-08-22 02:08:24', 'd66a27a6-b7e4-4419-b74d-f8cb1e1a954c'),
(874, 17, 13, NULL, 195, 3, '2015-08-22 02:08:24', '2015-08-22 02:08:24', 'e705bd3a-530a-4359-b731-9e3e458114bf'),
(875, 17, 13, NULL, 191, 4, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '189d4fef-b87c-44cf-b76c-e2a6d9b35ffa'),
(876, 9, 13, NULL, 202, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '50c026cd-7760-4199-8295-0246ae534687'),
(877, 13, 127, NULL, 189, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '93eee70c-f33b-4bb0-bfce-751f014dab1e'),
(878, 15, 127, NULL, 188, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '440b828a-2d52-4023-9fad-a05683c68643'),
(879, 7, 128, NULL, 200, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '9cf28c3c-478c-498b-8c65-e7d2fb80a4c4'),
(880, 13, 132, NULL, 194, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '16b4e925-5c33-48c8-92ee-3256b146b757'),
(881, 15, 132, NULL, 196, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '122288e1-e164-42b5-9fbe-88aeab04727d'),
(882, 7, 357, NULL, 198, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', 'a0bb4e8f-c3e2-4182-a047-9b58526a54e0'),
(883, 7, 358, NULL, 193, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', 'a3e02182-d514-4544-90b4-82f6983e3980'),
(884, 7, 133, NULL, 186, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '8b407bbb-d643-475c-bd09-5209cd693035'),
(885, 8, 134, NULL, 191, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '407b7521-b545-4f1a-84dd-2d7299647f45'),
(886, 12, 134, NULL, 201, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', 'a892151a-2e56-491d-9af4-91e768bba0c6'),
(887, 7, 135, NULL, 187, 1, '2015-08-22 02:08:24', '2015-08-22 02:08:24', '2b1dda5f-c3a8-403e-a237-fcfca1ac1fc7'),
(888, 17, 266, NULL, 263, 1, '2015-08-22 02:10:43', '2015-08-22 02:10:43', 'e81a6274-c459-4e9a-9278-a8491ce69f32'),
(889, 17, 266, NULL, 264, 2, '2015-08-22 02:10:43', '2015-08-22 02:10:43', '8fde6000-5084-4fde-ab9a-bc9443e37fe0'),
(890, 17, 266, NULL, 265, 3, '2015-08-22 02:10:43', '2015-08-22 02:10:43', 'ae98d13d-a481-4f2a-bc7e-dcaecce0b7ee'),
(891, 9, 266, NULL, 262, 1, '2015-08-22 02:10:43', '2015-08-22 02:10:43', '05674d2b-3a05-49c9-b4f2-9a7486d738b1'),
(892, 7, 273, NULL, 264, 1, '2015-08-22 02:10:43', '2015-08-22 02:10:43', 'b1d89823-987b-42d0-bcdb-37a8f31b450a'),
(893, 13, 274, NULL, 269, 1, '2015-08-22 02:10:44', '2015-08-22 02:10:44', '1db8af40-31b9-453a-a139-3bdc297a001f'),
(894, 15, 274, NULL, 270, 1, '2015-08-22 02:10:44', '2015-08-22 02:10:44', '8c8aa904-73c0-422d-8ffb-d0447e0c9aa0'),
(895, 7, 275, NULL, 263, 1, '2015-08-22 02:10:44', '2015-08-22 02:10:44', 'f4f4594d-b7ef-4b01-8c2d-2678cae31441'),
(896, 8, 276, NULL, 268, 1, '2015-08-22 02:10:44', '2015-08-22 02:10:44', '2861d6df-b956-4885-a945-aab7546dbbd7'),
(897, 12, 276, NULL, 271, 1, '2015-08-22 02:10:44', '2015-08-22 02:10:44', 'e2066cc9-98bc-490b-ad3b-c89c8f8a3295'),
(898, 13, 277, NULL, 267, 1, '2015-08-22 02:10:44', '2015-08-22 02:10:44', '74df944d-abbd-4e5a-89f8-4e0985e60601'),
(899, 15, 277, NULL, 265, 1, '2015-08-22 02:10:44', '2015-08-22 02:10:44', '75b6c99f-8075-4c4d-82b4-0b0280df54b8'),
(900, 7, 278, NULL, 272, 1, '2015-08-22 02:10:44', '2015-08-22 02:10:44', '05d0c8ec-85ac-469b-a685-95c627e24453'),
(901, 17, 15, NULL, 203, 1, '2015-08-22 02:16:03', '2015-08-22 02:16:03', 'db34f87a-533f-41a5-9249-e4565e736d96'),
(902, 17, 15, NULL, 209, 2, '2015-08-22 02:16:03', '2015-08-22 02:16:03', '1fbe8831-1485-47e5-88d7-f8706653a329'),
(903, 17, 15, NULL, 212, 3, '2015-08-22 02:16:03', '2015-08-22 02:16:03', '1107c8cd-9109-4e60-ba56-d63797951516'),
(904, 9, 15, NULL, 206, 1, '2015-08-22 02:16:03', '2015-08-22 02:16:03', 'a3618d59-b032-4c71-b51f-93069a416d40'),
(905, 13, 155, NULL, 212, 1, '2015-08-22 02:16:03', '2015-08-22 02:16:03', 'c796073f-2bea-4ce0-a355-a4ebb3bdb3a5'),
(906, 15, 155, NULL, 207, 1, '2015-08-22 02:16:03', '2015-08-22 02:16:03', '64f87d34-797b-4c95-a301-1566b3902e5a'),
(907, 7, 156, NULL, 210, 1, '2015-08-22 02:16:03', '2015-08-22 02:16:03', '13feb035-896e-4b9e-9a6b-013fbfaa8b89'),
(908, 8, 157, NULL, 203, 1, '2015-08-22 02:16:03', '2015-08-22 02:16:03', '60ce59cc-cc6a-4825-bf19-c502611567f3'),
(909, 12, 157, NULL, 205, 1, '2015-08-22 02:16:03', '2015-08-22 02:16:03', '41e16260-6d4d-42af-8299-0b3831f18b81'),
(910, 13, 213, NULL, 208, 1, '2015-08-22 02:16:04', '2015-08-22 02:16:04', '62985784-75de-48f3-b975-1035ea12919e'),
(911, 15, 213, NULL, 211, 1, '2015-08-22 02:16:04', '2015-08-22 02:16:04', '04813e0c-c63b-4e43-8137-e8bcc26d596e'),
(912, 7, 214, NULL, 204, 1, '2015-08-22 02:16:04', '2015-08-22 02:16:04', 'dcf572e8-8a5b-4cc4-acc4-2f5502fafafb'),
(913, 17, 73, NULL, 178, 1, '2015-08-22 02:29:17', '2015-08-22 02:29:17', 'a5fe1374-b34f-4ebc-b075-0eed47b936c4'),
(914, 17, 73, NULL, 163, 2, '2015-08-22 02:29:17', '2015-08-22 02:29:17', '2770e52a-91b0-4e6f-bf5a-a85cdb2dc0e5'),
(915, 17, 73, NULL, 181, 3, '2015-08-22 02:29:17', '2015-08-22 02:29:17', 'dd0d5a9d-24cb-4841-a7ec-93a4a5ff4cc3'),
(916, 17, 73, NULL, 175, 4, '2015-08-22 02:29:17', '2015-08-22 02:29:17', 'b01265e8-2e6b-4c1e-aee6-d44dd554a238'),
(917, 9, 73, NULL, 177, 1, '2015-08-22 02:29:17', '2015-08-22 02:29:17', 'e0bc4506-b0fc-405e-a841-ef79c672da50'),
(918, 7, 100, NULL, 184, 1, '2015-08-22 02:29:17', '2015-08-22 02:29:17', '0527ead7-dcbe-4e7e-af65-a91f60dde2a3'),
(919, 8, 101, NULL, 168, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '7a775ecb-1416-4378-90ef-17b77eef0304'),
(920, 12, 101, NULL, 169, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '1ed89afd-8bf1-41a0-a70d-c7b6c2a75269'),
(921, 7, 102, NULL, 179, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', 'b1323e2c-caf5-45a7-8c04-31f4d9c52aae'),
(922, 13, 103, NULL, 164, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '08ddc314-064c-48ae-8ce9-9f9fd0b2338d'),
(923, 15, 103, NULL, 159, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', 'd08055b9-c722-40dc-b740-1bd6830d9652'),
(924, 7, 104, NULL, 174, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '99f6c6fb-82db-4cb5-8f6e-e00c9906a81a'),
(925, 8, 105, NULL, 175, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '78e1dc5c-ff2a-43f5-8012-771c76060f23'),
(926, 12, 105, NULL, 176, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '72d6e078-4d09-4622-aba7-96b8e61b8fd8'),
(927, 13, 106, NULL, 182, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', 'd79fdc9d-e141-4ec6-9645-4eff684505a6'),
(928, 15, 106, NULL, 162, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '3aa66ef6-ac2b-4e79-ba44-4b1ff8a3ded7'),
(929, 7, 107, NULL, 183, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '8d5964c3-7dea-4e59-873f-b984e7205a42'),
(930, 8, 359, NULL, 165, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', 'f815825b-35be-4b16-b33f-1f7de0ad41b8'),
(931, 12, 359, NULL, 166, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', 'bdfa8cee-2f9a-4185-b4fc-07e370cb2a14'),
(932, 7, 360, NULL, 181, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', '4dd48e6c-2cb0-48c6-9804-de7321e95f0c'),
(933, 7, 361, NULL, 170, 1, '2015-08-22 02:29:18', '2015-08-22 02:29:18', 'f52c0086-f487-421e-87ba-2d7957607399'),
(934, 17, 343, NULL, 332, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '7a32a084-e903-4b0c-9c75-3488ab6e4242'),
(935, 9, 343, NULL, 331, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '4da9bea1-7d3d-4400-a8ef-917e12c41764'),
(936, 7, 344, NULL, 333, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '3367f454-7ffa-43e4-8c6e-54b9618e922c'),
(937, 7, 346, NULL, 335, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '44285cb7-6dbc-44e7-8345-fcff29fd2e48'),
(938, 7, 347, NULL, 337, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '2228423a-e98a-4141-97fb-2f030eba1b17'),
(939, 7, 348, NULL, 338, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', 'cbf7fe4b-fdeb-477f-978d-2ef66467a958'),
(940, 7, 349, NULL, 334, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', 'ec312453-bdf7-4ae9-b186-02b7d902828a'),
(941, 7, 345, NULL, 336, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '76effc51-3226-4c01-ab00-82906eb7575f'),
(942, 7, 350, NULL, 339, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '7aee669d-a84c-4806-b739-2617837b0d5b'),
(943, 7, 351, NULL, 340, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '8b009e0d-3198-42ff-96fd-caa667a76643'),
(944, 7, 352, NULL, 342, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '0f0bd554-394d-4071-9e19-88155c8d479a'),
(945, 7, 353, NULL, 341, 1, '2015-08-22 04:48:01', '2015-08-22 04:48:01', '04263879-ce59-4643-a999-bc87d0138777');

-- --------------------------------------------------------

--
-- Table structure for table `craft_routes`
--

CREATE TABLE IF NOT EXISTS `craft_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlParts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_routes_urlPattern_unq_idx` (`urlPattern`),
  KEY `craft_routes_locale_idx` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_searchindex`
--

CREATE TABLE IF NOT EXISTS `craft_searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`locale`),
  FULLTEXT KEY `craft_searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `craft_searchindex`
--

INSERT INTO `craft_searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`) VALUES
(1, 'username', 0, 'en_us', ' admin '),
(1, 'firstname', 0, 'en_us', ' jeremy '),
(1, 'lastname', 0, 'en_us', ' beasley '),
(1, 'fullname', 0, 'en_us', ' jeremy beasley '),
(1, 'email', 0, 'en_us', ' jeremy bsley com '),
(1, 'slug', 0, 'en_us', ''),
(13, 'slug', 0, 'en_us', ' gato '),
(13, 'title', 0, 'en_us', ' gato '),
(180, 'title', 0, 'en_us', ' nycta graphics standards manual detail 10 web '),
(180, 'slug', 0, 'en_us', ''),
(15, 'slug', 0, 'en_us', ' saks fifth avenue '),
(15, 'title', 0, 'en_us', ' saks fifth avenue '),
(222, 'extension', 0, 'en_us', ' jpg '),
(222, 'filename', 0, 'en_us', ' ms_south africa_german express_5481_web jpg '),
(221, 'title', 0, 'en_us', ' ms ge 36 web '),
(15, 'field', 1, 'en_us', ' various combinations are created out of the geometric letterforms to accomplish unique bag and catalog covers saks bag street 4 trix web saks look mark sfa look cataglogue new1 trix web saks bag street 5 trix web saks bag street 9 trix web every catalog cover featured a unique combination of the logotype and integration of photography sfa look cataglogue aug1 trix web sfa look cataglogue new2 trix web saks bag street 8 trix web '),
(15, 'field', 3, 'en_us', ' identity for the 2013 national campaign exploring the theme of looking a bold and geometric approach allowed for endless variations applied to bags catalogs and in store displays '),
(208, 'slug', 0, 'en_us', ''),
(208, 'title', 0, 'en_us', ' sfa look cataglogue aug1 trix web '),
(15, 'field', 9, 'en_us', ' saks bag street 16 trix web '),
(221, 'kind', 0, 'en_us', ' image '),
(221, 'slug', 0, 'en_us', ''),
(221, 'extension', 0, 'en_us', ' jpg '),
(221, 'filename', 0, 'en_us', ' ms_ge_36_web jpg '),
(182, 'extension', 0, 'en_us', ' jpg '),
(181, 'extension', 0, 'en_us', ' jpg '),
(220, 'slug', 0, 'en_us', ''),
(220, 'title', 0, 'en_us', ' ms ge 30 web '),
(220, 'kind', 0, 'en_us', ' image '),
(220, 'extension', 0, 'en_us', ' jpg '),
(13, 'field', 1, 'en_us', ' the identity uses a typeface called lisbon inspired by the house tiles found within the city s streets designed by colophon foundry a custom ball terminal was designed for the g in the logotype gato web 5 gato web 4 gato web 16 menus and other collateral use pitch as the supporting typeface gato web 10 gato web 12 gato web 14 gato web 9 gato web 2 gato web 7 gato web 17rev gato web 3 '),
(13, 'field', 3, 'en_us', ' identity for bobby flay s restaurant gato located in the noho neighborhood of manhattan gato combines a mix of mediterranean inspired dishes with the industrial environment of downtown nyc '),
(13, 'field', 9, 'en_us', ' gato web 18 '),
(15, 'field', 10, 'en_us', ' project 2013 campaign client saks fifth avenue cr dir michael beirut firm pentagram '),
(15, 'field', 11, 'en_us', ' pentagram blog http http new pentagram com 2013 09 new work saks fifth avenue look campaign '),
(160, 'kind', 0, 'en_us', ' image '),
(160, 'extension', 0, 'en_us', ' jpg '),
(160, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_2_web jpg '),
(220, 'filename', 0, 'en_us', ' ms_ge_30_web jpg '),
(219, 'title', 0, 'en_us', ' ms ge 26 web '),
(219, 'slug', 0, 'en_us', ''),
(219, 'kind', 0, 'en_us', ' image '),
(13, 'field', 10, 'en_us', ' project gato client bobby flay cr dir michael beirut firm pentagram photos martin seck '),
(13, 'field', 11, 'en_us', ' gatonyc com http gatonyc com pentagram com http http new pentagram com 2014 04 new work gato '),
(164, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_13_web jpg '),
(163, 'title', 0, 'en_us', ' nycta graphics standards manual 8 web '),
(163, 'slug', 0, 'en_us', ''),
(163, 'kind', 0, 'en_us', ' image '),
(163, 'extension', 0, 'en_us', ' jpg '),
(163, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_8_web jpg '),
(162, 'title', 0, 'en_us', ' nycta graphics standards manual 7 web '),
(208, 'extension', 0, 'en_us', ' jpg '),
(207, 'title', 0, 'en_us', ' saks look mark '),
(208, 'filename', 0, 'en_us', ' sfa_look_cataglogue_aug1_trix_web jpg '),
(207, 'slug', 0, 'en_us', ''),
(162, 'kind', 0, 'en_us', ' image '),
(162, 'slug', 0, 'en_us', ''),
(162, 'extension', 0, 'en_us', ' jpg '),
(162, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_7_web jpg '),
(161, 'title', 0, 'en_us', ' nycta graphics standards manual 4 web '),
(161, 'kind', 0, 'en_us', ' image '),
(161, 'slug', 0, 'en_us', ''),
(161, 'extension', 0, 'en_us', ' jpg '),
(161, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_4_web jpg '),
(160, 'title', 0, 'en_us', ' nycta graphics standards manual 2 web '),
(160, 'slug', 0, 'en_us', ''),
(212, 'kind', 0, 'en_us', ' image '),
(212, 'slug', 0, 'en_us', ''),
(207, 'kind', 0, 'en_us', ' image '),
(206, 'extension', 0, 'en_us', ' jpg '),
(206, 'kind', 0, 'en_us', ' image '),
(206, 'slug', 0, 'en_us', ''),
(206, 'title', 0, 'en_us', ' saks bag street 16 trix web '),
(207, 'filename', 0, 'en_us', ' saks_look_mark jpg '),
(207, 'extension', 0, 'en_us', ' jpg '),
(195, 'kind', 0, 'en_us', ' image '),
(195, 'slug', 0, 'en_us', ''),
(195, 'title', 0, 'en_us', ' gato web 11 '),
(196, 'filename', 0, 'en_us', ' gato_web_12 jpg '),
(196, 'extension', 0, 'en_us', ' jpg '),
(196, 'kind', 0, 'en_us', ' image '),
(196, 'slug', 0, 'en_us', ''),
(196, 'title', 0, 'en_us', ' gato web 12 '),
(197, 'filename', 0, 'en_us', ' gato_web_13 jpg '),
(197, 'extension', 0, 'en_us', ' jpg '),
(197, 'kind', 0, 'en_us', ' image '),
(197, 'slug', 0, 'en_us', ''),
(197, 'title', 0, 'en_us', ' gato web 13 '),
(198, 'filename', 0, 'en_us', ' gato_web_14 jpg '),
(198, 'extension', 0, 'en_us', ' jpg '),
(198, 'kind', 0, 'en_us', ' image '),
(198, 'slug', 0, 'en_us', ''),
(198, 'title', 0, 'en_us', ' gato web 14 '),
(199, 'filename', 0, 'en_us', ' gato_web_15 jpg '),
(199, 'extension', 0, 'en_us', ' jpg '),
(199, 'kind', 0, 'en_us', ' image '),
(199, 'slug', 0, 'en_us', ''),
(199, 'title', 0, 'en_us', ' gato web 15 '),
(200, 'filename', 0, 'en_us', ' gato_web_16 jpg '),
(200, 'extension', 0, 'en_us', ' jpg '),
(200, 'kind', 0, 'en_us', ' image '),
(200, 'slug', 0, 'en_us', ''),
(200, 'title', 0, 'en_us', ' gato web 16 '),
(201, 'filename', 0, 'en_us', ' gato_web_17rev jpg '),
(201, 'extension', 0, 'en_us', ' jpg '),
(201, 'kind', 0, 'en_us', ' image '),
(201, 'slug', 0, 'en_us', ''),
(201, 'title', 0, 'en_us', ' gato web 17rev '),
(202, 'filename', 0, 'en_us', ' gato_web_18 jpg '),
(202, 'extension', 0, 'en_us', ' jpg '),
(202, 'kind', 0, 'en_us', ' image '),
(202, 'slug', 0, 'en_us', ''),
(202, 'title', 0, 'en_us', ' gato web 18 '),
(185, 'extension', 0, 'en_us', ' jpg '),
(185, 'kind', 0, 'en_us', ' image '),
(185, 'slug', 0, 'en_us', ''),
(185, 'title', 0, 'en_us', ' gato web 1 '),
(186, 'filename', 0, 'en_us', ' gato_web_2 jpg '),
(186, 'extension', 0, 'en_us', ' jpg '),
(186, 'kind', 0, 'en_us', ' image '),
(186, 'slug', 0, 'en_us', ''),
(186, 'title', 0, 'en_us', ' gato web 2 '),
(187, 'filename', 0, 'en_us', ' gato_web_3 jpg '),
(187, 'extension', 0, 'en_us', ' jpg '),
(187, 'kind', 0, 'en_us', ' image '),
(187, 'slug', 0, 'en_us', ''),
(187, 'title', 0, 'en_us', ' gato web 3 '),
(188, 'filename', 0, 'en_us', ' gato_web_4 jpg '),
(188, 'extension', 0, 'en_us', ' jpg '),
(188, 'kind', 0, 'en_us', ' image '),
(188, 'slug', 0, 'en_us', ''),
(188, 'title', 0, 'en_us', ' gato web 4 '),
(189, 'filename', 0, 'en_us', ' gato_web_5 jpg '),
(189, 'extension', 0, 'en_us', ' jpg '),
(189, 'kind', 0, 'en_us', ' image '),
(189, 'slug', 0, 'en_us', ''),
(189, 'title', 0, 'en_us', ' gato web 5 '),
(190, 'filename', 0, 'en_us', ' gato_web_6 jpg '),
(190, 'extension', 0, 'en_us', ' jpg '),
(190, 'kind', 0, 'en_us', ' image '),
(190, 'slug', 0, 'en_us', ''),
(190, 'title', 0, 'en_us', ' gato web 6 '),
(191, 'filename', 0, 'en_us', ' gato_web_7 jpg '),
(191, 'extension', 0, 'en_us', ' jpg '),
(191, 'kind', 0, 'en_us', ' image '),
(191, 'slug', 0, 'en_us', ''),
(191, 'title', 0, 'en_us', ' gato web 7 '),
(192, 'filename', 0, 'en_us', ' gato_web_8 jpg '),
(192, 'extension', 0, 'en_us', ' jpg '),
(192, 'kind', 0, 'en_us', ' image '),
(192, 'slug', 0, 'en_us', ''),
(192, 'title', 0, 'en_us', ' gato web 8 '),
(193, 'filename', 0, 'en_us', ' gato_web_9 jpg '),
(193, 'extension', 0, 'en_us', ' jpg '),
(193, 'kind', 0, 'en_us', ' image '),
(193, 'slug', 0, 'en_us', ''),
(193, 'title', 0, 'en_us', ' gato web 9 '),
(194, 'filename', 0, 'en_us', ' gato_web_10 jpg '),
(194, 'extension', 0, 'en_us', ' jpg '),
(194, 'kind', 0, 'en_us', ' image '),
(194, 'slug', 0, 'en_us', ''),
(194, 'title', 0, 'en_us', ' gato web 10 '),
(195, 'filename', 0, 'en_us', ' gato_web_11 jpg '),
(195, 'extension', 0, 'en_us', ' jpg '),
(219, 'extension', 0, 'en_us', ' jpg '),
(219, 'filename', 0, 'en_us', ' ms_ge_26_web jpg '),
(218, 'title', 0, 'en_us', ' ms ge 24 web '),
(56, 'filename', 0, 'en_us', ' ms_ge_30 copy_web_960 jpg '),
(56, 'extension', 0, 'en_us', ' jpg '),
(56, 'kind', 0, 'en_us', ' image '),
(56, 'slug', 0, 'en_us', ''),
(56, 'title', 0, 'en_us', ' ms ge 30 copy web 960 '),
(182, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_detail_17_web jpg '),
(58, 'filename', 0, 'en_us', ' ms_ge_24 copy_web_960 jpg '),
(58, 'extension', 0, 'en_us', ' jpg '),
(58, 'kind', 0, 'en_us', ' image '),
(58, 'slug', 0, 'en_us', ''),
(58, 'title', 0, 'en_us', ' ms ge 24 copy web 960 '),
(59, 'filename', 0, 'en_us', ' ms_ge_17 copy_web_960 jpg '),
(59, 'extension', 0, 'en_us', ' jpg '),
(59, 'kind', 0, 'en_us', ' image '),
(59, 'slug', 0, 'en_us', ''),
(59, 'title', 0, 'en_us', ' ms ge 17 copy web 960 '),
(181, 'kind', 0, 'en_us', ' image '),
(181, 'slug', 0, 'en_us', ''),
(181, 'title', 0, 'en_us', ' nycta graphics standards manual detail 16 web '),
(218, 'kind', 0, 'en_us', ' image '),
(218, 'slug', 0, 'en_us', ''),
(218, 'extension', 0, 'en_us', ' jpg '),
(218, 'filename', 0, 'en_us', ' ms_ge_24_web jpg '),
(217, 'slug', 0, 'en_us', ''),
(217, 'title', 0, 'en_us', ' ms ge 21 web '),
(217, 'kind', 0, 'en_us', ' image '),
(217, 'extension', 0, 'en_us', ' jpg '),
(217, 'filename', 0, 'en_us', ' ms_ge_21_web jpg '),
(216, 'title', 0, 'en_us', ' ms ge 17 web '),
(216, 'slug', 0, 'en_us', ''),
(216, 'kind', 0, 'en_us', ' image '),
(216, 'extension', 0, 'en_us', ' jpg '),
(216, 'filename', 0, 'en_us', ' ms_ge_17_web jpg '),
(215, 'title', 0, 'en_us', ' german expressionism the graphic impulse '),
(215, 'slug', 0, 'en_us', ' german expressionism the graphic impulse '),
(215, 'field', 1, 'en_us', ' ms ge 30 web the identity was inspired by woodblock prints contained within the exhibition for the title wall 10 letters were hand painted to recreated a woodblock print texture ms ge 24 web ms ge 26 web ms ge 21 web as a way to promote the show a free newspaper was designed to showcase various prints from the exhibition as well as expand on the print making process in addition to german expressionism a second print related exhibition was also included in the paper ms south africa german express 5481 web ms south africa german express 5484 web ms south africa german express 5487 web ms south africa german express 5490 web ms south africa german express 5518 web ms south africa german express 5499 web ms south africa german express 5509 web ms south africa german express 5511 web ms south africa german express 5585 web ms south africa german express 5561 web '),
(215, 'field', 17, 'en_us', ' ms ge 30 web ms ge 36 web ms south africa german express 5585 web ms south africa german express 5487 web '),
(215, 'field', 9, 'en_us', ' ms ge 17 web '),
(215, 'field', 3, 'en_us', ' exhibition identity for german expressionism the graphic impulse a collection of prints painting and sculpture created in germany and austria during the earlier decades of the 20th century '),
(215, 'field', 11, 'en_us', ''),
(214, 'slug', 0, 'en_us', ''),
(214, 'field', 7, 'en_us', ' saks bag street 8 trix web '),
(213, 'slug', 0, 'en_us', ''),
(213, 'field', 16, 'en_us', ''),
(213, 'field', 15, 'en_us', ' sfa look cataglogue new2 trix web '),
(213, 'field', 14, 'en_us', ' every catalog cover featured a unique combination of the logotype and integration of photography '),
(213, 'field', 13, 'en_us', ' sfa look cataglogue aug1 trix web '),
(212, 'title', 0, 'en_us', ' saks bag street 4 trix web '),
(13, 'field', 17, 'en_us', ' gato web 16 gato web 1 gato web 11 gato web 7 '),
(15, 'field', 17, 'en_us', ' saks bag street 5 trix web sfa look cataglogue june 1 trix web saks bag street 4 trix web '),
(208, 'kind', 0, 'en_us', ' image '),
(181, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_detail_16_web jpg '),
(73, 'field', 10, 'en_us', ' project nycta graphics standards manual reissue photos brian kelley '),
(73, 'field', 11, 'en_us', ' thestandardsmanual com http thestandardsmanual com kickstarter campaign http https www kickstarter com projects thestandardsmanual full size reissue of the nycta graphics standards description '),
(73, 'field', 3, 'en_us', ' full size reissue of the 1970 nyc transit authority graphics standards manual foreword by michael bierut pentagram essay by christopher bonanos ny mag in collaboration with hamish smyth '),
(73, 'field', 17, 'en_us', ' nycta graphics standards manual detail 5 web nycta graphics standards manual 8 web nycta graphics standards manual detail 16 web nycta graphics standards manual 47 web '),
(73, 'field', 9, 'en_us', ' nycta graphics standards manual cover web '),
(73, 'field', 1, 'en_us', ' nycta graphics standards manual 1 web nycta graphics standards manual 26 web nycta graphics standards manual 30 web nycta graphics standards manual detail 7 web every page of the manual was scanned from the private collection of lella and massimo vignelli provided by their son luca vignelli nycta graphics standards manual 13 web nycta graphics standards manual detail 2 web nycta graphics standards manual 46 web nycta graphics standards manual 47 web nycta graphics standards manual 48 web the pages were reproduced at 100% scale without any modifications a stochastic printing method was used in order to achieve the best reproduction from the original manual nycta graphics standards manual detail 17 web nycta graphics standards manual 7 web nycta graphics standards manual detail 25 web nycta graphics standards manual 14 web nycta graphics standards manual 16 web nycta graphics standards manual detail 16 web nycta graphics standards manual 39 web '),
(73, 'slug', 0, 'en_us', ' nycta graphics standards manual '),
(73, 'title', 0, 'en_us', ' nycta graphics standards manual reissue '),
(180, 'extension', 0, 'en_us', ' jpg '),
(180, 'kind', 0, 'en_us', ' image '),
(180, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_detail_10_web jpg '),
(179, 'slug', 0, 'en_us', ''),
(179, 'title', 0, 'en_us', ' nycta graphics standards manual detail 7 web '),
(179, 'kind', 0, 'en_us', ' image '),
(179, 'extension', 0, 'en_us', ' jpg '),
(179, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_detail_7_web jpg '),
(178, 'title', 0, 'en_us', ' nycta graphics standards manual detail 5 web '),
(178, 'kind', 0, 'en_us', ' image '),
(178, 'slug', 0, 'en_us', ''),
(178, 'extension', 0, 'en_us', ' jpg '),
(178, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_detail_5_web jpg '),
(177, 'title', 0, 'en_us', ' nycta graphics standards manual cover web '),
(177, 'slug', 0, 'en_us', ''),
(177, 'kind', 0, 'en_us', ' image '),
(177, 'extension', 0, 'en_us', ' jpg '),
(177, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_cover_web jpg '),
(176, 'title', 0, 'en_us', ' nycta graphics standards manual 48 web '),
(176, 'kind', 0, 'en_us', ' image '),
(176, 'slug', 0, 'en_us', ''),
(176, 'extension', 0, 'en_us', ' jpg '),
(176, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_48_web jpg '),
(175, 'title', 0, 'en_us', ' nycta graphics standards manual 47 web '),
(175, 'slug', 0, 'en_us', ''),
(175, 'kind', 0, 'en_us', ' image '),
(175, 'extension', 0, 'en_us', ' jpg '),
(175, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_47_web jpg '),
(174, 'title', 0, 'en_us', ' nycta graphics standards manual 46 web '),
(174, 'kind', 0, 'en_us', ' image '),
(174, 'slug', 0, 'en_us', ''),
(174, 'extension', 0, 'en_us', ' jpg '),
(174, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_46_web jpg '),
(173, 'title', 0, 'en_us', ' nycta graphics standards manual 45 web '),
(173, 'slug', 0, 'en_us', ''),
(173, 'kind', 0, 'en_us', ' image '),
(173, 'extension', 0, 'en_us', ' jpg '),
(173, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_45_web jpg '),
(172, 'title', 0, 'en_us', ' nycta graphics standards manual 44 web '),
(172, 'kind', 0, 'en_us', ' image '),
(172, 'slug', 0, 'en_us', ''),
(172, 'extension', 0, 'en_us', ' jpg '),
(172, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_44_web jpg '),
(171, 'title', 0, 'en_us', ' nycta graphics standards manual 43 web '),
(171, 'slug', 0, 'en_us', ''),
(171, 'kind', 0, 'en_us', ' image '),
(171, 'extension', 0, 'en_us', ' jpg '),
(171, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_43_web jpg '),
(170, 'title', 0, 'en_us', ' nycta graphics standards manual 39 web '),
(170, 'kind', 0, 'en_us', ' image '),
(170, 'slug', 0, 'en_us', ''),
(170, 'extension', 0, 'en_us', ' jpg '),
(170, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_39_web jpg '),
(169, 'title', 0, 'en_us', ' nycta graphics standards manual 30 web '),
(169, 'slug', 0, 'en_us', ''),
(169, 'kind', 0, 'en_us', ' image '),
(169, 'extension', 0, 'en_us', ' jpg '),
(169, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_30_web jpg '),
(168, 'title', 0, 'en_us', ' nycta graphics standards manual 26 web '),
(168, 'kind', 0, 'en_us', ' image '),
(168, 'slug', 0, 'en_us', ''),
(168, 'extension', 0, 'en_us', ' jpg '),
(168, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_26_web jpg '),
(167, 'title', 0, 'en_us', ' nycta graphics standards manual 20 web '),
(167, 'slug', 0, 'en_us', ''),
(167, 'kind', 0, 'en_us', ' image '),
(167, 'extension', 0, 'en_us', ' jpg '),
(167, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_20_web jpg '),
(166, 'title', 0, 'en_us', ' nycta graphics standards manual 16 web '),
(166, 'kind', 0, 'en_us', ' image '),
(166, 'slug', 0, 'en_us', ''),
(166, 'extension', 0, 'en_us', ' jpg '),
(166, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_16_web jpg '),
(165, 'slug', 0, 'en_us', ''),
(165, 'title', 0, 'en_us', ' nycta graphics standards manual 14 web '),
(165, 'kind', 0, 'en_us', ' image '),
(165, 'extension', 0, 'en_us', ' jpg '),
(165, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_14_web jpg '),
(164, 'slug', 0, 'en_us', ''),
(164, 'title', 0, 'en_us', ' nycta graphics standards manual 13 web '),
(164, 'extension', 0, 'en_us', ' jpg '),
(164, 'kind', 0, 'en_us', ' image '),
(159, 'title', 0, 'en_us', ' nycta graphics standards manual detail 2 web '),
(159, 'kind', 0, 'en_us', ' image '),
(159, 'slug', 0, 'en_us', ''),
(159, 'extension', 0, 'en_us', ' jpg '),
(159, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual__detail_2_web jpg '),
(100, 'field', 7, 'en_us', ' nycta graphics standards manual 1 web '),
(100, 'slug', 0, 'en_us', ''),
(101, 'field', 8, 'en_us', ' nycta graphics standards manual 26 web '),
(101, 'field', 12, 'en_us', ' nycta graphics standards manual 30 web '),
(101, 'slug', 0, 'en_us', ''),
(102, 'field', 7, 'en_us', ' nycta graphics standards manual detail 7 web '),
(102, 'slug', 0, 'en_us', ''),
(103, 'field', 13, 'en_us', ' nycta graphics standards manual 13 web '),
(103, 'field', 14, 'en_us', ' every page of the manual was scanned from the private collection of lella and massimo vignelli provided by their son luca vignelli '),
(103, 'field', 15, 'en_us', ' nycta graphics standards manual detail 2 web '),
(103, 'field', 16, 'en_us', ''),
(103, 'slug', 0, 'en_us', ''),
(104, 'field', 7, 'en_us', ' nycta graphics standards manual 46 web '),
(104, 'slug', 0, 'en_us', ''),
(105, 'field', 8, 'en_us', ' nycta graphics standards manual 47 web '),
(105, 'field', 12, 'en_us', ' nycta graphics standards manual 48 web '),
(105, 'slug', 0, 'en_us', ''),
(106, 'field', 13, 'en_us', ' nycta graphics standards manual detail 17 web '),
(106, 'field', 14, 'en_us', ' the pages were reproduced at 100% scale without any modifications '),
(106, 'field', 15, 'en_us', ' nycta graphics standards manual 7 web '),
(106, 'field', 16, 'en_us', ' a stochastic printing method was used in order to achieve the best reproduction from the original manual '),
(106, 'slug', 0, 'en_us', ''),
(107, 'field', 7, 'en_us', ' nycta graphics standards manual detail 25 web '),
(107, 'slug', 0, 'en_us', ''),
(127, 'field', 13, 'en_us', ' gato web 5 '),
(127, 'field', 14, 'en_us', ' the identity uses a typeface called lisbon inspired by the house tiles found within the city s streets designed by colophon foundry '),
(127, 'field', 15, 'en_us', ' gato web 4 '),
(127, 'field', 16, 'en_us', ' a custom ball terminal was designed for the g in the logotype '),
(127, 'slug', 0, 'en_us', ''),
(128, 'field', 7, 'en_us', ' gato web 16 '),
(128, 'slug', 0, 'en_us', ''),
(209, 'filename', 0, 'en_us', ' sfa_look_cataglogue_june_1_trix_web jpg '),
(209, 'extension', 0, 'en_us', ' jpg '),
(185, 'filename', 0, 'en_us', ' gato_web_1 jpg '),
(132, 'field', 13, 'en_us', ' gato web 10 '),
(132, 'field', 14, 'en_us', ' menus and other collateral use pitch as the supporting typeface '),
(132, 'field', 15, 'en_us', ' gato web 12 '),
(132, 'field', 16, 'en_us', ''),
(132, 'slug', 0, 'en_us', ''),
(133, 'field', 7, 'en_us', ' gato web 2 '),
(133, 'slug', 0, 'en_us', ''),
(134, 'field', 8, 'en_us', ' gato web 7 '),
(134, 'field', 12, 'en_us', ' gato web 17rev '),
(134, 'slug', 0, 'en_us', ''),
(135, 'field', 7, 'en_us', ' gato web 3 '),
(135, 'slug', 0, 'en_us', ''),
(209, 'kind', 0, 'en_us', ' image '),
(209, 'slug', 0, 'en_us', ''),
(209, 'title', 0, 'en_us', ' sfa look cataglogue june 1 trix web '),
(210, 'filename', 0, 'en_us', ' sfa_look_cataglogue_new1_trix_web jpg '),
(210, 'extension', 0, 'en_us', ' jpg '),
(210, 'kind', 0, 'en_us', ' image '),
(210, 'slug', 0, 'en_us', ''),
(210, 'title', 0, 'en_us', ' sfa look cataglogue new1 trix web '),
(211, 'filename', 0, 'en_us', ' sfa_look_cataglogue_new2_trix_web jpg '),
(211, 'extension', 0, 'en_us', ' jpg '),
(211, 'kind', 0, 'en_us', ' image '),
(211, 'slug', 0, 'en_us', ''),
(211, 'title', 0, 'en_us', ' sfa look cataglogue new2 trix web '),
(212, 'filename', 0, 'en_us', ' saks_bag_street_4_trix_web jpg '),
(212, 'extension', 0, 'en_us', ' jpg '),
(203, 'filename', 0, 'en_us', ' saks_bag_street_5_trix_web jpg '),
(203, 'extension', 0, 'en_us', ' jpg '),
(203, 'kind', 0, 'en_us', ' image '),
(203, 'slug', 0, 'en_us', ''),
(203, 'title', 0, 'en_us', ' saks bag street 5 trix web '),
(204, 'filename', 0, 'en_us', ' saks_bag_street_8_trix_web jpg '),
(204, 'extension', 0, 'en_us', ' jpg '),
(204, 'kind', 0, 'en_us', ' image '),
(204, 'slug', 0, 'en_us', ''),
(204, 'title', 0, 'en_us', ' saks bag street 8 trix web '),
(205, 'filename', 0, 'en_us', ' saks_bag_street_9_trix_web jpg '),
(205, 'extension', 0, 'en_us', ' jpg '),
(205, 'kind', 0, 'en_us', ' image '),
(205, 'slug', 0, 'en_us', ''),
(205, 'title', 0, 'en_us', ' saks bag street 9 trix web '),
(206, 'filename', 0, 'en_us', ' saks_bag_street_16_trix_web jpg '),
(155, 'field', 13, 'en_us', ' saks bag street 4 trix web '),
(155, 'field', 14, 'en_us', ' various combinations are created out of the geometric letterforms to accomplish unique bag and catalog covers '),
(155, 'field', 15, 'en_us', ' saks look mark '),
(155, 'field', 16, 'en_us', ''),
(155, 'slug', 0, 'en_us', ''),
(156, 'field', 7, 'en_us', ' sfa look cataglogue new1 trix web '),
(156, 'slug', 0, 'en_us', ''),
(157, 'field', 8, 'en_us', ' saks bag street 5 trix web '),
(157, 'field', 12, 'en_us', ' saks bag street 9 trix web '),
(157, 'slug', 0, 'en_us', ''),
(215, 'field', 10, 'en_us', ' project german expressionism the graphic impulse client museum of modern art moma cr dir julia hoffmann ar dir brigitta bungard photos martin seck '),
(182, 'kind', 0, 'en_us', ' image '),
(182, 'slug', 0, 'en_us', ''),
(182, 'title', 0, 'en_us', ' nycta graphics standards manual detail 17 web '),
(183, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_detail_25_web jpg '),
(183, 'extension', 0, 'en_us', ' jpg '),
(183, 'kind', 0, 'en_us', ' image '),
(183, 'slug', 0, 'en_us', ''),
(183, 'title', 0, 'en_us', ' nycta graphics standards manual detail 25 web '),
(184, 'filename', 0, 'en_us', ' nycta_graphics_standards_manual_1_web jpg '),
(184, 'extension', 0, 'en_us', ' jpg '),
(184, 'kind', 0, 'en_us', ' image '),
(184, 'slug', 0, 'en_us', ''),
(184, 'title', 0, 'en_us', ' nycta graphics standards manual 1 web '),
(357, 'field', 7, 'en_us', ' gato web 14 '),
(222, 'kind', 0, 'en_us', ' image '),
(222, 'slug', 0, 'en_us', ''),
(222, 'title', 0, 'en_us', ' ms south africa german express 5481 web '),
(223, 'filename', 0, 'en_us', ' ms_south africa_german express_5484_web jpg '),
(223, 'extension', 0, 'en_us', ' jpg '),
(223, 'kind', 0, 'en_us', ' image '),
(223, 'slug', 0, 'en_us', ''),
(223, 'title', 0, 'en_us', ' ms south africa german express 5484 web '),
(224, 'filename', 0, 'en_us', ' ms_south africa_german express_5487_web jpg '),
(224, 'extension', 0, 'en_us', ' jpg '),
(224, 'kind', 0, 'en_us', ' image '),
(224, 'slug', 0, 'en_us', ''),
(224, 'title', 0, 'en_us', ' ms south africa german express 5487 web '),
(225, 'filename', 0, 'en_us', ' ms_south africa_german express_5490_web jpg '),
(225, 'extension', 0, 'en_us', ' jpg '),
(225, 'kind', 0, 'en_us', ' image '),
(225, 'slug', 0, 'en_us', ''),
(225, 'title', 0, 'en_us', ' ms south africa german express 5490 web '),
(226, 'filename', 0, 'en_us', ' ms_south africa_german express_5499_web jpg '),
(226, 'extension', 0, 'en_us', ' jpg '),
(226, 'kind', 0, 'en_us', ' image '),
(226, 'slug', 0, 'en_us', ''),
(226, 'title', 0, 'en_us', ' ms south africa german express 5499 web '),
(227, 'filename', 0, 'en_us', ' ms_south africa_german express_5509_web jpg '),
(227, 'extension', 0, 'en_us', ' jpg '),
(227, 'kind', 0, 'en_us', ' image '),
(227, 'slug', 0, 'en_us', ''),
(227, 'title', 0, 'en_us', ' ms south africa german express 5509 web '),
(228, 'filename', 0, 'en_us', ' ms_south africa_german express_5511_web jpg '),
(228, 'extension', 0, 'en_us', ' jpg '),
(228, 'kind', 0, 'en_us', ' image '),
(228, 'slug', 0, 'en_us', ''),
(228, 'title', 0, 'en_us', ' ms south africa german express 5511 web '),
(229, 'filename', 0, 'en_us', ' ms_south africa_german express_5518_web jpg '),
(229, 'extension', 0, 'en_us', ' jpg '),
(229, 'kind', 0, 'en_us', ' image '),
(229, 'slug', 0, 'en_us', ''),
(229, 'title', 0, 'en_us', ' ms south africa german express 5518 web '),
(230, 'filename', 0, 'en_us', ' ms_south africa_german express_5526_web jpg '),
(230, 'extension', 0, 'en_us', ' jpg '),
(230, 'kind', 0, 'en_us', ' image '),
(230, 'slug', 0, 'en_us', ''),
(230, 'title', 0, 'en_us', ' ms south africa german express 5526 web '),
(231, 'filename', 0, 'en_us', ' ms_south africa_german express_5561_web jpg '),
(231, 'extension', 0, 'en_us', ' jpg '),
(231, 'kind', 0, 'en_us', ' image '),
(231, 'slug', 0, 'en_us', ''),
(231, 'title', 0, 'en_us', ' ms south africa german express 5561 web '),
(232, 'filename', 0, 'en_us', ' ms_south africa_german express_5585_web jpg '),
(232, 'extension', 0, 'en_us', ' jpg '),
(232, 'kind', 0, 'en_us', ' image '),
(232, 'slug', 0, 'en_us', ''),
(232, 'title', 0, 'en_us', ' ms south africa german express 5585 web '),
(233, 'field', 7, 'en_us', ' ms ge 30 web '),
(233, 'slug', 0, 'en_us', ''),
(234, 'field', 13, 'en_us', ' ms ge 24 web '),
(234, 'field', 14, 'en_us', ' the identity was inspired by woodblock prints contained within the exhibition '),
(234, 'field', 15, 'en_us', ' ms ge 26 web '),
(234, 'field', 16, 'en_us', ' for the title wall 10 letters were hand painted to recreated a woodblock print texture '),
(234, 'slug', 0, 'en_us', ''),
(235, 'field', 7, 'en_us', ' ms ge 21 web '),
(235, 'slug', 0, 'en_us', ''),
(236, 'field', 13, 'en_us', ' ms south africa german express 5481 web '),
(236, 'field', 14, 'en_us', ' as a way to promote the show a free newspaper was designed to showcase various prints from the exhibition as well as expand on the print making process '),
(236, 'field', 15, 'en_us', ' ms south africa german express 5484 web '),
(236, 'field', 16, 'en_us', ' in addition to german expressionism a second print related exhibition was also included in the paper '),
(236, 'slug', 0, 'en_us', ''),
(237, 'field', 8, 'en_us', ' ms south africa german express 5487 web '),
(237, 'field', 12, 'en_us', ' ms south africa german express 5490 web '),
(237, 'slug', 0, 'en_us', ''),
(238, 'field', 7, 'en_us', ' ms south africa german express 5518 web '),
(238, 'slug', 0, 'en_us', ''),
(239, 'field', 8, 'en_us', ' ms south africa german express 5499 web '),
(239, 'field', 12, 'en_us', ' ms south africa german express 5509 web '),
(239, 'slug', 0, 'en_us', ''),
(240, 'field', 8, 'en_us', ' ms south africa german express 5511 web '),
(240, 'field', 12, 'en_us', ' ms south africa german express 5585 web '),
(240, 'slug', 0, 'en_us', ''),
(241, 'field', 7, 'en_us', ' ms south africa german express 5561 web '),
(241, 'slug', 0, 'en_us', ''),
(242, 'filename', 0, 'en_us', ' coyote_v_acme_01_web jpg '),
(242, 'extension', 0, 'en_us', ' jpg '),
(242, 'kind', 0, 'en_us', ' image '),
(242, 'slug', 0, 'en_us', ''),
(242, 'title', 0, 'en_us', ' coyote v acme 01 web '),
(243, 'filename', 0, 'en_us', ' coyote_v_acme_02_web jpg '),
(243, 'extension', 0, 'en_us', ' jpg '),
(243, 'kind', 0, 'en_us', ' image '),
(243, 'slug', 0, 'en_us', ''),
(243, 'title', 0, 'en_us', ' coyote v acme 02 web '),
(244, 'filename', 0, 'en_us', ' coyote_v_acme_03_web jpg '),
(244, 'extension', 0, 'en_us', ' jpg '),
(244, 'kind', 0, 'en_us', ' image '),
(244, 'slug', 0, 'en_us', ''),
(244, 'title', 0, 'en_us', ' coyote v acme 03 web '),
(245, 'filename', 0, 'en_us', ' coyote_v_acme_06_web jpg '),
(245, 'extension', 0, 'en_us', ' jpg '),
(245, 'kind', 0, 'en_us', ' image '),
(245, 'slug', 0, 'en_us', ''),
(245, 'title', 0, 'en_us', ' coyote v acme 06 web '),
(246, 'filename', 0, 'en_us', ' coyote_v_acme_11_web jpg '),
(246, 'extension', 0, 'en_us', ' jpg '),
(246, 'kind', 0, 'en_us', ' image '),
(246, 'slug', 0, 'en_us', ''),
(246, 'title', 0, 'en_us', ' coyote v acme 11 web '),
(247, 'filename', 0, 'en_us', ' coyote_v_acme_13_web jpg '),
(247, 'extension', 0, 'en_us', ' jpg '),
(247, 'kind', 0, 'en_us', ' image '),
(247, 'slug', 0, 'en_us', ''),
(247, 'title', 0, 'en_us', ' coyote v acme 13 web '),
(248, 'filename', 0, 'en_us', ' coyote_v_acme_16_web jpg '),
(248, 'extension', 0, 'en_us', ' jpg '),
(248, 'kind', 0, 'en_us', ' image '),
(248, 'slug', 0, 'en_us', ''),
(248, 'title', 0, 'en_us', ' coyote v acme 16 web '),
(249, 'filename', 0, 'en_us', ' coyote_v_acme_18_web jpg '),
(249, 'extension', 0, 'en_us', ' jpg '),
(249, 'kind', 0, 'en_us', ' image '),
(249, 'slug', 0, 'en_us', ''),
(249, 'title', 0, 'en_us', ' coyote v acme 18 web '),
(250, 'filename', 0, 'en_us', ' coyote_v_acme_19_web jpg '),
(250, 'extension', 0, 'en_us', ' jpg '),
(250, 'kind', 0, 'en_us', ' image '),
(250, 'slug', 0, 'en_us', ''),
(250, 'title', 0, 'en_us', ' coyote v acme 19 web '),
(251, 'filename', 0, 'en_us', ' coyote_v_acme_20_web jpg '),
(251, 'extension', 0, 'en_us', ' jpg '),
(251, 'kind', 0, 'en_us', ' image '),
(251, 'slug', 0, 'en_us', ''),
(251, 'title', 0, 'en_us', ' coyote v acme 20 web '),
(252, 'filename', 0, 'en_us', ' coyote_v_acme_23_web jpg '),
(252, 'extension', 0, 'en_us', ' jpg '),
(252, 'kind', 0, 'en_us', ' image '),
(252, 'slug', 0, 'en_us', ''),
(252, 'title', 0, 'en_us', ' coyote v acme 23 web '),
(253, 'filename', 0, 'en_us', ' coyote_v_acme_28_web jpg '),
(253, 'extension', 0, 'en_us', ' jpg '),
(253, 'kind', 0, 'en_us', ' image '),
(253, 'slug', 0, 'en_us', ''),
(253, 'title', 0, 'en_us', ' coyote v acme 28 web '),
(254, 'filename', 0, 'en_us', ' coyote_v_acme_29_web jpg '),
(254, 'extension', 0, 'en_us', ' jpg '),
(254, 'kind', 0, 'en_us', ' image '),
(254, 'slug', 0, 'en_us', ''),
(254, 'title', 0, 'en_us', ' coyote v acme 29 web '),
(255, 'field', 10, 'en_us', ' project coyote v acme client pentagram cr dir michael beirut firm pentagram photos martin seck '),
(255, 'field', 11, 'en_us', ' pentagram http new pentagram com 2014 01 new work coyote v acme core 77 http www core77 com posts 26324 safety first pentagram cleverly absolves acme in a design fiction starring wile e coyote 26324 '),
(255, 'field', 3, 'en_us', ' originally written by ian friazier for the new yorker this hypothetical lawsuit against wile e coyote versus the acme corporation has been meticulously illustrated by pentagram partner daniel weill '),
(255, 'field', 17, 'en_us', ' coyote v acme 29 web coyote v acme 20 web coyote v acme 03 web coyote v acme 06 web '),
(255, 'field', 9, 'en_us', ' coyote v acme 01 web '),
(255, 'field', 1, 'en_us', ' coyote v acme 02 web the booklets were distributed to clients friends and family as the 2013 holiday card from pentagram coyote v acme 03 web coyote v acme 06 web coyote v acme 11 web coyote v acme 13 web coyote v acme 19 web coyote v acme 28 web coyote v acme 23 web '),
(255, 'slug', 0, 'en_us', ' coyote v acme '),
(255, 'title', 0, 'en_us', ' coyote v acme '),
(256, 'field', 7, 'en_us', ' coyote v acme 02 web '),
(256, 'slug', 0, 'en_us', ''),
(257, 'field', 13, 'en_us', ' coyote v acme 03 web '),
(257, 'field', 14, 'en_us', ' the booklets were distributed to clients friends and family as the 2013 holiday card from pentagram '),
(257, 'field', 15, 'en_us', ' coyote v acme 06 web '),
(257, 'field', 16, 'en_us', ''),
(257, 'slug', 0, 'en_us', ''),
(258, 'field', 8, 'en_us', ' coyote v acme 11 web '),
(258, 'field', 12, 'en_us', ' coyote v acme 13 web '),
(258, 'slug', 0, 'en_us', ''),
(259, 'field', 7, 'en_us', ' coyote v acme 19 web '),
(259, 'slug', 0, 'en_us', ''),
(260, 'field', 7, 'en_us', ' coyote v acme 28 web '),
(260, 'slug', 0, 'en_us', ''),
(261, 'field', 7, 'en_us', ' coyote v acme 23 web '),
(261, 'slug', 0, 'en_us', ''),
(262, 'filename', 0, 'en_us', ' bigapps_symbol_web jpg '),
(262, 'extension', 0, 'en_us', ' jpg '),
(262, 'kind', 0, 'en_us', ' image '),
(262, 'slug', 0, 'en_us', ''),
(262, 'title', 0, 'en_us', ' bigapps symbol web '),
(263, 'filename', 0, 'en_us', ' bigapps_program_web jpg '),
(263, 'extension', 0, 'en_us', ' jpg '),
(263, 'kind', 0, 'en_us', ' image '),
(263, 'slug', 0, 'en_us', ''),
(263, 'title', 0, 'en_us', ' bigapps program web '),
(264, 'filename', 0, 'en_us', ' bigapps_mayor_web jpg '),
(264, 'extension', 0, 'en_us', ' jpg '),
(264, 'kind', 0, 'en_us', ' image '),
(264, 'slug', 0, 'en_us', ''),
(264, 'title', 0, 'en_us', ' bigapps mayor web '),
(265, 'filename', 0, 'en_us', ' bigapps_poster_detail_web jpg '),
(265, 'extension', 0, 'en_us', ' jpg '),
(265, 'kind', 0, 'en_us', ' image '),
(265, 'slug', 0, 'en_us', ''),
(265, 'title', 0, 'en_us', ' bigapps poster detail web '),
(266, 'field', 10, 'en_us', ' project big apps nyc client city of new york cr dir michael beirut firm pentagram '),
(266, 'field', 11, 'en_us', ' pentagram http new pentagram com 2013 07 nyc bigapps 2013 winners announced bigapps nyc http bigapps nyc '),
(266, 'field', 3, 'en_us', ' identity for big apps nyc a competition aimed at building better technology for the residents of new york city through open sourced data '),
(266, 'field', 17, 'en_us', ' bigapps program web bigapps mayor web bigapps poster detail web '),
(266, 'field', 9, 'en_us', ' bigapps symbol web '),
(266, 'field', 1, 'en_us', ' bigapps mayor web the new identity has been used for the past three years of the competition bigapps bus shelter 2014 11 cc web bigapps bus shelter 2014 50 web bigapps program web bigapps screen circles web bigapps button web a primary concept for the symbol was it s ability to communicate dynamic information as seen in this limited edition poster given away at the 2013 awards ceremony bigapps poster v4 web bigapps poster detail web bigapps bloom web '),
(266, 'slug', 0, 'en_us', ' big apps nyc '),
(266, 'title', 0, 'en_us', ' big apps nyc '),
(267, 'filename', 0, 'en_us', ' bigapps_poster_v4_web jpg '),
(267, 'extension', 0, 'en_us', ' jpg '),
(267, 'kind', 0, 'en_us', ' image '),
(267, 'slug', 0, 'en_us', ''),
(267, 'title', 0, 'en_us', ' bigapps poster v4 web '),
(268, 'filename', 0, 'en_us', ' bigapps_screen_circles_web jpg '),
(268, 'extension', 0, 'en_us', ' jpg '),
(268, 'kind', 0, 'en_us', ' image '),
(268, 'slug', 0, 'en_us', ''),
(268, 'title', 0, 'en_us', ' bigapps screen circles web '),
(269, 'filename', 0, 'en_us', ' bigapps_bus shelter_2014_11_cc_web jpg '),
(269, 'extension', 0, 'en_us', ' jpg '),
(269, 'kind', 0, 'en_us', ' image '),
(269, 'slug', 0, 'en_us', ''),
(269, 'title', 0, 'en_us', ' bigapps bus shelter 2014 11 cc web '),
(270, 'filename', 0, 'en_us', ' bigapps_bus shelter_2014_50_web jpg '),
(270, 'extension', 0, 'en_us', ' jpg '),
(270, 'kind', 0, 'en_us', ' image '),
(270, 'slug', 0, 'en_us', ''),
(270, 'title', 0, 'en_us', ' bigapps bus shelter 2014 50 web '),
(271, 'filename', 0, 'en_us', ' bigapps_button_web jpg '),
(271, 'extension', 0, 'en_us', ' jpg '),
(271, 'kind', 0, 'en_us', ' image '),
(271, 'slug', 0, 'en_us', ''),
(271, 'title', 0, 'en_us', ' bigapps button web '),
(272, 'filename', 0, 'en_us', ' bigapps_bloom_web jpg '),
(272, 'extension', 0, 'en_us', ' jpg '),
(272, 'kind', 0, 'en_us', ' image '),
(272, 'slug', 0, 'en_us', ''),
(272, 'title', 0, 'en_us', ' bigapps bloom web '),
(273, 'field', 7, 'en_us', ' bigapps mayor web '),
(273, 'slug', 0, 'en_us', ''),
(274, 'field', 13, 'en_us', ' bigapps bus shelter 2014 11 cc web '),
(274, 'field', 14, 'en_us', ' the new identity has been used for the past three years of the competition '),
(274, 'field', 15, 'en_us', ' bigapps bus shelter 2014 50 web '),
(274, 'field', 16, 'en_us', ''),
(274, 'slug', 0, 'en_us', ''),
(275, 'field', 7, 'en_us', ' bigapps program web '),
(275, 'slug', 0, 'en_us', ''),
(276, 'field', 8, 'en_us', ' bigapps screen circles web '),
(276, 'field', 12, 'en_us', ' bigapps button web '),
(276, 'slug', 0, 'en_us', ''),
(277, 'field', 13, 'en_us', ' bigapps poster v4 web '),
(277, 'field', 14, 'en_us', ' a primary concept for the symbol was it s ability to communicate dynamic information as seen in this limited edition poster given away at the 2013 awards ceremony '),
(277, 'field', 15, 'en_us', ' bigapps poster detail web '),
(277, 'field', 16, 'en_us', ''),
(277, 'slug', 0, 'en_us', ''),
(278, 'field', 7, 'en_us', ' bigapps bloom web '),
(278, 'slug', 0, 'en_us', ''),
(279, 'filename', 0, 'en_us', ' jessicajorgensen absinthemakestheheartgrowfonder_web jpg '),
(279, 'extension', 0, 'en_us', ' jpg '),
(279, 'kind', 0, 'en_us', ' image '),
(279, 'slug', 0, 'en_us', ''),
(279, 'title', 0, 'en_us', ' jessicajorgensen absinthemakestheheartgrowfonder web '),
(280, 'field', 10, 'en_us', ' project walk nyc client dot nyc cr dir michael beirut design hamish smyth tracey cameron firm pentagram planning city id ind des t kartor photos martin seck '),
(280, 'field', 11, 'en_us', ' pentagram http new pentagram com 2013 06 new work nyc wayfinding pentacity group http www pentacitygroup com '),
(280, 'field', 3, 'en_us', ' icon design for the new york city pedestrian wayfinding program walk nyc the signs are positioned on the street level throughout all five boroughs of nyc '),
(280, 'field', 17, 'en_us', ' dot seck1 dot icons8 dot taxi 140407 web dot icons17 '),
(280, 'field', 9, 'en_us', ' dot seck4 '),
(280, 'field', 1, 'en_us', ' a custom cut of helvetica neue named after our client the department of transportation dot was developed specifically for this project design by hamish smyth and monotype dot type med dot type med2 with reference to the aiga symbol signs every icon was redrawn to share relationships to helvetica neue dot at 16 pt as set on the maps this concept along with a strict grid system developed the family of icons dot ration dot grid dot seck3 dot icons20 dot icons19 dot icons23 dot icons3 dot icons8 dot seck1 dot icons22 dot icons17 dot icons6 dot icons7 icons were later applied to redesigned parking signage for the city of new york also designed by pentagram dot taxi 140407 web dot icons4 dot icons21 dot truckloading2 140407 web dot icons11 dot icons2 dot icons5 dot icons16 dot seck2 '),
(280, 'slug', 0, 'en_us', ' new york city pedestrian wayfinding '),
(280, 'title', 0, 'en_us', ' walk nyc '),
(281, 'filename', 0, 'en_us', ' dot_taxi_140407_web jpg '),
(281, 'extension', 0, 'en_us', ' jpg '),
(281, 'kind', 0, 'en_us', ' image '),
(281, 'slug', 0, 'en_us', ''),
(281, 'title', 0, 'en_us', ' dot taxi 140407 web '),
(282, 'filename', 0, 'en_us', ' dot_truckloading2_140407_web jpg '),
(282, 'extension', 0, 'en_us', ' jpg '),
(282, 'kind', 0, 'en_us', ' image '),
(282, 'slug', 0, 'en_us', ''),
(282, 'title', 0, 'en_us', ' dot truckloading2 140407 web '),
(283, 'field', 8, 'en_us', ' dot icons20 '),
(283, 'field', 12, 'en_us', ' dot icons19 '),
(283, 'slug', 0, 'en_us', ''),
(285, 'filename', 0, 'en_us', ' dot_icons jpg '),
(285, 'extension', 0, 'en_us', ' jpg '),
(285, 'kind', 0, 'en_us', ' image '),
(285, 'slug', 0, 'en_us', ' dot icons '),
(285, 'title', 0, 'en_us', ' dot icons '),
(309, 'slug', 0, 'en_us', ''),
(309, 'field', 12, 'en_us', ' dot icons3 '),
(287, 'filename', 0, 'en_us', ' dot_icons2 jpg '),
(287, 'extension', 0, 'en_us', ' jpg '),
(287, 'kind', 0, 'en_us', ' image '),
(287, 'slug', 0, 'en_us', ''),
(287, 'title', 0, 'en_us', ' dot icons2 '),
(288, 'filename', 0, 'en_us', ' dot_icons3 jpg '),
(288, 'extension', 0, 'en_us', ' jpg '),
(288, 'kind', 0, 'en_us', ' image '),
(288, 'slug', 0, 'en_us', ''),
(288, 'title', 0, 'en_us', ' dot icons3 '),
(289, 'filename', 0, 'en_us', ' dot_icons4 jpg '),
(289, 'extension', 0, 'en_us', ' jpg '),
(289, 'kind', 0, 'en_us', ' image '),
(289, 'slug', 0, 'en_us', ''),
(289, 'title', 0, 'en_us', ' dot icons4 '),
(290, 'filename', 0, 'en_us', ' dot_icons5 jpg '),
(290, 'extension', 0, 'en_us', ' jpg '),
(290, 'kind', 0, 'en_us', ' image '),
(290, 'slug', 0, 'en_us', ''),
(290, 'title', 0, 'en_us', ' dot icons5 '),
(291, 'filename', 0, 'en_us', ' dot_icons6 jpg '),
(291, 'extension', 0, 'en_us', ' jpg '),
(291, 'kind', 0, 'en_us', ' image '),
(291, 'slug', 0, 'en_us', ''),
(291, 'title', 0, 'en_us', ' dot icons6 '),
(292, 'filename', 0, 'en_us', ' dot_icons7 jpg '),
(292, 'extension', 0, 'en_us', ' jpg '),
(292, 'kind', 0, 'en_us', ' image '),
(292, 'slug', 0, 'en_us', ''),
(292, 'title', 0, 'en_us', ' dot icons7 '),
(293, 'filename', 0, 'en_us', ' dot_icons8 jpg '),
(293, 'extension', 0, 'en_us', ' jpg '),
(293, 'kind', 0, 'en_us', ' image '),
(293, 'slug', 0, 'en_us', ''),
(293, 'title', 0, 'en_us', ' dot icons8 '),
(294, 'filename', 0, 'en_us', ' dot_icons9 jpg '),
(294, 'extension', 0, 'en_us', ' jpg '),
(294, 'kind', 0, 'en_us', ' image '),
(294, 'slug', 0, 'en_us', ''),
(294, 'title', 0, 'en_us', ' dot icons9 '),
(295, 'filename', 0, 'en_us', ' dot_icons10 jpg '),
(295, 'extension', 0, 'en_us', ' jpg '),
(295, 'kind', 0, 'en_us', ' image '),
(295, 'slug', 0, 'en_us', ''),
(295, 'title', 0, 'en_us', ' dot icons10 '),
(296, 'filename', 0, 'en_us', ' dot_icons11 jpg '),
(296, 'extension', 0, 'en_us', ' jpg '),
(296, 'kind', 0, 'en_us', ' image '),
(296, 'slug', 0, 'en_us', ''),
(296, 'title', 0, 'en_us', ' dot icons11 '),
(297, 'filename', 0, 'en_us', ' dot_icons12 jpg '),
(297, 'extension', 0, 'en_us', ' jpg '),
(297, 'kind', 0, 'en_us', ' image '),
(297, 'slug', 0, 'en_us', ''),
(297, 'title', 0, 'en_us', ' dot icons12 '),
(298, 'filename', 0, 'en_us', ' dot_icons13 jpg '),
(298, 'extension', 0, 'en_us', ' jpg '),
(298, 'kind', 0, 'en_us', ' image '),
(298, 'slug', 0, 'en_us', ''),
(298, 'title', 0, 'en_us', ' dot icons13 '),
(299, 'filename', 0, 'en_us', ' dot_icons14 jpg '),
(299, 'extension', 0, 'en_us', ' jpg '),
(299, 'kind', 0, 'en_us', ' image '),
(299, 'slug', 0, 'en_us', ''),
(299, 'title', 0, 'en_us', ' dot icons14 '),
(300, 'filename', 0, 'en_us', ' dot_icons15 jpg '),
(300, 'extension', 0, 'en_us', ' jpg '),
(300, 'kind', 0, 'en_us', ' image '),
(300, 'slug', 0, 'en_us', ''),
(300, 'title', 0, 'en_us', ' dot icons15 '),
(301, 'filename', 0, 'en_us', ' dot_icons16 jpg '),
(301, 'extension', 0, 'en_us', ' jpg '),
(301, 'kind', 0, 'en_us', ' image '),
(301, 'slug', 0, 'en_us', ''),
(301, 'title', 0, 'en_us', ' dot icons16 '),
(302, 'filename', 0, 'en_us', ' dot_icons17 jpg '),
(302, 'extension', 0, 'en_us', ' jpg '),
(302, 'kind', 0, 'en_us', ' image '),
(302, 'slug', 0, 'en_us', ''),
(302, 'title', 0, 'en_us', ' dot icons17 '),
(303, 'filename', 0, 'en_us', ' dot_icons18 jpg '),
(303, 'extension', 0, 'en_us', ' jpg '),
(303, 'kind', 0, 'en_us', ' image '),
(303, 'slug', 0, 'en_us', ''),
(303, 'title', 0, 'en_us', ' dot icons18 '),
(304, 'filename', 0, 'en_us', ' dot_icons19 jpg '),
(304, 'extension', 0, 'en_us', ' jpg '),
(304, 'kind', 0, 'en_us', ' image '),
(304, 'slug', 0, 'en_us', ''),
(304, 'title', 0, 'en_us', ' dot icons19 '),
(305, 'filename', 0, 'en_us', ' dot_icons20 jpg '),
(305, 'extension', 0, 'en_us', ' jpg '),
(305, 'kind', 0, 'en_us', ' image '),
(305, 'slug', 0, 'en_us', ''),
(305, 'title', 0, 'en_us', ' dot icons20 '),
(306, 'filename', 0, 'en_us', ' dot_icons21 jpg '),
(306, 'extension', 0, 'en_us', ' jpg '),
(306, 'kind', 0, 'en_us', ' image '),
(306, 'slug', 0, 'en_us', ''),
(306, 'title', 0, 'en_us', ' dot icons21 '),
(307, 'filename', 0, 'en_us', ' dot_icons22 jpg '),
(307, 'extension', 0, 'en_us', ' jpg '),
(307, 'kind', 0, 'en_us', ' image '),
(307, 'slug', 0, 'en_us', ''),
(307, 'title', 0, 'en_us', ' dot icons22 '),
(308, 'filename', 0, 'en_us', ' dot_icons23 jpg '),
(308, 'extension', 0, 'en_us', ' jpg '),
(308, 'kind', 0, 'en_us', ' image '),
(308, 'slug', 0, 'en_us', ''),
(308, 'title', 0, 'en_us', ' dot icons23 '),
(309, 'field', 8, 'en_us', ' dot icons23 '),
(310, 'field', 7, 'en_us', ' dot icons8 '),
(310, 'slug', 0, 'en_us', ''),
(311, 'filename', 0, 'en_us', ' dot_sign_chinatown_web jpg '),
(311, 'extension', 0, 'en_us', ' jpg '),
(311, 'kind', 0, 'en_us', ' image '),
(311, 'slug', 0, 'en_us', ''),
(311, 'title', 0, 'en_us', ' dot sign chinatown web '),
(312, 'filename', 0, 'en_us', ' dot_type jpg '),
(312, 'extension', 0, 'en_us', ' jpg '),
(312, 'kind', 0, 'en_us', ' image '),
(312, 'slug', 0, 'en_us', ''),
(312, 'title', 0, 'en_us', ' dot type '),
(313, 'filename', 0, 'en_us', ' dot_type2 jpg '),
(313, 'extension', 0, 'en_us', ' jpg '),
(313, 'kind', 0, 'en_us', ' image '),
(313, 'slug', 0, 'en_us', ''),
(313, 'title', 0, 'en_us', ' dot type2 '),
(314, 'field', 13, 'en_us', ' dot type med '),
(314, 'field', 14, 'en_us', ' a custom cut of helvetica neue named after our client the department of transportation dot was developed specifically for this project design by hamish smyth and monotype '),
(314, 'field', 15, 'en_us', ' dot type med2 '),
(314, 'field', 16, 'en_us', ''),
(314, 'slug', 0, 'en_us', ''),
(315, 'field', 8, 'en_us', ' dot icons22 '),
(315, 'field', 12, 'en_us', ' dot icons17 '),
(315, 'slug', 0, 'en_us', ''),
(316, 'field', 13, 'en_us', ' dot taxi 140407 web '),
(316, 'field', 14, 'en_us', ' icons were later applied to redesigned parking signage for the city of new york also designed by pentagram '),
(316, 'field', 15, 'en_us', ' dot icons4 '),
(316, 'field', 16, 'en_us', ''),
(316, 'slug', 0, 'en_us', ''),
(317, 'field', 7, 'en_us', ' dot icons21 '),
(317, 'slug', 0, 'en_us', ''),
(318, 'filename', 0, 'en_us', ' dot_seck1 jpg '),
(318, 'extension', 0, 'en_us', ' jpg '),
(318, 'kind', 0, 'en_us', ' image '),
(318, 'slug', 0, 'en_us', ''),
(318, 'title', 0, 'en_us', ' dot seck1 '),
(319, 'filename', 0, 'en_us', ' dot_seck2 jpg '),
(319, 'extension', 0, 'en_us', ' jpg '),
(319, 'kind', 0, 'en_us', ' image '),
(319, 'slug', 0, 'en_us', ''),
(319, 'title', 0, 'en_us', ' dot seck2 '),
(320, 'filename', 0, 'en_us', ' dot_seck3 jpg '),
(320, 'extension', 0, 'en_us', ' jpg '),
(320, 'kind', 0, 'en_us', ' image '),
(320, 'slug', 0, 'en_us', ''),
(320, 'title', 0, 'en_us', ' dot seck3 '),
(321, 'filename', 0, 'en_us', ' dot_seck4 jpg '),
(321, 'extension', 0, 'en_us', ' jpg '),
(321, 'kind', 0, 'en_us', ' image '),
(321, 'slug', 0, 'en_us', ''),
(321, 'title', 0, 'en_us', ' dot seck4 '),
(322, 'field', 7, 'en_us', ' dot seck1 '),
(322, 'slug', 0, 'en_us', ''),
(323, 'field', 8, 'en_us', ' dot icons6 '),
(323, 'field', 12, 'en_us', ' dot icons7 '),
(323, 'slug', 0, 'en_us', ''),
(324, 'field', 7, 'en_us', ' dot seck3 '),
(324, 'slug', 0, 'en_us', ''),
(325, 'filename', 0, 'en_us', ' dot_ration jpg '),
(325, 'extension', 0, 'en_us', ' jpg '),
(325, 'kind', 0, 'en_us', ' image '),
(325, 'slug', 0, 'en_us', ''),
(325, 'title', 0, 'en_us', ' dot ration '),
(326, 'filename', 0, 'en_us', ' dot_grid jpg '),
(326, 'extension', 0, 'en_us', ' jpg '),
(326, 'kind', 0, 'en_us', ' image '),
(326, 'slug', 0, 'en_us', ''),
(326, 'title', 0, 'en_us', ' dot grid '),
(327, 'filename', 0, 'en_us', ' dot_type_med jpg '),
(327, 'extension', 0, 'en_us', ' jpg '),
(327, 'kind', 0, 'en_us', ' image '),
(327, 'slug', 0, 'en_us', ''),
(327, 'title', 0, 'en_us', ' dot type med '),
(328, 'filename', 0, 'en_us', ' dot_type_med2 jpg '),
(328, 'extension', 0, 'en_us', ' jpg '),
(328, 'kind', 0, 'en_us', ' image '),
(328, 'slug', 0, 'en_us', ''),
(328, 'title', 0, 'en_us', ' dot type med2 '),
(329, 'field', 13, 'en_us', ' dot ration '),
(329, 'field', 14, 'en_us', ' with reference to the aiga symbol signs every icon was redrawn to share relationships to helvetica neue dot at 16 pt as set on the maps '),
(329, 'field', 15, 'en_us', ' dot grid '),
(329, 'field', 16, 'en_us', ' this concept along with a strict grid system developed the family of icons '),
(329, 'slug', 0, 'en_us', ''),
(330, 'field', 8, 'en_us', ' dot icons11 '),
(330, 'field', 12, 'en_us', ' dot icons2 '),
(330, 'slug', 0, 'en_us', ''),
(331, 'filename', 0, 'en_us', ' jrfo_logotype_crops jpg '),
(331, 'extension', 0, 'en_us', ' jpg '),
(331, 'kind', 0, 'en_us', ' image '),
(331, 'slug', 0, 'en_us', ''),
(331, 'title', 0, 'en_us', ' jrfo logotype crops '),
(332, 'filename', 0, 'en_us', ' jrfo_logotype_crops2 jpg '),
(332, 'extension', 0, 'en_us', ' jpg '),
(332, 'kind', 0, 'en_us', ' image '),
(332, 'slug', 0, 'en_us', ''),
(332, 'title', 0, 'en_us', ' jrfo logotype crops2 '),
(333, 'filename', 0, 'en_us', ' jrfo_logotype jpg '),
(333, 'extension', 0, 'en_us', ' jpg '),
(333, 'kind', 0, 'en_us', ' image '),
(333, 'slug', 0, 'en_us', ''),
(333, 'title', 0, 'en_us', ' jrfo logotype '),
(334, 'filename', 0, 'en_us', ' jrfo_logotype2 jpg '),
(334, 'extension', 0, 'en_us', ' jpg '),
(334, 'kind', 0, 'en_us', ' image '),
(334, 'slug', 0, 'en_us', ''),
(334, 'title', 0, 'en_us', ' jrfo logotype2 '),
(335, 'filename', 0, 'en_us', ' jrfo_logotype3 jpg '),
(335, 'extension', 0, 'en_us', ' jpg '),
(335, 'kind', 0, 'en_us', ' image '),
(335, 'slug', 0, 'en_us', ''),
(335, 'title', 0, 'en_us', ' jrfo logotype3 '),
(336, 'filename', 0, 'en_us', ' jrfo_logotype4 jpg '),
(336, 'extension', 0, 'en_us', ' jpg '),
(336, 'kind', 0, 'en_us', ' image '),
(336, 'slug', 0, 'en_us', ''),
(336, 'title', 0, 'en_us', ' jrfo logotype4 '),
(337, 'filename', 0, 'en_us', ' jrfo_logotype5 jpg '),
(337, 'extension', 0, 'en_us', ' jpg '),
(337, 'kind', 0, 'en_us', ' image '),
(337, 'slug', 0, 'en_us', ''),
(337, 'title', 0, 'en_us', ' jrfo logotype5 '),
(338, 'filename', 0, 'en_us', ' jrfo_logotype6 jpg '),
(338, 'extension', 0, 'en_us', ' jpg '),
(338, 'kind', 0, 'en_us', ' image '),
(338, 'slug', 0, 'en_us', '');
INSERT INTO `craft_searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`) VALUES
(338, 'title', 0, 'en_us', ' jrfo logotype6 '),
(339, 'filename', 0, 'en_us', ' jrfo_logotype7 jpg '),
(339, 'extension', 0, 'en_us', ' jpg '),
(339, 'kind', 0, 'en_us', ' image '),
(339, 'slug', 0, 'en_us', ''),
(339, 'title', 0, 'en_us', ' jrfo logotype7 '),
(340, 'filename', 0, 'en_us', ' jrfo_logotype8 jpg '),
(340, 'extension', 0, 'en_us', ' jpg '),
(340, 'kind', 0, 'en_us', ' image '),
(340, 'slug', 0, 'en_us', ''),
(340, 'title', 0, 'en_us', ' jrfo logotype8 '),
(341, 'filename', 0, 'en_us', ' jrfo_logotype9 jpg '),
(341, 'extension', 0, 'en_us', ' jpg '),
(341, 'kind', 0, 'en_us', ' image '),
(341, 'slug', 0, 'en_us', ''),
(341, 'title', 0, 'en_us', ' jrfo logotype9 '),
(342, 'filename', 0, 'en_us', ' jrfo_logotype10 jpg '),
(342, 'extension', 0, 'en_us', ' jpg '),
(342, 'kind', 0, 'en_us', ' image '),
(342, 'slug', 0, 'en_us', ''),
(342, 'title', 0, 'en_us', ' jrfo logotype10 '),
(343, 'field', 10, 'en_us', ' client various '),
(343, 'field', 11, 'en_us', ''),
(343, 'field', 3, 'en_us', ' various marks and logotypes creative direction by michael bierut pentagram for m t bank penguin press penguin random house hudson yards and midwood '),
(343, 'field', 17, 'en_us', ' jrfo logotype crops2 '),
(343, 'field', 9, 'en_us', ' jrfo logotype crops '),
(343, 'field', 1, 'en_us', ' jrfo logotype jrfo logotype3 jrfo logotype5 jrfo logotype6 jrfo logotype2 jrfo logotype4 jrfo logotype7 jrfo logotype8 jrfo logotype10 jrfo logotype9 '),
(343, 'slug', 0, 'en_us', ' marks logotypes '),
(343, 'title', 0, 'en_us', ' marks logotypes '),
(344, 'field', 7, 'en_us', ' jrfo logotype '),
(344, 'slug', 0, 'en_us', ''),
(345, 'field', 7, 'en_us', ' jrfo logotype4 '),
(345, 'slug', 0, 'en_us', ''),
(346, 'field', 7, 'en_us', ' jrfo logotype3 '),
(346, 'slug', 0, 'en_us', ''),
(347, 'field', 7, 'en_us', ' jrfo logotype5 '),
(347, 'slug', 0, 'en_us', ''),
(348, 'field', 7, 'en_us', ' jrfo logotype6 '),
(348, 'slug', 0, 'en_us', ''),
(349, 'field', 7, 'en_us', ' jrfo logotype2 '),
(349, 'slug', 0, 'en_us', ''),
(350, 'field', 7, 'en_us', ' jrfo logotype7 '),
(350, 'slug', 0, 'en_us', ''),
(351, 'field', 7, 'en_us', ' jrfo logotype8 '),
(351, 'slug', 0, 'en_us', ''),
(352, 'field', 7, 'en_us', ' jrfo logotype10 '),
(352, 'slug', 0, 'en_us', ''),
(353, 'field', 7, 'en_us', ' jrfo logotype9 '),
(353, 'slug', 0, 'en_us', ''),
(354, 'field', 7, 'en_us', ' dot truckloading2 140407 web '),
(354, 'slug', 0, 'en_us', ''),
(355, 'field', 8, 'en_us', ' dot icons5 '),
(355, 'field', 12, 'en_us', ' dot icons16 '),
(355, 'slug', 0, 'en_us', ''),
(356, 'field', 7, 'en_us', ' dot seck2 '),
(356, 'slug', 0, 'en_us', ''),
(357, 'slug', 0, 'en_us', ''),
(358, 'field', 7, 'en_us', ' gato web 9 '),
(358, 'slug', 0, 'en_us', ''),
(359, 'field', 8, 'en_us', ' nycta graphics standards manual 14 web '),
(359, 'field', 12, 'en_us', ' nycta graphics standards manual 16 web '),
(359, 'slug', 0, 'en_us', ''),
(360, 'field', 7, 'en_us', ' nycta graphics standards manual detail 16 web '),
(360, 'slug', 0, 'en_us', ''),
(361, 'field', 7, 'en_us', ' nycta graphics standards manual 39 web '),
(361, 'slug', 0, 'en_us', '');

-- --------------------------------------------------------

--
-- Table structure for table `craft_sections`
--

CREATE TABLE IF NOT EXISTS `craft_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enableVersioning` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_sections_name_unq_idx` (`name`),
  UNIQUE KEY `craft_sections_handle_unq_idx` (`handle`),
  KEY `craft_sections_structureId_fk` (`structureId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `craft_sections`
--

INSERT INTO `craft_sections` (`id`, `structureId`, `name`, `handle`, `type`, `hasUrls`, `template`, `enableVersioning`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(6, NULL, 'Projects', 'projects', 'channel', 1, 'projects/_entry.html', 1, '2015-05-05 19:43:47', '2015-05-14 03:36:57', '9c3a3afe-83d8-4c63-8bca-1964145014bd');

-- --------------------------------------------------------

--
-- Table structure for table `craft_sections_i18n`
--

CREATE TABLE IF NOT EXISTS `craft_sections_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `enabledByDefault` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `urlFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nestedUrlFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_sections_i18n_sectionId_locale_unq_idx` (`sectionId`,`locale`),
  KEY `craft_sections_i18n_locale_fk` (`locale`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `craft_sections_i18n`
--

INSERT INTO `craft_sections_i18n` (`id`, `sectionId`, `locale`, `enabledByDefault`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(6, 6, 'en_us', 0, 'projects/{slug}', NULL, '2015-05-05 19:43:47', '2015-05-14 03:36:57', '90bdd886-225a-4208-a513-2c9bd331fc31');

-- --------------------------------------------------------

--
-- Table structure for table `craft_sessions`
--

CREATE TABLE IF NOT EXISTS `craft_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_sessions_uid_idx` (`uid`),
  KEY `craft_sessions_token_idx` (`token`),
  KEY `craft_sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `craft_sessions_userId_fk` (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `craft_sessions`
--

INSERT INTO `craft_sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(4, 1, 'e030bb778352e555751e018dd88cd9703abd5b9bczozNjoiYjE1NjU0Y2YtZmEzOC00ZjJiLWIyMmMtZThiZmUyOGRlNWRhIjs=', '2015-05-05 18:21:11', '2015-05-05 18:21:11', '3ba19a31-96cb-4858-be5a-28be81569bfe'),
(5, 1, '00bbd9baf74ba676d30a86364129b16021a4a5f8czozMjoiOVBsZldXWTVLTTRQUjZtT195aWJOdWtIRl9iV1NzaWEiOw==', '2015-05-07 22:53:37', '2015-05-07 22:53:37', '3771170b-1092-4087-9893-04e7ebf7218a'),
(6, 1, 'a13fc89348e48cc6f6f4337b05ddc1958c957808czozMjoiQk91czZUVmJCVjlmdVJqZm9rN080TTBvdG9TZ09SNFIiOw==', '2015-05-08 06:30:31', '2015-05-08 06:30:31', '9e72eb2d-55ee-466a-9cd8-db2ce0213633'),
(7, 1, 'fa48452d8fdc9d94dbfb417bbc39df671166495cczozMjoiZm5oZUpROXRmcm02ek5uSGNuUjU3V2JyUF9IempDb3AiOw==', '2015-05-14 03:36:21', '2015-05-14 03:52:58', '2a9c7936-524d-4da4-bae8-8cb15668b48c'),
(8, 1, 'c60b008535d373d1767aa15c1bc5f506f3347810czozMjoickI0SlpQfnlWNllEckc4Z2k0Uk9Hc1kwVV9SQWFHeEUiOw==', '2015-05-15 07:07:46', '2015-05-15 07:07:46', 'ad51b5de-4677-485c-935f-cb6e0641a5e0'),
(9, 1, '9a2b7366bfc1d24411e1a533d0b14abf53d800acczozMjoiZ3ozTFZYcnJncHNUdDBuQkRfU01DQTZBfjN4bTdaWk0iOw==', '2015-06-01 04:55:21', '2015-06-01 04:55:21', 'b37273f9-a806-49c7-9919-84c432340c86'),
(10, 1, '270334c4f6d544e48afb9ef05409e71a92cb80dfczozMjoiWGpuRWZaYkRfZ2hzMTBldDhwOWNPVGU0eWIzYzJZbkwiOw==', '2015-07-04 01:39:09', '2015-07-04 01:39:09', '7ebb000f-fcb8-44a4-8fe0-d9e4b35d3380'),
(11, 1, 'bfffb51bb37be140ff889e601839c9a0a01ad546czozMjoiT3pwRDB1RDB4ZzFGSUdvc3RrVHdzWUYwS0F3YmxZSTIiOw==', '2015-07-12 01:59:41', '2015-07-12 01:59:41', 'e1b15f17-fc6a-46cf-97bf-dbdd60348cb7'),
(13, 1, '987bff2b7661027f525146c04c0a450e296c0f7fczozMjoibzB6bGh3bkdLOUlPWmpqd25+bmM2VUg2cmxDMWY0U3kiOw==', '2015-07-12 20:49:26', '2015-07-12 20:49:26', '5e943c0c-0dd8-4731-abfe-20229bbea963'),
(14, 1, 'b3b4942d5a7279eab930e4e6b290cac8a5fa3651czozMjoiQkVzTENSWE8ySjJ6U3NONmVPXzgwN2RSZ091dVZsZlgiOw==', '2015-07-12 22:14:17', '2015-07-12 22:14:17', '93254eb8-2760-41fa-acc9-d34ac4c83e6e'),
(17, 1, '7bae2e31f7cd52d83fb4205804ecc0a37587b7c4czozMjoienlhZjAybkloZkl1b0JqMGRDTm1+Unk5eVVCbTFMNXAiOw==', '2015-07-14 21:05:53', '2015-07-14 21:05:53', 'ebe0e2d7-c234-4c76-aca7-20ba22407db5'),
(18, 1, '2630738b1464bcf452910296c5cac3b6e44e9a41czozMjoiZmR1TF83Vk5IS2E2OUpxR0NIaVJQZWNmUTdGaDgwRXgiOw==', '2015-07-18 22:07:50', '2015-07-18 22:07:50', '9d2a42aa-6375-4610-97d6-94fed244588e'),
(19, 1, 'bee506a0eb0113d06a8fc1d9ac7ffb6a26fc126cczozMjoidWd+bE5vR0JYQ3JzOXRreDdyXzFqbE45WEdqanJ5M2siOw==', '2015-07-20 10:59:11', '2015-07-20 10:59:11', 'dffceac9-ca28-46e1-a080-965ef050efed'),
(21, 1, 'aa7b345c1d68bca00519ceb3fb71ced9107b63adczozMjoiVXVhaTY2TjNISnN4TzdxTUQyeXN2eGo1RWVlTVRhVVUiOw==', '2015-07-23 22:58:19', '2015-07-23 22:58:19', 'b54cd1fd-b65b-48c1-be7c-6120fd17d3c0'),
(23, 1, '7ad657a2d20d7499646b73ef36a2133e35ebb738czozMjoiX3FfNXBJZks4MGZUTFpCNkU4MkNab3l6ZkRnUzE0ZEgiOw==', '2015-08-07 17:31:09', '2015-08-07 17:31:09', 'ea66d5b5-606d-41f5-9bc5-ea3869ec3472'),
(25, 1, 'e0cd9fcd06c7abe1148b2f050e521d372b9d4f2dczozMjoiTWozWVdheTZCVURCaWtCRllzVkFFd0xuWkpoQ1htaDAiOw==', '2015-08-16 21:10:49', '2015-08-16 21:10:49', '26cec4b0-8e01-458b-aa15-638fd25923fb'),
(26, 1, '4474ded3e15dde487faf2296ccdf9d4b449425d1czozMjoiUXBOUmppQ0dRZW1pcVB5b0ZjZVJtQldJVV9+RnVYQTYiOw==', '2015-08-16 21:13:31', '2015-08-16 21:13:31', '7fee739e-3382-4649-a420-814a70274c14'),
(27, 1, 'd27993cf9a5fd0d88282c33e483f9704d06ab86bczozMjoiT19NRjBoV01KTHlHM1FNT29raDVmcGZvUDBzOWRSTGQiOw==', '2015-08-18 13:19:47', '2015-08-18 13:19:47', '02ec4880-1973-4f30-9b75-4f396dfa6dde'),
(28, 1, 'd0735b8fa40c4aa7006307f782b7249a316543aeczozMjoiZTBMSVBuSW5PdzRxY0x1c3ZOUWJ3NzhxSDJZYnAzdTEiOw==', '2015-08-19 02:28:16', '2015-08-19 02:28:16', 'd73a9610-6fd3-4eb5-afce-75379869a025'),
(29, 1, '72e450263e4e0898173bcb591ce0b9411bcdd11cczozMjoibnlUZXJlTDVza2FTc2NwNnRJS3k3aFluUVBOcUhfMHMiOw==', '2015-08-20 13:50:40', '2015-08-20 13:50:40', '586f5e72-92b6-4a53-9b71-fcc15cf3bf1c'),
(30, 1, '9898329afd3847d1d9f48775a51fddfcb3a5e6fcczozMjoiTVNDdHJrUXJTU1Y5RmM4cVdsYTVrX242TzNpY1F3UGMiOw==', '2015-08-20 18:43:17', '2015-08-20 18:43:17', '21c2ca3b-8585-46ee-848f-ade9db431f71'),
(31, 1, '963eb4918435e85e6944d5b1d9273ebcc9b08156czozMjoiVmZnVmZCaFQ3dkxDMkpSVEtkTU1OZ2RRSlk2ZXcwN1IiOw==', '2015-08-22 01:53:05', '2015-08-22 01:53:05', 'eba1d851-e210-4ff2-9650-e553c5851b74'),
(32, 1, '561a92f4108e576518ed49fbae978f4ceafc3be4czozMjoiY0dabTBFWHVIemtOY1Nqc3h4ODJFMndFZUUwMlpHbEoiOw==', '2015-08-22 04:46:25', '2015-08-22 04:46:25', 'a7278445-2911-493a-87f5-fc0ea478eeec');

-- --------------------------------------------------------

--
-- Table structure for table `craft_shunnedmessages`
--

CREATE TABLE IF NOT EXISTS `craft_shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_shunnedmessages_userId_message_unq_idx` (`userId`,`message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_structureelements`
--

CREATE TABLE IF NOT EXISTS `craft_structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `craft_structureelements_root_idx` (`root`),
  KEY `craft_structureelements_lft_idx` (`lft`),
  KEY `craft_structureelements_rgt_idx` (`rgt`),
  KEY `craft_structureelements_level_idx` (`level`),
  KEY `craft_structureelements_elementId_fk` (`elementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_structures`
--

CREATE TABLE IF NOT EXISTS `craft_structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_systemsettings`
--

CREATE TABLE IF NOT EXISTS `craft_systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `craft_systemsettings`
--

INSERT INTO `craft_systemsettings` (`id`, `category`, `settings`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'email', '{"protocol":"php","emailAddress":"jeremy@bsley.com","senderName":"REST"}', '2014-12-30 09:30:30', '2014-12-30 09:30:30', 'c8a5a8ce-ff72-43f5-b4f2-6f90d6be4779');

-- --------------------------------------------------------

--
-- Table structure for table `craft_taggroups`
--

CREATE TABLE IF NOT EXISTS `craft_taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_taggroups_handle_unq_idx` (`handle`),
  KEY `craft_taggroups_fieldLayoutId_fk` (`fieldLayoutId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `craft_taggroups`
--

INSERT INTO `craft_taggroups` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Default', 'default', 1, '2014-12-30 09:30:30', '2014-12-30 09:30:30', '43e64b4c-7dca-4651-923d-17c04f8204aa');

-- --------------------------------------------------------

--
-- Table structure for table `craft_tags`
--

CREATE TABLE IF NOT EXISTS `craft_tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_tags_groupId_fk` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `craft_tasks`
--

CREATE TABLE IF NOT EXISTS `craft_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `currentStep` int(11) unsigned DEFAULT NULL,
  `totalSteps` int(11) unsigned DEFAULT NULL,
  `status` enum('pending','error','running') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_tasks_root_idx` (`root`),
  KEY `craft_tasks_lft_idx` (`lft`),
  KEY `craft_tasks_rgt_idx` (`rgt`),
  KEY `craft_tasks_level_idx` (`level`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=117 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_templatecachecriteria`
--

CREATE TABLE IF NOT EXISTS `craft_templatecachecriteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecachecriteria_cacheId_fk` (`cacheId`),
  KEY `craft_templatecachecriteria_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_templatecacheelements`
--

CREATE TABLE IF NOT EXISTS `craft_templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `craft_templatecacheelements_cacheId_fk` (`cacheId`),
  KEY `craft_templatecacheelements_elementId_fk` (`elementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `craft_templatecaches`
--

CREATE TABLE IF NOT EXISTS `craft_templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecaches_expiryDate_cacheKey_locale_path_idx` (`expiryDate`,`cacheKey`,`locale`,`path`),
  KEY `craft_templatecaches_locale_fk` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_tokens`
--

CREATE TABLE IF NOT EXISTS `craft_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_tokens_token_unq_idx` (`token`),
  KEY `craft_tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_usergroups`
--

CREATE TABLE IF NOT EXISTS `craft_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_usergroups_users`
--

CREATE TABLE IF NOT EXISTS `craft_usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `craft_usergroups_users_userId_fk` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_userpermissions`
--

CREATE TABLE IF NOT EXISTS `craft_userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_userpermissions_usergroups`
--

CREATE TABLE IF NOT EXISTS `craft_userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `craft_userpermissions_usergroups_groupId_fk` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_userpermissions_users`
--

CREATE TABLE IF NOT EXISTS `craft_userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `craft_userpermissions_users_userId_fk` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `craft_users`
--

CREATE TABLE IF NOT EXISTS `craft_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferredLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekStartDay` tinyint(4) NOT NULL DEFAULT '0',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `client` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `suspended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pending` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIPAddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(4) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `verificationCode` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_users_username_unq_idx` (`username`),
  UNIQUE KEY `craft_users_email_unq_idx` (`email`),
  KEY `craft_users_verificationCode_idx` (`verificationCode`),
  KEY `craft_users_uid_idx` (`uid`),
  KEY `craft_users_preferredLocale_fk` (`preferredLocale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `craft_users`
--

INSERT INTO `craft_users` (`id`, `username`, `photo`, `firstName`, `lastName`, `email`, `password`, `preferredLocale`, `weekStartDay`, `admin`, `client`, `locked`, `suspended`, `pending`, `archived`, `lastLoginDate`, `lastLoginAttemptIPAddress`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'admin', NULL, 'Jeremy ', 'Beasley', 'jeremy@bsley.com', '$2a$13$LLLN9babjDbH5hD090Us8elqCCWUp99YIfLDiBjJ5TVZ76OnvQv4G', NULL, 0, 1, 0, 0, 0, 0, 0, '2015-08-22 04:46:25', '24.90.91.23', NULL, NULL, '2015-07-04 01:37:52', NULL, NULL, NULL, NULL, 0, '2015-07-04 01:38:44', '2014-12-30 09:30:28', '2015-08-22 04:46:25', 'ae47ccd8-9ce4-4156-904b-cd9f282c6e28');

-- --------------------------------------------------------

--
-- Table structure for table `craft_widgets`
--

CREATE TABLE IF NOT EXISTS `craft_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_widgets_userId_fk` (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `craft_widgets`
--

INSERT INTO `craft_widgets` (`id`, `userId`, `type`, `sortOrder`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'RecentEntries', 1, NULL, 1, '2014-12-30 09:30:34', '2014-12-30 09:30:34', '3d2ccf07-7f2d-4199-ae1e-c7b8537bc1e4'),
(2, 1, 'GetHelp', 2, NULL, 0, '2014-12-30 09:30:34', '2014-12-30 09:32:30', 'cabdd6d6-944a-4f49-9cf6-b72715aca813'),
(3, 1, 'Updates', 3, NULL, 0, '2014-12-30 09:30:34', '2014-12-30 09:32:32', 'c28e89c3-2b2f-4261-87e7-3a4481a2914f'),
(4, 1, 'Feed', 4, '{"url":"http:\\/\\/feeds.feedburner.com\\/blogandtonic","title":"Blog & Tonic"}', 0, '2014-12-30 09:30:34', '2014-12-30 09:32:34', 'fa549347-eafc-48a7-ac66-a6b918ac3fb6');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `craft_assetfiles`
--
ALTER TABLE `craft_assetfiles`
  ADD CONSTRAINT `craft_assetfiles_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_assetfiles_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_assetfiles_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_assetfolders`
--
ALTER TABLE `craft_assetfolders`
  ADD CONSTRAINT `craft_assetfolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_assetfolders_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_assetindexdata`
--
ALTER TABLE `craft_assetindexdata`
  ADD CONSTRAINT `craft_assetindexdata_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_assetsources`
--
ALTER TABLE `craft_assetsources`
  ADD CONSTRAINT `craft_assetsources_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `craft_categories`
--
ALTER TABLE `craft_categories`
  ADD CONSTRAINT `craft_categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_categories_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_categorygroups`
--
ALTER TABLE `craft_categorygroups`
  ADD CONSTRAINT `craft_categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `craft_categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_categorygroups_i18n`
--
ALTER TABLE `craft_categorygroups_i18n`
  ADD CONSTRAINT `craft_categorygroups_i18n_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_categorygroups_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craft_content`
--
ALTER TABLE `craft_content`
  ADD CONSTRAINT `craft_content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_content_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craft_elements_i18n`
--
ALTER TABLE `craft_elements_i18n`
  ADD CONSTRAINT `craft_elements_i18n_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_elements_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craft_emailmessages`
--
ALTER TABLE `craft_emailmessages`
  ADD CONSTRAINT `craft_emailmessages_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craft_entries`
--
ALTER TABLE `craft_entries`
  ADD CONSTRAINT `craft_entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entries_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_entrytypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_entrydrafts`
--
ALTER TABLE `craft_entrydrafts`
  ADD CONSTRAINT `craft_entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entrydrafts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_entrytypes`
--
ALTER TABLE `craft_entrytypes`
  ADD CONSTRAINT `craft_entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `craft_entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_entryversions`
--
ALTER TABLE `craft_entryversions`
  ADD CONSTRAINT `craft_entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `craft_entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entryversions_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_fieldlayoutfields`
--
ALTER TABLE `craft_fieldlayoutfields`
  ADD CONSTRAINT `craft_fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `craft_fieldlayouttabs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_fieldlayouttabs`
--
ALTER TABLE `craft_fieldlayouttabs`
  ADD CONSTRAINT `craft_fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_fields`
--
ALTER TABLE `craft_fields`
  ADD CONSTRAINT `craft_fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_fieldgroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_globalsets`
--
ALTER TABLE `craft_globalsets`
  ADD CONSTRAINT `craft_globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `craft_globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_matrixblocks`
--
ALTER TABLE `craft_matrixblocks`
  ADD CONSTRAINT `craft_matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_matrixblocktypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_matrixblocktypes`
--
ALTER TABLE `craft_matrixblocktypes`
  ADD CONSTRAINT `craft_matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `craft_matrixcontent_body`
--
ALTER TABLE `craft_matrixcontent_body`
  ADD CONSTRAINT `craft_matrixcontent_body_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_body_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craft_migrations`
--
ALTER TABLE `craft_migrations`
  ADD CONSTRAINT `craft_migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `craft_plugins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_relations`
--
ALTER TABLE `craft_relations`
  ADD CONSTRAINT `craft_relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_relations_sourceLocale_fk` FOREIGN KEY (`sourceLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_routes`
--
ALTER TABLE `craft_routes`
  ADD CONSTRAINT `craft_routes_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craft_sections`
--
ALTER TABLE `craft_sections`
  ADD CONSTRAINT `craft_sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `craft_sections_i18n`
--
ALTER TABLE `craft_sections_i18n`
  ADD CONSTRAINT `craft_sections_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_sections_i18n_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_sessions`
--
ALTER TABLE `craft_sessions`
  ADD CONSTRAINT `craft_sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_shunnedmessages`
--
ALTER TABLE `craft_shunnedmessages`
  ADD CONSTRAINT `craft_shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_structureelements`
--
ALTER TABLE `craft_structureelements`
  ADD CONSTRAINT `craft_structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_taggroups`
--
ALTER TABLE `craft_taggroups`
  ADD CONSTRAINT `craft_taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `craft_tags`
--
ALTER TABLE `craft_tags`
  ADD CONSTRAINT `craft_tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_taggroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_tags_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_templatecachecriteria`
--
ALTER TABLE `craft_templatecachecriteria`
  ADD CONSTRAINT `craft_templatecachecriteria_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_templatecacheelements`
--
ALTER TABLE `craft_templatecacheelements`
  ADD CONSTRAINT `craft_templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_templatecaches`
--
ALTER TABLE `craft_templatecaches`
  ADD CONSTRAINT `craft_templatecaches_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craft_usergroups_users`
--
ALTER TABLE `craft_usergroups_users`
  ADD CONSTRAINT `craft_usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_userpermissions_usergroups`
--
ALTER TABLE `craft_userpermissions_usergroups`
  ADD CONSTRAINT `craft_userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_userpermissions_users`
--
ALTER TABLE `craft_userpermissions_users`
  ADD CONSTRAINT `craft_userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `craft_users`
--
ALTER TABLE `craft_users`
  ADD CONSTRAINT `craft_users_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_users_preferredLocale_fk` FOREIGN KEY (`preferredLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `craft_widgets`
--
ALTER TABLE `craft_widgets`
  ADD CONSTRAINT `craft_widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
