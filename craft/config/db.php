<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(

  '*' => array(
    // The prefix to use when naming tables. This can be no more than 5 characters.
    'tablePrefix' => 'craft',
  ),

  'jessereedfromohio.com' => array(
	'server' => 'external-db.s206686.gridserver.com',
	'user' => 'db206686',
	'password' => 'Fromohio1987!!',
	'database' => 'db206686_jessereedfromohio'
    ),

  'jessereed.dev' => array(
    'server' => 'localhost',
    'user' => 'root',
    'password' => 'root',
    'database' => 'jessereed_local'
    )
);
