
$(document).ready(function() {
  $('.imgSlider').slick({
    lazyLoad: 'ondemand',
    infinite: true,
    speed: 500,
    fade: true,
  });
  $('.sideNav.left').click(function() {
    $(this).parent().parent().find('.imgSlider').slick('slickPrev');
    console.log("worked");
  });
  $('.sideNav.right').click(function() {
    $(this).parent().parent().find('.imgSlider').slick('slickNext');
    console.log("worked");
  });

});



$(document).ready(function() {

  $('.content').addClass("closed");

  if (document.documentElement.clientWidth < 760) {
    
    $(".title").click(function(){
      var theone = $(this).parent().find('.content');
      var indicator = $(this).find('span');
      theone.toggleClass("closed");
      indicator.toggle();    
    });

  };

  // INITIATE HEADROOM.JS

  // $("header").headroom({
  //   "offset": 30,
  //   "tolerance": 5,
  //   "classes": {
  //     "initial": "",
  //     "pinned": "slideDown",
  //     "unpinned": "slideUp"
  //   }
  // });

  // $("#header").headroom("destroy");

  // fade in each image individually as it's downloaded  
  // $('img').load(function() {  
  //     $(this).fadeTo( "slow", 1 );  
  // });  

  // LINKING ENTIRE DIVS

  $(".linked").click(function(){
    window.location=$(this).find("a").attr("href"); 
    console.log('clicked');
    return false;
  });
  

  // HEADER LINKS SCROLL TO FOOTER

  $('header #info').click(function(){
    $('html,body').animate({
   scrollTop: $("footer").offset().top
    });
  });

  // SEE GRID WITH

  // $('p').click(function(){
  //   $('p').toggleClass('highlighted');
  // });

  // BACK TO TOP BUTTON

  $('footer .arrow').click(function(){
    $('html,body').animate({
   scrollTop: $("body").offset().top
    });
  });

});






  